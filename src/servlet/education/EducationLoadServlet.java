package servlet.education;

import bean.Education;
import dao.implementation.EducationDAOImpl;
import net.sf.json.JSONArray;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/4/30 下午12:34
 */
@WebServlet("/edu.servlet")
public class EducationLoadServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=UTF-8");

        PrintWriter out = resp.getWriter();
        EducationDAOImpl educationDAO = new EducationDAOImpl();

        int userId = Integer.parseInt(req.getParameter("userId"));
        String type = req.getParameter("type");

        String queryEdu = "query_all_edu";
        if (type.equals(queryEdu)){
            System.out.println("query education exper");
            ArrayList<Education> resultList = educationDAO.queryAllEducationById(userId);
            for (Education e : resultList){
                e.setStr_starttime(StringUtils.dateToString(e.getEnrollmenttime()));
                e.setStr_endtime(StringUtils.dateToString(e.getGraduationtime()));
            }
            String json = JSONArray.fromObject(resultList).toString();
            System.out.println(json);
            out.println(json);
        }

        String insertNew = "insert_new_edu";
        if (type.equals(insertNew)){
            String school = req.getParameter("school");
            String major = req.getParameter("major");
            String degree = req.getParameter("degree");
            String startTime = req.getParameter("starttime");
            String endTime = req.getParameter("endtime");
            String maincourse = req.getParameter("maincourse");
            Education edu = new Education();
            System.out.println("main course " + maincourse);
            edu.setUserid(userId);
            edu.setSchool(school);
            edu.setMajor(major);
            edu.setDegree(degree);
            edu.setMaincourse(maincourse);
            if (startTime.equals("") && !endTime.equals("")){
                edu.setEnrollmenttime(StringUtils.stringToDate("9999-12-31"));
                edu.setGraduationtime(StringUtils.stringToDate(endTime));
            }else if (!startTime.equals("") && endTime.equals("")){
                edu.setEnrollmenttime(StringUtils.stringToDate(startTime));
                edu.setGraduationtime(StringUtils.stringToDate("9999-12-31"));
            }else if (!startTime.equals("") && !endTime.equals("")){
                edu.setEnrollmenttime(StringUtils.stringToDate(startTime));
                edu.setGraduationtime(StringUtils.stringToDate(endTime));
            }else if (startTime.equals("") && endTime.equals("")){
                edu.setEnrollmenttime(StringUtils.stringToDate("9999-12-31"));
                edu.setGraduationtime(StringUtils.stringToDate("9999-12-31"));
            }
            int res = educationDAO.addEducation(edu);
            System.out.println(res);
            if(res == -1000000000){
                resp.getWriter().print("failed");
            } else{
                System.out.println("HERE !!!!!!");
                resp.getWriter().print("success");
            }


        }

        if (type.equals("delete")){
            int eduid = Integer.parseInt(req.getParameter("eduid"));
            int res = educationDAO.deleteEdu(eduid);
            if(res == -1){
                resp.getWriter().print("failed");
            } else{
                resp.getWriter().print("success");
            }

        }

        if (type.equals("edit")){
            System.out.println("start to edit");
            int educationId = Integer.parseInt(req.getParameter("educationId"));
            String school = req.getParameter("school");
            String degree = req.getParameter("degree");
            String major = req.getParameter("major");
            String startTime = req.getParameter("starttime");
            String endTime = req.getParameter("endtime");
            String course = req.getParameter("course");

            Education edu = new Education();
            edu.setEducationid(educationId);
            edu.setSchool(school);
            edu.setDegree(degree);
            edu.setMajor(major);
            edu.setMaincourse(course);

            if (startTime.equals("") && !endTime.equals("")){
                edu.setEnrollmenttime(StringUtils.stringToDate("9999-12-31"));
                edu.setGraduationtime(StringUtils.stringToDate(endTime));
            }else if (!startTime.equals("") && endTime.equals("")){
                edu.setEnrollmenttime(StringUtils.stringToDate(startTime));
                edu.setGraduationtime(StringUtils.stringToDate("9999-12-31"));
            }else if (!startTime.equals("") && !endTime.equals("")){
                edu.setEnrollmenttime(StringUtils.stringToDate(startTime));
                edu.setGraduationtime(StringUtils.stringToDate(endTime));
            }else if (startTime.equals("") && endTime.equals("")){
                edu.setEnrollmenttime(StringUtils.stringToDate("9999-12-31"));
                edu.setGraduationtime(StringUtils.stringToDate("9999-12-31"));
            }

            int res = educationDAO.editEdu(edu);
            if(res == -1){
                resp.getWriter().print("failed");
            } else{
                resp.getWriter().print("success");
            }
        }



    }
}
