package servlet.image;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;
import com.oreilly.servlet.multipart.FileRenamePolicy;
import util.OSSClientUtils;

import java.io.*;
import java.util.Enumeration;


/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/4/27 下午10:27
 */

@WebServlet(urlPatterns ="/upload")
public class ImageUploadServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Start to process upload request");
        String filename = null;
        String url = null;
        String fileType = "";
        String requextip = req.getRemoteAddr();
        String saveDirectory = this.getServletContext().getRealPath("") + "\\img";
        File savedir = new File(saveDirectory);
        System.out.println(savedir);
        if (!savedir.exists())
            savedir.mkdir();
        int maxPostSize = 10 * 1024 * 1024;
        FileRenamePolicy policy = (FileRenamePolicy) new DefaultFileRenamePolicy();
        MultipartRequest multi;
        multi = new MultipartRequest(req, saveDirectory, maxPostSize, "UTF-8", policy);
        Enumeration<String> files = multi.getFileNames();
        System.out.println(files.toString() + "1111");
        String name = files.nextElement();
        File f = multi.getFile(name);
        if (f != null) {
            filename = f.getName();
        }
        InputStream in = new FileInputStream(f);
        OSSClientUtils ossUtil = new OSSClientUtils();
        url = ossUtil.uploadImageToOSS(filename, in);
        req.setAttribute("url", url);
        String pathname = saveDirectory + "/" + filename;
        File file = new File(pathname);
        if (file.isFile() && file.exists()) {
            System.out.println("delete file");
            file.delete();
        }
        PrintWriter out = resp.getWriter();
        System.out.println(url);
        out.println(url);

    }




}
