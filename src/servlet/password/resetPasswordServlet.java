package servlet.password;

import dao.implementation.ResetPasswordDAOImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/resetPasswordServlet")
public class resetPasswordServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        String password = req.getParameter("password");
        String email = req.getParameter("email");
        PrintWriter out = resp.getWriter();
        ResetPasswordDAOImpl changePasswordDAO = new ResetPasswordDAOImpl();
        int result = changePasswordDAO.resetPassword(email,password);
        out.print(result);

    }
}
