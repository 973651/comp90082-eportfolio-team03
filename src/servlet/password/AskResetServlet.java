package servlet.password;

import util.EncodeUtils;
import util.MailUtils;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Properties;

@WebServlet(urlPatterns ="/AskResetServlet")
public class AskResetServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        String email = req.getParameter("email");
        System.out.println("------------------");
        System.out.println(email);
//        String encodeEmail= EncodeUtils.encode(email);
        System.out.println("after encoding the mail is:"+email);
        MailUtils mailUtils = new MailUtils();
//        mailUtils.sendMail(email,"Click here to reset your password","Reset Password");

        boolean result = mailUtils.sendMail(email,"<p>\n" +
                " <span style=\"font-family:Helvetica, &quot;Microsoft Yahei&quot;, verdana;font-size:14px;\"> Dear User，&nbsp;</span><br />\n" +
                "<br />\n" +
                "<span style=\"font-family:Helvetica, &quot;Microsoft Yahei&quot;, verdana;font-size:14px;\">Recently we received a request to reset your password. &nbsp;</span><br />\n" +
                "<br />\n" +
                "<span style=\"font-family:Helvetica, &quot;Microsoft Yahei&quot;, verdana;font-size:14px;\">If this your request, please click the following link to reset your password：&nbsp;</span><br />\n" +
                "<a href=\"http://localhost:8080/webpage/password/reset-password.html?keyword="+email+"\">click here to reset your password</a><span style=\"font-family:Helvetica, &quot;Microsoft Yahei&quot;, verdana;font-size:14px;\">&nbsp;</span><br />\n" +
                "<br />\n" +
                "<br />\n" +
                "<span style=\"font-family:Helvetica, &quot;Microsoft Yahei&quot;, verdana;font-size:14px;\">If this is not your request, please check your account security. &nbsp;</span><br />\n" +
                "<br />\n" +
                "<span style=\"font-family:Helvetica, &quot;Microsoft Yahei&quot;, verdana;font-size:14px;\">Thanks &nbsp;</span><br />\n" +
                "<span><span style=\"font-size:14px;\">Team 03</span></span>\n" +
                "</p >","Reset Password");
        System.out.println(result);

        if (result){
            resp.getWriter().print("success");
        }else{
            resp.getWriter().print("failed");
        }

    }

}
