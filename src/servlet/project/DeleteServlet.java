package servlet.project;

import dao.implementation.ProjectDAOImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/5/26 下午5:24
 */

@WebServlet(name = "DeleteServlet", urlPatterns = "/DeleteServlet")
public class DeleteServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        String type = req.getParameter("type");
        int userid = Integer.parseInt(req.getParameter("userId"));
        int projectid = Integer.parseInt(req.getParameter("projectId"));
        ProjectDAOImpl pro = new ProjectDAOImpl();

        if (type.equals("deleteProject")){

            int res = pro.deleteAProject(userid, projectid);
            if(res == -1){
                resp.getWriter().print("failed");
            } else{
                resp.getWriter().print("success");
            }
        }else if (type.equals("deleteImage")){
            int deleteIndex = Integer.parseInt(req.getParameter("index"));
            String imageStr = req.getParameter("images");
            String[] imageList = imageStr.split(";;;");
            String newImgList = "";
            for (int i = 0; i < imageList.length; i++){
                if (i != deleteIndex){
                    newImgList = newImgList + imageList[i] + ";;;";
                }
            }

            int res = pro.deleteAImage(userid, projectid, newImgList);
            if(res == -1){
                resp.getWriter().print("failed");
            } else{
                resp.getWriter().print("success");
            }
        }else if (type.equals("deleteFile")){
            int deleteIndex = Integer.parseInt(req.getParameter("index"));
            String videoStr = req.getParameter("videos");
            String docStr = req.getParameter("docs");
            String fileType = req.getParameter("fType");
            String[] videoList = videoStr.split(";;;");
            String[] docList = docStr.split(";;;");
            String newList = "";
            if (fileType.equals("video")){
                for (int i = 0; i < videoList.length; i++){
                    if (i != deleteIndex){
                        newList = newList + videoList[i] + ";;;";
                    }
                }
            }else{
                for (int i = 0; i < docList.length; i++){
                    if (i != deleteIndex){
                        newList = newList + docList[i] + ";;;";
                    }
                }
            }

            int res = pro.deleteAFile(userid, projectid, newList, fileType);
            if(res == -1){
                resp.getWriter().print("failed");
            } else{
                resp.getWriter().print("success");
            }
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
