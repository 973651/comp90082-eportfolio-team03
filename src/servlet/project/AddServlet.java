package servlet.project;

import bean.Project;
import dao.implementation.ProjectDAOImpl;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(name = "AddServlet", urlPatterns = "/AddServlet")
public class AddServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        System.out.println("start to stor a new project");

        int userId = Integer.parseInt(request.getParameter("userid"));
        String projectName = request.getParameter("title");
        String description = request.getParameter("description");
        String content = request.getParameter("content");
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");
        String projectimg = request.getParameter("img");
        String projectvideo = request.getParameter("vedio");
        String document = request.getParameter("otherfile");
        String tag = request.getParameter("tag");
        int visibility = Integer.parseInt(request.getParameter("visibility"));

//        System.out.println("1 " + userId);
//        System.out.println("2 " + projectName);
//        System.out.println("3 " + description);
//        System.out.println("4 " + content);
        System.out.println("5 " + startTime);
        System.out.println("6 " + endTime);
//        System.out.println("7 " + projectimg);
//        System.out.println("8 " + projectvideo);
//        System.out.println("9 " + document);
//        System.out.println("10 " + tag);
//        System.out.println("11 " + visibility);


        Project p = new Project();
        p.setUserid(userId);
        p.setProjectname(projectName);
        p.setProjectdescription(description);
        p.setContent(content);
        p.setProjectimg(projectimg);
        p.setProjectvideo(projectvideo);
        p.setDocument(document);
        p.setVisibility(visibility);
        p.setVisibleto("");
        if (startTime.equals("") && !endTime.equals("")){
            p.setStarttime(StringUtils.stringToDate("9999-12-31"));
            p.setEndtime(StringUtils.stringToDate(endTime));
        }else if (!startTime.equals("") && endTime.equals("")){
            p.setStarttime(StringUtils.stringToDate(startTime));
            p.setEndtime(StringUtils.stringToDate("9999-12-31"));
        }else if (!startTime.equals("") && !endTime.equals("")){
            p.setStarttime(StringUtils.stringToDate(startTime));
            p.setEndtime(StringUtils.stringToDate(endTime));
        }else if (startTime.equals("") && endTime.equals("")){
            p.setStarttime(StringUtils.stringToDate("9999-12-31"));
            p.setEndtime(StringUtils.stringToDate("9999-12-31"));
        }

        if (tag.equals("")){
//            System.out.println("no tag project");
            ProjectDAOImpl projectDao = new ProjectDAOImpl();
            int res = projectDao.addProject(p);
            if(res == 1){
                response.getWriter().print("success");
            } else{
                response.getWriter().print("failed");
            }
        }else{
            String[] tagList = tag.split(";;;");
            ProjectDAOImpl projectDao = new ProjectDAOImpl();
            int res = projectDao.addProject(p);
            int res2 = 0;
            if(res == 1){
//                System.out.println("acquire project id");
                Project result = projectDao.getSpecificProject(userId, projectName);
//                System.out.println("project id is");
                int projectId = result.getProjectid();
                System.out.println(projectId);
                for (int i = 0; i < tagList.length; i ++){
                    System.out.println(tagList[i]);
                    res2 += projectDao.addTag(userId, projectId, tagList[i]);
                }

            } else{
                response.getWriter().print("failed");
            }

            if(res2 == tagList.length){
                response.getWriter().print("success");
            } else{
                response.getWriter().print("failed");
            }
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}

