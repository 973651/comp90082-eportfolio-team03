package servlet.project;

import bean.Tag;
import dao.implementation.ProjectDAOImpl;
import net.sf.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/5/23 下午10:02
 */
@WebServlet(urlPatterns = "/query.tag")
public class TagServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("load servlet success");
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        System.out.println("start to stor all tags");

        int userId = Integer.parseInt(req.getParameter("userid"));
        String requestT = req.getParameter("type");

        String requestAllTag = "queryAllTag";
        if (requestAllTag.equals(requestT)){
            System.out.println(requestAllTag);
            ProjectDAOImpl peopleIml = new ProjectDAOImpl();
            ArrayList<Tag> tagList = peopleIml.getOnesAllProjectTag(userId);
            String json = JSONArray.fromObject(tagList).toString();
            System.out.println(json);
            out.println(json);
        }
        String queryOneProjectTag = "queryOneProjectTag";
        if (queryOneProjectTag.equals(requestT)){
            System.out.println(queryOneProjectTag);
            int projectid = Integer.parseInt(req.getParameter("projectId"));
            ProjectDAOImpl projectIml = new ProjectDAOImpl();
            ArrayList<Tag> tagList = projectIml.getOneProjectTags(userId, projectid);
            String json = JSONArray.fromObject(tagList).toString();
            System.out.println(json);
            out.println(json);
        }

        if(requestT.equals("getOnesAllVisibleTag")){
            ProjectDAOImpl peopleIml = new ProjectDAOImpl();
            ArrayList<Tag> tagList = peopleIml.getOnesAllVisibleTag(userId);
            String json = JSONArray.fromObject(tagList).toString();
            System.out.println(json);
            out.println(json);
        }
    }
}
