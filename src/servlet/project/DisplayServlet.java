package servlet.project;

import bean.Project;
import bean.Tag;
import dao.ProjectDAO;
import dao.implementation.ProjectDAOImpl;
import net.sf.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import net.sf.json.JSONObject;
import util.StringUtils;

@WebServlet(name = "DisplayServlet", urlPatterns = "/DisplayServlet")
public class DisplayServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        String type = request.getParameter("type");
        ProjectDAOImpl pro = new ProjectDAOImpl();

        if(type.equals("queryAll")){
            int userId = Integer.parseInt(request.getParameter("userId"));
            List<Project> list = pro.getProjects(userId);
            for(int i=0; i<list.size(); i++){
                Project p = list.get(i);
                p.setStr_starttime(StringUtils.dateToString(p.getStarttime()));
                p.setStr_endtime(StringUtils.dateToString(p.getEndtime()));
            }
            String json = JSONArray.fromObject(list).toString();
            response.getWriter().print(json);
        } else if(type.equals("queryOne")){
            int projectId = Integer.parseInt(request.getParameter("projectId"));
            Project result = pro.getOneProject(projectId);
            result.setStr_starttime(StringUtils.dateToString(result.getStarttime()));
            result.setStr_endtime(StringUtils.dateToString(result.getEndtime()));
            String json = JSONArray.fromObject(result).toString();
            response.getWriter().print(json);
        }else if (type.equals("queryProjectByTag")){
            int userId = Integer.parseInt(request.getParameter("userId"));
            String tag = request.getParameter("tag");
            if (tag.equals("All")){
                List<Project> list = pro.getProjects(userId);
                for(int i=0; i<list.size(); i++){
                    Project p = list.get(i);
                    p.setStr_starttime(StringUtils.dateToString(p.getStarttime()));
                    p.setStr_endtime(StringUtils.dateToString(p.getEndtime()));
                }
                String json = JSONArray.fromObject(list).toString();
                response.getWriter().print(json);
            }else{
                ArrayList<Tag> taglist = pro.getProjectByTag(userId, tag);
                ArrayList<Project> projects = new ArrayList<>();
                for (Tag t : taglist){
                    Project p = pro.getOneProject(t.getProjectid());
                    p.setStr_starttime(StringUtils.dateToString(p.getStarttime()));
                    p.setStr_endtime(StringUtils.dateToString(p.getEndtime()));
                    projects.add(p);
                }
                String json = JSONArray.fromObject(projects).toString();
                response.getWriter().print(json);
            }

        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
