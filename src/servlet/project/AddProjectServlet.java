package servlet.project;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/5/14 上午1:27
 */
import bean.Project;
import dao.implementation.ProjectDAOImpl;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/add.project")
public class AddProjectServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=UTF-8");

        PrintWriter out = resp.getWriter();
        String requestT = req.getParameter("type");
        String title = req.getParameter("title");
        String content = req.getParameter("content");
        String description = req.getParameter("description");
        System.out.println(requestT);

        System.out.println(title);
        System.out.println(content);
        System.out.println(description);

        Project project = new Project();
        project.setProjectname(title);
        project.setProjectdescription(description);
//        project.setProjectcontent(content);
        String requestAll = "upload_project";
        if (requestAll.equals(requestT)){
            ProjectDAOImpl peopleIml = new ProjectDAOImpl();
            int json = peopleIml.addProject(project);
            out.println(json);
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
