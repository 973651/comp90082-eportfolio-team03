package servlet.experience;

import bean.Experience;
import bean.Project;
import dao.ExperienceDAO;
import dao.implementation.ExperienceDAOImpl;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ExperienceServlet", urlPatterns = "/ExperienceServlet")
public class ExperienceShowServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      req.setCharacterEncoding("utf-8");
      resp.setContentType("text/html;charset=utf-8");
      int userId = Integer.parseInt(req.getParameter("userId"));
      System.out.println("query experience info of " + userId);
      ExperienceDAOImpl exp = new ExperienceDAOImpl();
      List<Experience> list = exp.getExperience(userId);
      for(int i=0; i<list.size(); i++){
          Experience e= list.get(i);
          e.setStr_starttime(StringUtils.dateToString(e.getStarttime()));
          e.setStr_endtime(StringUtils.dateToString(e.getEndtime()));
      }
      String json = JSONArray.fromObject(list).toString();
      System.out.println(json);

      resp.getWriter().print(json);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
