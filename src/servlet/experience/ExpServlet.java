package servlet.experience;

import bean.Experience;

import dao.implementation.ExperienceDAOImpl;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name="exp.servlet.add", urlPatterns="/exp.servlet.add")
public class ExpServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("load !!!! SUCCESS!!!!!!!!!!!!!!!!!!!!!!!!!!load !!!! SUCCESS!!!!!!!!!!!!!!!!!!!!!!!!!!");
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");

        String type = req.getParameter("type");

        if (type.equals("insert_new_work")){
            int userId = Integer.parseInt(req.getParameter("userId"));
            String jobname = req.getParameter("jobname");
            String companyname = req.getParameter("company");
            String workdescription = req.getParameter("desc");
            String startTime = req.getParameter("start");
            String endTime = req.getParameter("end");
            System.out.println(startTime);
            System.out.println(endTime);

            ExperienceDAOImpl expImpl = new ExperienceDAOImpl();

            Experience e = new Experience();
            e.setUserid(userId);
            e.setJobname(jobname);
            e.setCompany(companyname);
            e.setWorkdescription(workdescription);

            if (startTime.equals("") && !endTime.equals("")){
                e.setStarttime(StringUtils.stringToDate("9999-12-31"));
                e.setEndtime(StringUtils.stringToDate(endTime));
            }else if (!startTime.equals("") && endTime.equals("")){
                e.setStarttime(StringUtils.stringToDate(startTime));
                e.setEndtime(StringUtils.stringToDate("9999-12-31"));
            }else if (!startTime.equals("") && !endTime.equals("")){
                e.setStarttime(StringUtils.stringToDate(startTime));
                e.setEndtime(StringUtils.stringToDate(endTime));
            }else if (startTime.equals("") && endTime.equals("")){
                e.setStarttime(StringUtils.stringToDate("9999-12-31"));
                e.setEndtime(StringUtils.stringToDate("9999-12-31"));
            }
            System.out.println("start process");

            int res = expImpl.addExperience(e);
//        System.out.println("input++++++++++"+userId);
            if(res == 1){
                resp.getWriter().print("success");
            } else{
                resp.getWriter().print("failed");
            }
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
