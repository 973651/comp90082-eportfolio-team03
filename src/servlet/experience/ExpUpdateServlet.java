package servlet.experience;

import bean.Experience;
import dao.implementation.ExperienceDAOImpl;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/5/29 上午1:54
 */
@WebServlet(name="exp.servlet.update", urlPatterns="/exp.servlet.update")
public class ExpUpdateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");

        String type = req.getParameter("type");
        ExperienceDAOImpl eDao = new ExperienceDAOImpl();

        if (type.equals("delete")){
            int workid = Integer.parseInt(req.getParameter("workid"));
            int res = eDao.deleteExp(workid);
            if(res == -1){
                resp.getWriter().print("failed");
            } else{
                resp.getWriter().print("success");
            }
        }

        if (type.equals("edit")){
            int expid = Integer.parseInt(req.getParameter("expid"));
            String company = req.getParameter("company");
            String job = req.getParameter("job");
            String startTime = req.getParameter("starttime");
            String endTime = req.getParameter("endtime");
            String des = req.getParameter("des");

            Experience exp = new Experience();
            exp.setWorkid(expid);
            exp.setCompany(company);
            exp.setJobname(job);
            exp.setWorkdescription(des);

            if (startTime.equals("") && !endTime.equals("")){
                exp.setStarttime(StringUtils.stringToDate("9999-12-31"));
                exp.setEndtime(StringUtils.stringToDate(endTime));
            }else if (!startTime.equals("") && endTime.equals("")){
                exp.setStarttime(StringUtils.stringToDate(startTime));
                exp.setEndtime(StringUtils.stringToDate("9999-12-31"));
            }else if (!startTime.equals("") && !endTime.equals("")){
                exp.setStarttime(StringUtils.stringToDate(startTime));
                exp.setEndtime(StringUtils.stringToDate(endTime));
            }else if (startTime.equals("") && endTime.equals("")){
                exp.setStarttime(StringUtils.stringToDate("9999-12-31"));
                exp.setEndtime(StringUtils.stringToDate("9999-12-31"));
            }


            int res = eDao.editExp(exp);
            if(res == -1){
                resp.getWriter().print("failed");
            } else{
                resp.getWriter().print("success");
            }
        }
    }
}
