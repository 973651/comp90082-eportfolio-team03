package servlet.award;

import bean.Award;
import dao.implementation.AwardDAOImpl;
import net.sf.json.JSONArray;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

@WebServlet(name = "AwardsServlet", urlPatterns = "/AwardsServlet")
public class AwardsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        String type = request.getParameter("type");
        PrintWriter out = response.getWriter();
        AwardDAOImpl aDao = new AwardDAOImpl();
        System.out.println("servlet access success!");

        if(type.equals("add")){
            int userId = Integer.parseInt(request.getParameter("userId"));
            int infoId = Integer.parseInt(request.getParameter("infoId"));
            String awardName = request.getParameter("awardName");
            String description = request.getParameter("description");
            Date awardTime = StringUtils.stringToDate(request.getParameter("awardTime"));

            Award award = new Award();
            award.setUserid(userId);
            award.setInfoid(infoId);
            award.setAwardname(awardName);
            award.setAwarddescription(description);
            award.setAwardtime(awardTime);

            int res = aDao.addAward(award);
            if(res == -1){
                response.getWriter().print("failed");
            } else{
                response.getWriter().print("success");
            }

        } else if(type.equals("query")){
            int userId = Integer.parseInt(request.getParameter("userId"));

            List<Award> list = aDao.getAwards(userId);
            for(int i = 0; i < list.size(); i++) {
                Award a = list.get(i);
                a.setStr_awardtime(StringUtils.dateToString(a.getAwardtime()));
            }

            String json = JSONArray.fromObject(list).toString();
            out.print(json);
        } else if(type.equals("edit")){
            int awardId = Integer.parseInt(request.getParameter("awardId"));
            String awardName = request.getParameter("awardName");
            String description = request.getParameter("description");
            Date awardTime = StringUtils.stringToDate(request.getParameter("awardTime"));

            Award award = new Award();
            award.setAwardid(awardId);
            award.setAwardname(awardName);
            award.setAwarddescription(description);
            award.setAwardtime(awardTime);

            int res = aDao.editAward(award);
            if(res == -1){
                response.getWriter().print("failed");
            } else{
                response.getWriter().print("success");
            }
        } else if(type.equals("delete")){
            int awardId = Integer.parseInt(request.getParameter("awardId"));
            int res = aDao.deleteAward(awardId);
            if(res == -1){
                response.getWriter().print("failed");
            } else{
                response.getWriter().print("success");
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
