package servlet.privacy;

import bean.Project;
import bean.Request;
import dao.implementation.PrivacyDAOImpl;
import net.sf.json.JSONArray;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

@WebServlet(name = "PrivacyServlet", urlPatterns = "/PrivacyServlet")
public class PrivacyServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        String type = request.getParameter("type");
        PrintWriter out = response.getWriter();
        PrivacyDAOImpl privacyDAO = new PrivacyDAOImpl();

        if(type.equals("setVisibility")){
            int projectId = Integer.parseInt(request.getParameter("projectId"));
            int visibility = Integer.parseInt(request.getParameter("visibility"));
            int res = privacyDAO.setPrivacy(projectId, visibility);
            if(res == 1){
                out.print("success");
            } else{
                out.print("failed");
            }
        } else if(type.equals("setVisibleTo")){
            int projectId = Integer.parseInt(request.getParameter("projectId"));
            String currentVisibleTo = request.getParameter("toUserList");
            String newList = currentVisibleTo + request.getParameter("toUser");
            int res = privacyDAO.addVisibleUsers(projectId, newList);
            if(res == 1){
                out.print("success");
            } else{
                out.print("failed");
            }
        } else if(type.equals("getProjects")){
            int userId = Integer.parseInt(request.getParameter("userId"));
            List<Project> list = privacyDAO.getRequestedProjects(userId);
            String json = JSONArray.fromObject(list).toString();
            out.print(json);
        } else if(type.equals("sentRequest")){
            int projectId = Integer.parseInt(request.getParameter("projectId"));
            int userId = Integer.parseInt(request.getParameter("userId"));
            Date requestTime = StringUtils.stringToDate(request.getParameter("requestTime"));
            String content = request.getParameter("requestContent");

            Request privacyRequest = new Request();
            privacyRequest.setProjectid(projectId);
            privacyRequest.setRequestfrom(userId);
            privacyRequest.setRequesttime(requestTime);
            privacyRequest.setRequestcontent(content);

            int res = privacyDAO.addRequest(privacyRequest);
            if(res == -1){
                out.print("failed");
            } else{
                out.print("success");
            }
        } else if(type.equals("getRequests")){
            int projectId = Integer.parseInt(request.getParameter("projectId"));
            List<Request> list = privacyDAO.getRequests(projectId);
            for(Request r: list){
                r.setStr_requesttime(StringUtils.dateToString(r.getRequesttime()));
            }
            String json = JSONArray.fromObject(list).toString();
            out.print(json);
        } else if(type.equals("getMyRequests")){
            int userId = Integer.parseInt(request.getParameter("userId"));
            List<Request> list = privacyDAO.getMyRequests(userId);
            for(Request r: list){
                r.setStr_requesttime(StringUtils.dateToString(r.getRequesttime()));
            }
            String json = JSONArray.fromObject(list).toString();
            out.print(json);
        } else if(type.equals("processRequest")){
            int projectId = Integer.parseInt(request.getParameter("projectId"));
            int status = Integer.parseInt(request.getParameter("status"));
            String usersStr = request.getParameter("users");
            String[] users = usersStr.split(",");
            for(String u: users){
                privacyDAO.processRequest(projectId, Integer.parseInt(u), status);
            }
            out.print("success");
        } else if(type.equals("deleteRequest")){
            int requestId = Integer.parseInt(request.getParameter("requestId"));
            int res = -1;
            res = privacyDAO.cancelRequest(requestId);
            if(res == -1){
                out.print("failed");
            } else{
                out.print("success");
            }
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
