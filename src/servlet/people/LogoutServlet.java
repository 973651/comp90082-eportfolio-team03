package servlet.people;

import bean.User;
import dao.implementation.LogoutDAOImpl;
import dao.implementation.PeopleDAOImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/6/4 下午11:44
 */

@WebServlet(urlPatterns = "/delete.account")
public class LogoutServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=UTF-8");

        PrintWriter out = resp.getWriter();
        int userId = Integer.parseInt(req.getParameter("userId"));
        String type = req.getParameter("type");

        String delete_account = "delete_account";
        if (type.equals(delete_account)){
            LogoutDAOImpl logoutDAO = new LogoutDAOImpl();
            int updateResult = logoutDAO.deleteAllInfo(userId);
            out.print(updateResult);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
