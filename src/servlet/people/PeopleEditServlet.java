package servlet.people;

import bean.User;
import dao.implementation.PeopleDAOImpl;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/4/23 下午9:48
 */

@WebServlet(urlPatterns = "/edit.people")
public class PeopleEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=UTF-8");

        PrintWriter out = resp.getWriter();
        PeopleDAOImpl peopleDAO = new PeopleDAOImpl();
        System.out.println("start to process query");

        int userId = Integer.parseInt(req.getParameter("userId"));

        String type = req.getParameter("type");

        String changePassword = "change_pwd";
        if (type.equals(changePassword)){

            String password = req.getParameter("password");
            System.out.println("update "+ userId+"'s password");
            User user = new User();
            user.setUserid(userId);
            user.setPassword(password);
            System.out.println(password);
            int updateResult = peopleDAO.updateUserPwdById(user);
            out.print(updateResult);
        }

        String validatePwd = "auth_password";
        if (type.equals(validatePwd)){
            String password = req.getParameter("password");
            System.out.println("check password of "+userId);
            String oldPassword = peopleDAO.queryPasswordById(userId);
            if (password.equals(oldPassword)){
                out.print(1);
            }else{
                out.print(0);
            }
        }

        String queryInfo = "query_info";
        if (type.equals(queryInfo)){
            System.out.println("query infor of " + userId);
            User user = new User();
            user.setUserid(userId);
            User info = peopleDAO.queryInfoById(user);
            String dob = StringUtils.dateToString(info.getDob());
            info.setStr_dob(dob);
            JSONArray result = JSONArray.fromObject(info);
            System.out.println(result.toString());
            out.print(result.toString());
        }

        String queryAvatar = "query_avatar";
        if (type.equals(queryAvatar)){
            System.out.println("start to get user avatar");
            User avatar = peopleDAO.queryAvatarById(userId);
            JSONArray result = JSONArray.fromObject(avatar);
            System.out.println(result.toString());
            out.print(result.toString());
        }

        String modifyInfo = "modify_info";
        if (type.equals(modifyInfo)) {
            System.out.println("modify infomation of " + userId);
            String description = req.getParameter("description");
            String address = req.getParameter("address");
            String dobstr = req.getParameter("dob");
            String username = req.getParameter("userName");
            Date dob = StringUtils.stringToDate(dobstr);
            int phone = Integer.parseInt(req.getParameter("phone"));
            int id = Integer.parseInt(req.getParameter("userId"));
            String job = req.getParameter("job");
            String major = req.getParameter("major");
            int gender = Integer.parseInt(req.getParameter("gender"));
            User user = new User();
            user.setUserid(id);
            user.setName(username);
            user.setDob(dob);
            user.setAddress(address);
            user.setPhone(phone);
            user.setDescription(description);
            user.setJob(job);
            user.setMajor(major);
            user.setGender(gender);


            int updateResult = peopleDAO.updateUserInfoById(user);
            if (updateResult == 1){
                System.out.println("Successful !!!");
            }
            out.print(updateResult);

            return;
        }

        String modifyPhoto = "modify_photo";
        if (type.equals(modifyPhoto)){
            int id = Integer.parseInt(req.getParameter("userId"));
            String photo = req.getParameter("photo");
            peopleDAO.updatePhotoById(id, photo);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
