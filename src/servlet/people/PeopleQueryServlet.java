/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/4/21 下午10:24
 */

package servlet.people;

import bean.User;
import dao.implementation.PeopleDAOImpl;
import net.sf.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.ArrayList;


@WebServlet(urlPatterns = "/query.people")
public class PeopleQueryServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=UTF-8");

        PrintWriter out = resp.getWriter();
        String requestT = req.getParameter("type");

        User newUser = new User();
        //query all user information which satisfied the keywords and two filters
        String requestAll = "query_all";
        if (requestAll.equals(requestT)){
            String requestK = req.getParameter("keywords");
            String filter1 = req.getParameter("fil1");
            String filter2 = req.getParameter("fil2");
            requestK = (requestK == null ? "" : URLDecoder.decode(requestK, "utf-8"));
            newUser.setName(requestK);
            if (filter1.equals("0") && filter2.equals("0")){
                PeopleDAOImpl peopleIml = new PeopleDAOImpl();
                ArrayList<User> userList = peopleIml.searchPeopleByName(newUser);
                String json = JSONArray.fromObject(userList).toString();
                out.print(json);
            }else if (filter1.equals("0") && !filter2.equals("0")){
                newUser.setJob(filter2);
                PeopleDAOImpl peopleIml = new PeopleDAOImpl();
                ArrayList<User> userList = peopleIml.searchPeopleByNameAndJob(newUser);
                String json = JSONArray.fromObject(userList).toString();
                out.print(json);
            }else if (filter2.equals("0") && !filter1.equals("0")){
                newUser.setMajor(filter1);
                PeopleDAOImpl peopleIml = new PeopleDAOImpl();
                ArrayList<User> userList = peopleIml.searchPeopleByNameAndMajor(newUser);
                String json = JSONArray.fromObject(userList).toString();
                out.print(json);
            }else if (!filter1.equals("0") && !filter2.equals("0")){
                newUser.setMajor(filter1);
                newUser.setJob(filter2);
                PeopleDAOImpl peopleIml = new PeopleDAOImpl();
                ArrayList<User> userList = peopleIml.searchPeopleByNameAndBothFilter(newUser);
                String json = JSONArray.fromObject(userList).toString();
                out.print(json);
            }

            return;
        }
        // load recommend user (without current user!)
        String requestRecommend = "query_recommend";
        if (requestRecommend.equals(requestT)){
            int userid = Integer.parseInt(req.getParameter("userId"));
            if (userid == 0){
                PeopleDAOImpl peopleDAO = new PeopleDAOImpl();
                String sql = "select * from user";
                ArrayList<User> recommendList = peopleDAO.queryRecommendUser(sql);
                String json = JSONArray.fromObject(recommendList).toString();
                out.print(json);
            }else{
                PeopleDAOImpl peopleDAO = new PeopleDAOImpl();
                String sql = "select * from user where userid != '" + userid +"'";
                ArrayList<User> recommendList = peopleDAO.queryRecommendUser(sql);
                String json = JSONArray.fromObject(recommendList).toString();
                out.print(json);
            }

            return;
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
