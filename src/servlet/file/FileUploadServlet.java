package servlet.file;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;
import com.oreilly.servlet.multipart.FileRenamePolicy;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import util.OSSClientUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/5/14 下午9:59
 */
@WebServlet(urlPatterns = "/large.file.upload")
public class FileUploadServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Start to process upload large file");
        String filename = null;
        String filepath = null;
        ArrayList<String> urls = new ArrayList<>();
        String fileType = "";

        String requextip = req.getRemoteAddr();
        String saveDirectory = this.getServletContext().getRealPath("") + "\\img";
        File savedir = new File(saveDirectory);
        System.out.println(savedir);
        if (!savedir.exists())
            savedir.mkdir();
        int maxPostSize = 1024 * 1024 * 1024;
        FileRenamePolicy policy = (FileRenamePolicy) new DefaultFileRenamePolicy();
        MultipartRequest multi;
        multi = new MultipartRequest(req, saveDirectory, maxPostSize, "UTF-8", policy);
        Enumeration<String> files = multi.getFileNames();
        System.out.println(files.toString() + "1111");
        while (files.hasMoreElements()){
            String name = files.nextElement();
            File f = multi.getFile(name);
            if (f != null) {
                filename = f.getName();
                filepath = f.getPath();
            }
            System.out.println(filename + " and " +filepath);
            FileInputStream in = new FileInputStream(f);
            OSSClientUtils ossUtil = new OSSClientUtils();
            String url = ossUtil.uploadFileToOSS(filename, filepath);
            String pathname = saveDirectory + "/" + filename;
            File file = new File(pathname);
            if (file.isFile() && file.exists()) {
                System.out.println("delete file");
                file.delete();
            }
            System.out.println(url);
            PrintWriter out = resp.getWriter();
            out.print(url);
        }


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
