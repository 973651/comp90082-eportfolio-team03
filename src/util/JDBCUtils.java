package util;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.*;

public class JDBCUtils {
    private static String url;
    private static String user;
    private static String password;
    private static String dirver;

    private Connection connection;
    private PreparedStatement pstmt;
    private ResultSet resultSet;

    static{
        //读取资源文件，获取值
        try {
            //1.创建properties集合类
            Properties pro = new Properties();
            //2.获取src路径下的文件方式：Classloader 类加载器
            ClassLoader classloader = JDBCUtils.class.getClassLoader();
            URL res = classloader.getResource("jdbc.properties");
            String path = res.getPath();
            //3.加载文件
            pro.load(new FileReader(path));
            //4.获取数据，赋值
            url = pro.getProperty("url");
            user = pro.getProperty("user");
            password = pro.getProperty("password");
            dirver = pro.getProperty("driver");
            //5.注册驱动
            Class.forName(dirver);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取连接
     * @return 连接对象
     */
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }


    /**
     * 释放资源
     * @param ress
     * @param stat
     * @param conn
     */
    public static void close(ResultSet ress, Statement stat, Connection conn){
        if(ress != null){
            try {
                ress.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(stat != null){
            try {
                stat.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(conn != null){
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(Statement stat, Connection conn){
        if(stat != null){
            try {
                stat.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(conn != null){
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    
    /**
     * This method is used to find simple result from DataBase.
     * @param sql(sql statement)
     * @param params
     * @return A Key-Value map which contains a simple result acquired from DB
     * @throws SQLException
     */
    public Map<String, String> findSimpleResult(String sql, List<Object> params) throws SQLException{
        Map<String, String> map = new LinkedHashMap<String, String>();
        int index =1;
        pstmt = connection.prepareStatement(sql);
        if(params!=null && !params.isEmpty()){
            for (int i =0;i<params.size();i++){
                pstmt.setObject(index++, params.get(i));
            }
        }
        resultSet = pstmt.executeQuery();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int cols_len = metaData.getColumnCount();
        while(resultSet.next()){
            for(int i=0;i<cols_len;i++){
                String cols_name = metaData.getColumnName(i+1);
                String cols_value = resultSet.getString(cols_name);
                if(cols_value==null){
                    cols_value = "";
                }
                map.put(cols_name,cols_value);
            }
        }

        return map;
    }

    /**
     * This method is used to find more results from DataBase.
     * @param sql
     * @param params
     * @return A list of Key-Value map, which contains several results acquired from DB
     * @throws SQLException
     */
    public List<Map<String, Object>> findMoreResult(String sql, List<Object> params) throws SQLException{

        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        int index =1;
        pstmt = connection.prepareStatement(sql);
        if(params!=null && !params.isEmpty()){
            for (int i =0;i<params.size();i++){
                pstmt.setObject(index++, params.get(i));
            }
        }
        resultSet = pstmt.executeQuery();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int cols_len = metaData.getColumnCount();
        while(resultSet.next()){
            Map<String, Object> map = new LinkedHashMap<String, Object>();
            for(int i=0;i<cols_len;i++){
                String cols_name = metaData.getColumnName(i+1);
                Object cols_value = resultSet.getObject(cols_name);

                if (cols_value ==null){
                    cols_value = "";
                }
                map.put(cols_name, cols_value);

            }
            list.add(map);
        }
        return list;
    }


}
