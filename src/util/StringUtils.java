package util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class contains methods about String
 * Including transform between Date ans String, ...
 */
public class StringUtils {
    /**
     * Convert String date to Date type
     * @param time must in format "yyyy-MM-dd"
     * @return Date
     */
    public static Date stringToDate(String time) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * Convert Date to String
     * @param time Date
     * @return String in format "yyyy-MM-dd"
     */
    public static String dateToString(Date time){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(time);
    }
}
