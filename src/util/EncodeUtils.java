package util;

public class EncodeUtils {
    public static void main(String[] args) {
        String email="firecaixinquan@gmail.com";
        System.out.println("输入邮箱为："+email);
        String answer = EncodeUtils.encode(email);
        System.out.println("加密后为："+answer);
        String result = EncodeUtils.encode(answer);
        System.out.println("解密后为："+result);

    }

    public static String encode(String input){
        char[] array = input.toCharArray();
        for(int i=0;i<array.length;i++){
            array[i]=(char)(array[i]^2);
        }
        return new String(array);
    }

}
