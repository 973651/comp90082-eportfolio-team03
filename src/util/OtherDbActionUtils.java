/**
 * @author Boyan XIN
 * @version 1.1 Debug of SetParamUnits in line 58.
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/4/21 下午9:27
 */

package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class OtherDbActionUtils {

    /**
     * This method is used to insert data into DB
     * @param sql
     * @param elements
     * @return a int value that represent whether the manipulation is success
     * @throws SQLException
     */
    public static int insertIntoDB(String sql, ArrayList<Object> elements) throws SQLException {
        return otherDbAction(sql, elements);
    }

    /**
     * This method is used to update data that already in DB
     * @param sql
     * @param elements
     * @return a int value that represent whether the manipulation is success
     * @throws SQLException
     */
    public static int updataDataInDB(String sql, ArrayList<Object> elements) throws  SQLException{
        return otherDbAction(sql, elements);
    }

    /**
     * This method is used to delete data
     * @param sql
     * @param elements
     * @return
     * @throws SQLException
     */
    public static int deleteData(String sql, ArrayList<Object> elements) throws SQLException{
        return otherDbAction(sql, elements);
    }

    /**
     * Other action (except query action)
     * @param sql
     * @param elements
     * @return a int value that represent whether the manipulation is success
     * @throws SQLException
     */
    public static int otherDbAction(String sql, ArrayList<Object> elements) throws SQLException {
        Connection connection = JDBCUtils.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        int result = 0;

        try{
            ps = connection.prepareStatement(sql);
            if (elements != null && elements.size() != 0){
                for (int i = 0; i < elements.size(); i++){
                    SetParmUtils.setParamInit(i + 1, elements.get(i), ps);
                    System.out.println(ps);
                }
            }

            result = ps.executeUpdate();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            JDBCUtils.close(rs, ps, connection);
        }

        return result;
    }

}
