package util;

import com.aliyun.oss.model.*;
import helper.PutObjectProgressHelper;
import org.apache.commons.lang.StringUtils;

import com.aliyun.oss.OSSClient;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/4/27 下午11:33
 */
public class OSSClientUtils {

    private static String endpoint = "http://oss-cn-shenzhen.aliyuncs.com";
    private static String accessKeyId = "LTAI4G9C1zsnwaDG9eZKSjKv";
    private static String accessKeySecret = "8ZJ5u6pBghn0QZaZfSuZ1aBrzIeVVu";
    private static String bucketName = "comp90082-image-storage";
    private static String folder = "image/";
    private static String key = "http://comp90082-image-storage.oss-cn-shenzhen.aliyuncs.com/";

    public String uploadImageToOSS(String fileName, InputStream inputStream) {

        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        String uuid = "";
        String name = fileName;
        // determine the folder name.

        try {
            System.out.println(folder + name);
            ossClient.putObject(new PutObjectRequest(bucketName, folder + name, inputStream).
                    <PutObjectRequest>withProgressListener(new PutObjectProgressHelper()));
            return key + folder + name;
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        } finally {
            ossClient.shutdown();
        }
        return null;
    }

    public String uploadFileToOSS(String fileName, String path) throws IOException {
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        System.out.println(fileName);
        System.out.println(path);
        //创建multipart upload对象
        // 创建InitiateMultipartUploadRequest对象。
        InitiateMultipartUploadRequest request = new InitiateMultipartUploadRequest(bucketName, "files/"+fileName);

        // 如果需要在初始化分片时设置文件存储类型，请参考以下示例代码。
        // ObjectMetadata metadata = new ObjectMetadata();
        // metadata.setHeader(OSSHeaders.OSS_STORAGE_CLASS, StorageClass.Standard.toString());
        // request.setObjectMetadata(metadata);

        // 初始化分片。
        InitiateMultipartUploadResult upresult = ossClient.initiateMultipartUpload(request);
        // 返回uploadId，它是分片上传事件的唯一标识，您可以根据这个ID来发起相关的操作，如取消分片上传、查询分片上传等。
        String uploadId = upresult.getUploadId();

        // partETags是PartETag的集合。PartETag由分片的ETag和分片号组成。
        List<PartETag> partETags =  new ArrayList<PartETag>();
        // 计算文件有多少个分片。
        final long partSize = 5 * 1024 * 1024L;   // 1MB
        final File file = new File(path);
        long fileLength = file.length();
        int partCount = (int) (fileLength / partSize);
        if (fileLength % partSize != 0) {
            partCount++;
        }
        // 遍历分片上传。
        for (int i = 0; i < partCount; i++) {
            long startPos = i * partSize;
            long curPartSize = (i + 1 == partCount) ? (fileLength - startPos) : partSize;
            InputStream instream = new FileInputStream(file);
            // 跳过已经上传的分片。
            instream.skip(startPos);
            UploadPartRequest uploadPartRequest = new UploadPartRequest();
            uploadPartRequest.setBucketName(bucketName);
            uploadPartRequest.setKey("files/"+fileName);
            uploadPartRequest.setUploadId(uploadId);
            uploadPartRequest.setInputStream(instream);
            // 设置分片大小。除了最后一个分片没有大小限制，其他的分片最小为100KB。
            uploadPartRequest.setPartSize(curPartSize);
            // 设置分片号。每一个上传的分片都有一个分片号，取值范围是1~10000，如果超出这个范围，OSS将返回InvalidArgument的错误码。
            uploadPartRequest.setPartNumber( i + 1);
            // 每个分片不需要按顺序上传，甚至可以在不同客户端上传，OSS会按照分片号排序组成完整的文件。
            UploadPartResult uploadPartResult = ossClient.uploadPart(uploadPartRequest);
            // 每次上传分片之后，OSS的返回结果会包含一个PartETag。PartETag将被保存到partETags中。
            partETags.add(uploadPartResult.getPartETag());
        }


        // 创建CompleteMultipartUploadRequest对象。
        // 在执行完成分片上传操作时，需要提供所有有效的partETags。OSS收到提交的partETags后，会逐一验证每个分片的有效性。当所有的数据分片验证通过后，OSS将把这些分片组合成一个完整的文件。
        CompleteMultipartUploadRequest completeMultipartUploadRequest =
                new CompleteMultipartUploadRequest(bucketName, "files/"+fileName, uploadId, partETags);

        // 如果需要在完成文件上传的同时设置文件访问权限，请参考以下示例代码。
        // completeMultipartUploadRequest.setObjectACL(CannedAccessControlList.PublicRead);

        // 完成上传。
        CompleteMultipartUploadResult completeMultipartUploadResult = ossClient.completeMultipartUpload(completeMultipartUploadRequest);
        OSSClientUtils ossClientUtils = new OSSClientUtils();
        String url = key + "files/" + fileName;
        System.out.println(url);

        // 关闭OSSClient。
        ossClient.shutdown();
        return url;
    }


    private OSSClient ossClient;


    public void destory() {
        ossClient.shutdown();
    }

    public String uploadImgToOSS(String fileName, InputStream inputStream){
//        init();
        try{
            String uuid = UUID.randomUUID().toString().replaceAll("-","");
            String[] names = fileName.split("[.]");
            String name = uuid + "." + names[names.length - 1];
            ossClient.putObject(new PutObjectRequest(bucketName, folder + name, inputStream));
            return key + folder +name;
        }catch (Exception e){
            e.printStackTrace();
            System.err.println(e.getMessage());
        }finally {
            destory();
        }
        return null;
    }

    /**
     * Description: 判断OSS服务文件上传时文件的contentType 
     *
     * @param filenameExtension 文件后缀
     * @return String
      */
    public static String getcontentType(String filenameExtension) {
        if (filenameExtension.equalsIgnoreCase("bmp")) {
            return "image/bmp";
        }
        if (filenameExtension.equalsIgnoreCase("gif")) {
            return "image/gif";
        }
        if (filenameExtension.equalsIgnoreCase("jpeg") || filenameExtension.equalsIgnoreCase("jpg")
                || filenameExtension.equalsIgnoreCase("png")) {
            return "image/jpeg";
        }
        if (filenameExtension.equalsIgnoreCase("html")) {
            return "text/html";
        }
        if (filenameExtension.equalsIgnoreCase("txt")) {
            return "text/plain";
        }
        if (filenameExtension.equalsIgnoreCase("vsd")) {
            return "application/vnd.visio";
        }
        if (filenameExtension.equalsIgnoreCase("pptx") || filenameExtension.equalsIgnoreCase("ppt")) {
            return "application/vnd.ms-powerpoint";
        }
        if (filenameExtension.equalsIgnoreCase("docx") || filenameExtension.equalsIgnoreCase("doc")) {
            return "application/msword";
        }
        if (filenameExtension.equalsIgnoreCase("xml")) {
            return "text/xml";
        }
        return "image/jpeg";
    }

    /**
    * 获得图片路径
    *
    * @param fileUrl
    * @return
    */
    public String getImgUrl(String fileUrl) {
        System.out.println("fileUrl="+fileUrl);
        if (!StringUtils.isEmpty(fileUrl)) {
            String[] split = fileUrl.split("/");
            return this.getUrl(this.folder + split[split.length - 1]);
        }
        return null;
    }

    /**
    * 获得url链接 
    *
    * @param key
    * @return
    */
    public String getUrl(String key) {
        System.out.println(key);
        // 设置URL过期时间为10年 3600l* 1000*24*365*10
        Date expiration = new Date(System.currentTimeMillis() + 3600L * 1000 * 24 * 365 * 10);
        // 生成URL  
        URL url = ossClient.generatePresignedUrl(bucketName, key, expiration);
        if (url != null) {
            System.out.println("url="+url.toString());
            return url.toString();
        }
        return null;
    }







}
