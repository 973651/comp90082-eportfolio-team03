package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/4/21 下午8:51
 */

public class DbQueryUtils {

    /**
     * This method query some value like String int Date etc.
     * @param sql
     * @param elements
     * @return the query results
     */
    public static Object queryResultByElement(String sql, ArrayList<Object> elements) throws SQLException {
        Connection connection = JDBCUtils.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Object result = null;

        try{
            ps = connection.prepareStatement(sql);

            if (elements != null && elements.size() != 0){
                for (int i = 0; i < elements.size(); i++){
                    SetParmUtils.setParamInit(i+1, elements.get(i), ps);
                }
            }

            rs = ps.executeQuery();

            if (rs.next()){
                result = rs.getObject(1);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            JDBCUtils.close(rs, ps, connection);
        }

        return result;

    }

    /**
     *
     * @param sql
     * @param elements
     * @param <T>
     * @return A list of results query from DB
     * @throws SQLException
     */
    public static <T> ArrayList<T> queryBeanListByElements(String sql, ArrayList<Object> elements, Class<T> classT) throws SQLException {
        Connection connection = JDBCUtils.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<T> results = null;

        try{
            ps = connection.prepareStatement(sql);
            if (elements != null && elements.size() != 0){
                for (int i = 0; i < elements.size(); i++){
                    SetParmUtils.setParamInit(i+1, elements.get(i), ps);
                }
            }
            rs = ps.executeQuery();
            results = TransformUtils.resultToBeanList(rs, classT);

        }catch (Exception e){
            e.printStackTrace();
        }finally{
            JDBCUtils.close(rs, ps, connection);
        }

        return results;

    }

    /**
     *
     * @param sql
     * @param elements
     * @param classT
     * @param <T>
     * @return A bean element read from DB
     */
    public static <T> T queryBeanByElements(String sql, ArrayList<Object> elements, Class<T> classT) throws SQLException {
        Connection connnection = JDBCUtils.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        T result = null;

        try {
            ps = connnection.prepareStatement(sql);
            if (elements != null && elements.size() != 0){
                for (int i = 0; i < elements.size(); i++){
                    SetParmUtils.setParamInit(i + 1, elements.get(i), ps);
                }
            }

            rs = ps.executeQuery();
            result = TransformUtils.resultToBean(rs, classT);

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            JDBCUtils.close(rs, ps, connnection);
        }

        return result;

    }

}
