package util;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.security.MessageDigest;
import java.util.Properties;

/**
 * 发邮件工具类
 */
public class MailUtils {
    private static final String USER = "xinquancaitest@163.com"; // 发件人称号，同邮箱地址
    private static final String PASSWORD = "OHIGQXSHCHDPDACR"; // 如果是qq邮箱可以使户端授权码，或者登录密码

    /**
     * @param to    收件人邮箱
     * @param text  邮件正文
     * @param title 标题
     */
    /* 发送验证信息的邮件 */
    public static boolean sendMail(String to, String text, String title) {
        System.out.println("if there is any input------");
        System.out.println(to);


        try {
            final Properties props = new Properties();

            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.host", "smtp.163.com");
            props.put("mail.smtp.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.ssl.enable", "true");
            // 发件人的账号
            props.put("mail.user", USER);
            //发件人的密码
            props.put("mail.password", PASSWORD);

            // 构建授权信息，用于进行SMTP进行身份验证
            Authenticator authenticator = new Authenticator() {
                @Override
                protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                    // 用户名、密码
                    String userName = props.getProperty("mail.user");
                    String password = props.getProperty("mail.password");
                    return new javax.mail.PasswordAuthentication(userName, password);
                }
            };
            // 使用环境属性和授权信息，创建邮件会话
            Session mailSession = Session.getInstance(props, authenticator);
            // 创建邮件消息
            MimeMessage message = new MimeMessage(mailSession);
            // 设置发件人
            String username = props.getProperty("mail.user");
            InternetAddress form = new InternetAddress(username);
            message.setFrom(form);

//            System.out.println("if has a sender------");
//            System.out.println(form);

            // 设置收件人
            InternetAddress toAddress = new InternetAddress(to);
            message.setRecipient(Message.RecipientType.TO, toAddress);

            // 设置邮件标题
            message.setSubject(title);

            // 设置邮件的内容体
            message.setContent(text, "text/html;charset=UTF-8");
            // 发送邮件
//            System.out.println("can send a mail------");
//            System.out.println(to);
            Transport.send(message);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void main(String[] args) throws Exception { // 做测试用
        String email="563642139@qq.com";
        String send="http://localhost:8888/ePortfolio_war_exploded/changePassword.html?keyword="+email;
        String encode=MailUtils.md5(email);
        System.out.println("加密后为：");
        System.out.println(encode);
        System.out.println("解密后为：");

        System.out.println(send);
       // MailUtils.sendMail("563642139@qq.com",  "<a href=\"http://localhost:8888/ePortfolio_war_exploded/changePassword.html?keyword="+email+"\">click here to reset your password</a>","test");
        System.out.println("发送成功");
    }

    public static String md5(String source) {
        StringBuffer sb = new StringBuffer(32);
        try {   	        MessageDigest md    = MessageDigest.getInstance("MD5");
            byte[] array        = md.digest(source.getBytes("utf-8"));
            for (int i = 0; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).toUpperCase().substring(1, 3));
            }
        } catch (Exception e) {

        }
        return sb.toString();   	}


}
