package bean;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/5/23 下午10:10
 */
public class Tag {

    private int projectid;
    private int userid;
    private String projecttag;

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getUserid() {
        return userid;
    }

    public int getProjectid() {
        return projectid;
    }

    public String getProjecttag() {
        return projecttag;
    }

    public void setProjectid(int projectid) {
        this.projectid = projectid;
    }

    public void setProjecttag(String projecttag) {
        this.projecttag = projecttag;
    }
}
