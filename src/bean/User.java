package bean;


import java.util.Date;

public class User {

    private int userid;
    private String name;
    private String email;
    private String password;
    private Date dob;
    private int gender;
    private int phone;
    private String address;
    private String photo;
    private String description;
    private String str_dob;
    private String major;
    private String job;

    public User(){
        super();
    }

    public int getUserid(){
        return userid;
    }
    public void setUserid(int id){
        this.userid = id;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getEmail(){
        return email;
    }
    public void setEmail(String email){
        this.email = email;
    }

    public String getPassword(){
        return password;
    }
    public void setPassword(String password){
        this.password = password;
    }

    public Date getDob(){
        return dob;
    }
    public void setDob(Date DOB){
        this.dob = DOB;
    }

    public int getGender(){
        return gender;
    }
    public void setGender(int gender){
        this.gender = gender;
    }

    public int getPhone(){
        return phone;
    }
    public void setPhone(int phone){
        this.phone = phone;
    }

    public String getAddress(){
        return address;
    }
    public void setAddress(String address){
        this.address = address;
    }

    public String getPhoto(){
        return photo;
    }
    public void setPhoto(String photo){
        this.photo = photo;
    }

    public String getDescription(){
        return description;
    }
    public void setDescription(String description){
        this.description = description;
    }

    public String getStr_dob(){ return this.str_dob; }
    public void setStr_dob(String dob){ this.str_dob = dob; }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getMajor() {
        return major;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }
}
