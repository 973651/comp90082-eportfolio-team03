package bean;


import java.util.Date;

public class Project {
    private String str_starttime;
    private String str_endtime;

    private int projectid;
    private int userid;
    private String projectname;
    private String projectdescription;
    private Date starttime;
    private Date endtime;
    private String projectimg;
    private String projectvideo;
    private String content;
    private String document;
    private int visibility;
    private String visibleto;
    private int countrequest;

    public int getProjectid() {
        return projectid;
    }

    public void setProjectid(int projectid) {
        this.projectid = projectid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getProjectname() {
        return projectname;
    }

    public void setProjectname(String projectname) {
        this.projectname = projectname;
    }

    public String getProjectdescription() {
        return projectdescription;
    }

    public void setProjectdescription(String projectdescription) {
        this.projectdescription = projectdescription;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public String getProjectimg() {
        return projectimg;
    }

    public void setProjectimg(String projectimg) {
        this.projectimg = projectimg;
    }

    public String getProjectvideo() {
        return projectvideo;
    }

    public void setProjectvideo(String projectvideo) {
        this.projectvideo = projectvideo;
    }

    public String getStr_starttime() {
        return str_starttime;
    }

    public void setStr_starttime(String str_starttime) {
        this.str_starttime = str_starttime;
    }

    public String getStr_endtime() {
        return str_endtime;
    }

    public void setStr_endtime(String str_endtime) {
        this.str_endtime = str_endtime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public String getVisibleto() {
        return visibleto;
    }

    public void setVisibleto(String visibleto) {
        this.visibleto = visibleto;
    }

    public int getCountrequest() {
        return countrequest;
    }

    public void setCountrequest(int countrequest) {
        this.countrequest = countrequest;
    }
}
