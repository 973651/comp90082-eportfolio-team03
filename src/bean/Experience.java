package bean;

import java.util.Date;

public class Experience {

    public int getWorkid() {
        return workid;
    }

    public void setWorkid(int workid) {
        this.workid = workid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getJobname() {
        return jobname;
    }

    public void setJobname(String jobname) {
        this.jobname = jobname;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getWorkdescription() {
        return workdescription;
    }

    public void setWorkdescription(String workdescription) {
        this.workdescription = workdescription;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public String getJobimg() {
        return jobimg;
    }

    public void setJobimg(String jobimg) {
        this.jobimg = jobimg;
    }

    public String getJobvideo() {
        return jobvideo;
    }

    public void setJobvideo(String jobvideo) {
        this.jobvideo = jobvideo;
    }



    private int workid;
    private int userid;
    private String jobname;
    private String company;
    private String workdescription;
    private Date starttime;
    private Date endtime;
    private String jobimg;
    private String jobvideo;

    public String getStr_starttime() {
        return str_starttime;
    }

    public void setStr_starttime(String str_starttime) {
        this.str_starttime = str_starttime;
    }

    public String getStr_endtime() {
        return str_endtime;
    }

    public void setStr_endtime(String str_endtime) {
        this.str_endtime = str_endtime;
    }

    private String str_starttime;
    private String str_endtime;
}
