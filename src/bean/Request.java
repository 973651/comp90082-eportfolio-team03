package bean;

import java.util.Date;

public class Request {
    private int requestid;
    private int projectid;
    private int requestfrom;
    private Date requesttime;
    private String requestcontent;
    private int requeststatus;

    private String name;
    private String photo;
    private String projectname;
    private int userid;

    private String str_requesttime;

    public int getProjectid() {
        return projectid;
    }

    public void setProjectid(int projectid) {
        this.projectid = projectid;
    }

    public int getRequestfrom() {
        return requestfrom;
    }

    public void setRequestfrom(int requestfrom) {
        this.requestfrom = requestfrom;
    }

    public Date getRequesttime() {
        return requesttime;
    }

    public void setRequesttime(Date requesttime) {
        this.requesttime = requesttime;
    }

    public String getRequestcontent() {
        return requestcontent;
    }

    public void setRequestcontent(String requestcontent) {
        this.requestcontent = requestcontent;
    }

    public String getStr_requesttime() {
        return str_requesttime;
    }

    public void setStr_requesttime(String str_requesttime) {
        this.str_requesttime = str_requesttime;
    }

    public int getRequestid() {
        return requestid;
    }

    public void setRequestid(int requestid) {
        this.requestid = requestid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getProjectname() {
        return projectname;
    }

    public void setProjectname(String projectname) {
        this.projectname = projectname;
    }

    public int getRequeststatus() {
        return requeststatus;
    }

    public void setRequeststatus(int requeststatus) {
        this.requeststatus = requeststatus;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }
}
