package bean;

import java.util.Date;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/4/30 下午12:36
 */
public class Education {
    private int educationid;
    private int userid;
    private String school;
    private String major;
    private Date enrollmenttime;
    private Date graduationtime;
    private String maincourse;
    private String str_starttime;
    private String str_endtime;
    private String degree;

    public Date getEnrollmenttime() {
        return enrollmenttime;
    }

    public Date getGraduationtime() {
        return graduationtime;
    }

    public int getEducationid() {
        return educationid;
    }

    public int getUserid() {
        return userid;
    }

    public String getMaincourse() {
        return maincourse;
    }

    public String getMajor() {
        return major;
    }

    public String getSchool() {
        return school;
    }

    public void setEducationid(int educationid) {
        this.educationid = educationid;
    }

    public void setEnrollmenttime(Date enrollmenttime) {
        this.enrollmenttime = enrollmenttime;
    }

    public void setGraduationtime(Date graduationtime) {
        this.graduationtime = graduationtime;
    }

    public void setMaincourse(String maincourse) {
        this.maincourse = maincourse;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getStr_endtime() {
        return str_endtime;
    }

    public String getStr_starttime() {
        return str_starttime;
    }

    public void setStr_endtime(String str_endtime) {
        this.str_endtime = str_endtime;
    }

    public void setStr_starttime(String str_starttime) {
        this.str_starttime = str_starttime;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }
}
