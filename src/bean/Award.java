package bean;

import java.util.Date;

public class Award {
    private int awardid;
    private int userid;
    private int infoid;
    private String awardname;
    private String awarddescription;
    private Date awardtime;
    private String str_awardtime;

    public int getAwardid() {
        return awardid;
    }

    public void setAwardid(int awardid) {
        this.awardid = awardid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getInfoid() {
        return infoid;
    }

    public void setInfoid(int infoid) {
        this.infoid = infoid;
    }

    public String getAwardname() {
        return awardname;
    }

    public void setAwardname(String awardname) {
        this.awardname = awardname;
    }

    public String getAwarddescription() {
        return awarddescription;
    }

    public void setAwarddescription(String awarddescription) {
        this.awarddescription = awarddescription;
    }

    public Date getAwardtime() {
        return awardtime;
    }

    public void setAwardtime(Date awardtime) {
        this.awardtime = awardtime;
    }

    public String getStr_awardtime() {
        return str_awardtime;
    }

    public void setStr_awardtime(String str_awardtime) {
        this.str_awardtime = str_awardtime;
    }
}
