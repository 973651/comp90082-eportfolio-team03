package dao;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/5/22 上午12:43
 */
public interface LogoutDAO {

    public int deleteAllInfo(int userId);

}
