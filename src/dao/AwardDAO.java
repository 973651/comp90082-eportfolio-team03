package dao;

import bean.Award;
import bean.Project;

import java.util.List;

public interface AwardDAO {
    /**
     * This method is used to get all awards of an user
     * @param userId
     * @return A list of award objects that store query outcomes
     */
    public List<Award> getAwards(int userId);

    /**
     * Insert a new award for user whose id stores in award object
     * @param a
     * @return result represents whether the manipulation is success
     */
    public int addAward(Award a);

    /**
     * Update an award for user whose award id stores in award object
     * @param a
     * @return result represents whether the manipulation is success
     */
    public int editAward(Award a);

    /**
     * Delete an award with the award id
     * @param awardId
     * @return result represents whether the manipulation is success
     */
    public int deleteAward(int awardId);
}
