package dao;

public interface ResetPasswordDAO {
    public int resetPassword(String email, String password);

    }
