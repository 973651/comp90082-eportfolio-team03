package dao;

import bean.Project;
import bean.Request;

import java.util.List;
import java.util.Map;

public interface PrivacyDAO {
    /**
     * Set visibility of the project with its id
     * @param projectId
     * @return result represents whether the manipulation is success
     */
    public int setPrivacy(int projectId, int visibility);

    /**
     * Update users in visibleTo column
     * @param projectId
     * @param userList  new user list contains previous users
     * @return result represents whether the manipulation is success
     */
    public int addVisibleUsers(int projectId, String userList);

    /**
     * Insert new request for a project whose id stores in Request object
     * @param req a request object
     * @return result represents whether the manipulation is success
     */
    public int addRequest(Request req);

    /**
     * Get all requests for a project
     * @param projectId
     * @return a list of requests
     */
    public List<Request> getRequests(int projectId);

    /**
     * Get all requests sent by the user
     * @param userId
     * @return a list of requests
     */
    public List<Request> getMyRequests(int userId);

    /**
     * Get all unprocessed requested projects of the user in visibility state 1
     * @param userId
     * @return a list of projects
     */
    public List<Project> getRequestedProjects(int userId);

    /**
     * Delete the request with request id
     * @param requestId
     * @return result represents whether the manipulation is success
     */
    public int cancelRequest(int requestId);

    /**
     * Update the request status for accept or refuse
     * @param projectId
     * @param requestFrom
     */
    public void processRequest(int projectId, int requestFrom, int status);
}
