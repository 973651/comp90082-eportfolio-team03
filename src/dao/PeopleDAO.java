/**
 * @author Boyan XIN
 * @version 1.1 New interface
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/4/21 下午10:39
 */

package dao;

import bean.User;
import java.util.ArrayList;

public interface PeopleDAO {

    /**
     * This method used to query a list of people
     * @return A list of people that satisfying the query keywords.
     */
    public ArrayList<User> searchPeopleByName(User user);

    public int updateUserPwdById(User user);

    public String queryPasswordById(int userId);

    public User queryInfoById(User user);

    public int updateUserInfoById(User user);
}
