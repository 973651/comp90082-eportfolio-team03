package dao;

import bean.Project;

import java.util.Date;
import java.util.List;

public interface ProjectDAO {
    /**
     * This method is used to get all projects of one user
     * @param userId
     * @return A list of project objects that store query outcomes
     */
    public List<Project> getProjects(int userId);

    /**
     * Insert a new project for user whose id stores in project object
     * @param p
     * @return a value that represent whether the manipulation is success
     */
    public int addProject(Project p);

    /**
     * This method is used to get one project with its id
     * @param projectId
     * @return a project object that store query outcomes
     */
    public Project getOneProject(int projectId);

}
