package dao.implementation;

import dao.LogoutDAO;
import util.OtherDbActionUtils;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/5/22 上午12:43
 */
public class LogoutDAOImpl implements LogoutDAO {
    @Override
    public int deleteAllInfo(int userId) {
        ArrayList<String> sqlList = new ArrayList<>();
        String safeZero = "SET SQL_SAFE_UPDATES = 0";
        String userDel = "DELETE FROM user WHERE userId = ?";
        String projectDel = "DELETE FROM project WHERE userId = ?";
        String expDel = "DELETE FROM workingexperience WHERE userId = ?";
        String eduDel = "DELETE FROM education WHERE userId = ?";
        String awardDel = "DELETE FROM award WHERE userId = ?";
        String tagDel = "DELETE FROM tag WHERE userId = ?";
        String visiDel = "DELETE FROM request WHERE requestFrom = ?";
        String safeOne = "SET SQL_SAFE_UPDATES = 1";

        sqlList.add(userDel);
        sqlList.add(projectDel);
        sqlList.add(expDel);
        sqlList.add(eduDel);
        sqlList.add(awardDel);
        sqlList.add(tagDel);
        sqlList.add(visiDel);


        int res = -1;
        ArrayList<Object> paramList = new ArrayList<>();
        try {
            res = OtherDbActionUtils.deleteData(safeZero, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for (String sql : sqlList){
//            System.out.println(sql);
            ArrayList<Object> x = new ArrayList<Object>();
            x.add(userId);
            res = -1;
            try {
                res = OtherDbActionUtils.deleteData(sql, x);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        try {
            res = OtherDbActionUtils.deleteData(safeOne, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }
}
