package dao.implementation;

import bean.Education;
import dao.EducationDAO;
import util.DbQueryUtils;
import util.OtherDbActionUtils;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/4/30 下午12:37
 */
public class EducationDAOImpl implements EducationDAO {

    /**
     * Load All Eduction situation of current User
     * @param id
     * @return A list of Eduction experience of current User
     */
    @Override
    public ArrayList<Education> queryAllEducationById(int id) {

        String sql = "SELECT * FROM education WHERE userid = ?";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(id);
        ArrayList<Education> educationList = new ArrayList<>();
        try {
            educationList = DbQueryUtils.queryBeanListByElements(sql, paramList, Education.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return educationList;
    }

    public int addEducation(Education edu){
        String sql = "insert into education (userId, school, major, degree, enrollmenttime, graduationtime, maincourse) values (?,?,?,?,?,?,?)";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(edu.getUserid());
        paramList.add(edu.getSchool());
        paramList.add(edu.getMajor());
        paramList.add(edu.getDegree());
        paramList.add(edu.getEnrollmenttime());
        paramList.add(edu.getGraduationtime());
        paramList.add(edu.getMaincourse());
        int res = -1000000000;
        try {
            res = OtherDbActionUtils.insertIntoDB(sql, paramList);
            System.out.println(res);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public int deleteEdu(int eduid){
        String sql = "DELETE FROM education WHERE educationid = ?";
        ArrayList<Object> paramList = new ArrayList<Object>();
        paramList.add(eduid);
        int res = -1;
        try {
            res = OtherDbActionUtils.deleteData(sql, paramList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public int editEdu(Education edu){
        String sql = "UPDATE education SET school=?, major=?, degree=?, enrollmentTime=?, graduationTime=?, maincourse=?  WHERE educationid=?";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(edu.getSchool());
        paramList.add(edu.getMajor());
        paramList.add(edu.getDegree());
        paramList.add(edu.getEnrollmenttime());
        paramList.add(edu.getGraduationtime());
        paramList.add(edu.getMaincourse());
        paramList.add(edu.getEducationid());
        int res = -1;
        try {
            res = OtherDbActionUtils.updataDataInDB(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
}
