package dao.implementation;

import bean.Project;
import bean.Tag;
import dao.ProjectDAO;
import util.DbQueryUtils;
import util.OtherDbActionUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProjectDAOImpl implements ProjectDAO {
    public List<Project> getProjects(int userId){
        List<Project> results = new ArrayList<>();
        String sql = "select * from project where userId = '"+userId+"'";
        ArrayList<Object> paramList = new ArrayList<Object>();
        try {
            results = DbQueryUtils.queryBeanListByElements(sql, paramList, Project.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    public int addProject(Project p){
        String sql = "insert into project (userId, projectName, projectDescription, startTime, endTime, projectimg, " +
                "projectvideo, content, document, visibility, visibleTo) values (?,?,?,?,?,?,?,?,?,?,?)";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(p.getUserid());
        paramList.add(p.getProjectname());
        paramList.add(p.getProjectdescription());
        paramList.add(p.getStarttime());
        paramList.add(p.getEndtime());
        paramList.add(p.getProjectimg());
        paramList.add(p.getProjectvideo());
        paramList.add(p.getContent());
        paramList.add(p.getDocument());
        paramList.add(p.getVisibility());
        paramList.add(p.getVisibleto());
        int res = -1;
        try{
            res = OtherDbActionUtils.insertIntoDB(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public Project getOneProject(int projectId) {
        String sql = "select * from project where projectId = "+projectId;
        System.out.println("projectid  " + projectId);
        ArrayList<Object> paramList = new ArrayList<Object>();
        Project result = null;
        try {
            result = DbQueryUtils.queryBeanByElements(sql, paramList, Project.class);
            //System.out.println(result.getProjectid() + "!!!!!!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Project getSpecificProject(int userid, String projectName){
        String sql = "select * from project where userid = "+ userid +" AND projectname = '"+ projectName +"'";
        System.out.println(userid +" " +projectName + " "+sql);
        ArrayList<Object> paramList = new ArrayList<Object>();
        Project result = null;
        try {
            result = DbQueryUtils.queryBeanByElements(sql, paramList, Project.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("new Project is " + result.getProjectname());
        return result;
    }

    public int addTag(int userid, int projectid, String tag){
        String sql = "insert into Tag (userid, projectid, projecttag) values (?,?,?)";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(userid);
        paramList.add(projectid);
        paramList.add(tag);
        int res = -1;
        try{
            res = OtherDbActionUtils.insertIntoDB(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;

    }

    public int updateAProject(Project p){
        String sql = "UPDATE project SET projectname=?,projectdescription=?,content=?,startTime=?,endTime=?,projectimg=?,projectvideo =?,document=?,visibility=? WHERE userId=? AND projectId=?";

        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(p.getProjectname());
        paramList.add(p.getProjectdescription());
        paramList.add(p.getContent());
        paramList.add(p.getStarttime());
        paramList.add(p.getEndtime());
        paramList.add(p.getProjectimg());
        paramList.add(p.getProjectvideo());
        paramList.add(p.getDocument());
        paramList.add(p.getVisibility());
        paramList.add(p.getUserid());
        paramList.add(p.getProjectid());

        int res = -1;
        try{
            res = OtherDbActionUtils.insertIntoDB(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;

    }

    public int updateAProjectTag(int userid, int projectid, String[] taglist){

        ArrayList<String> sqlList = new ArrayList<>();
        String safeZero = "SET SQL_SAFE_UPDATES = 0";
        String delete = "delete from tag where userid="+ userid+" and projectid =" + projectid;
        String safeOne = "SET SQL_SAFE_UPDATES = 1";
        sqlList.add(safeZero);
        sqlList.add(delete);

        for(int i = 0; i < taglist.length; i++){
            String sql = "insert into Tag (userid, projectid, projecttag) values ("+userid+","+projectid+",'"+taglist[i]+"')";
            sqlList.add(sql);
        }
        sqlList.add(safeOne);

        int res = -1;
        for (String sql : sqlList){
            ArrayList<Object> paramList = new ArrayList<Object>();
            try {
                res = OtherDbActionUtils.updataDataInDB(sql, paramList);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return res;
    }

    public ArrayList<Tag> getOnesAllProjectTag(int userid){
        String sql = "select distinct(projecttag) as projecttag from tag where userid =" + userid;
        ArrayList<Object> paramList = new ArrayList<>();

        ArrayList<Tag> result = new ArrayList<>();
        try {
            result = DbQueryUtils.queryBeanListByElements(sql, paramList, Tag.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<Tag> getOnesAllVisibleTag(int userid){
        String sql = "select distinct(t.projecttag) as projecttag " +
                "from tag t left join project p on t.projectid=p.projectId" +
                " where t.userid =" + userid + " and (p.visibility = 1 or p.visibility=2)";
        ArrayList<Object> paramList = new ArrayList<>();
        ArrayList<Tag> result = new ArrayList<>();
        try {
            result = DbQueryUtils.queryBeanListByElements(sql, paramList, Tag.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }



    public ArrayList<Tag> getOneProjectTags(int userid, int projectId){
        String sql = "select * from tag where userid="+ userid +" and projectid="+ projectId +" and userid in (select min(userid) from tag group by projecttag);";
        ArrayList<Object> paramList = new ArrayList<>();

        ArrayList<Tag> result = new ArrayList<>();
        try {
            result = DbQueryUtils.queryBeanListByElements(sql, paramList, Tag.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;

    }

    public ArrayList<Tag> getProjectByTag(int userid, String tag){
        String sql = "select * from tag where  userid=? and projecttag=?";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(userid);
        paramList.add(tag);

        ArrayList<Tag> result = new ArrayList<>();
        try {
            result = DbQueryUtils.queryBeanListByElements(sql, paramList, Tag.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;

    }

    public int deleteAProject(int userId, int projectId){

        String delPro = "DELETE FROM project WHERE userId = "+userId+" AND projectid = " + projectId;
        String delTag = "DELETE FROM tag WHERE userId = "+userId+" AND projectid = " + projectId;
        ArrayList<Object> paramList = new ArrayList<Object>();
        int res1 = -1;
        int res2 = -1;
        try {
            res1 = OtherDbActionUtils.deleteData(delPro, paramList);
            if (res1 != -1){
                res2 = OtherDbActionUtils.deleteData(delTag, paramList);
            }else{
                return -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("delete success");
        return res2;

    }

    public int deleteAImage(int userid, int projectid, String projectimg){
        String delSql = "UPDATE project SET projectimg=? WHERE userid=? and projectid=?";
        ArrayList<Object> paramList = new ArrayList<Object>();
        paramList.add(projectimg);
        paramList.add(userid);
        paramList.add(projectid);
        int res = -1;
        try{
            res = OtherDbActionUtils.updataDataInDB(delSql, paramList);
        }catch (Exception e){
            e.printStackTrace();
        }

        return res;

    }

    public int deleteAFile(int userid, int projectid, String file, String fileType){
        String sql = "";
        if (fileType.equals("video")){
            sql = "UPDATE project SET projectvideo=? WHERE userid=? and projectid=?";
        }else{
            sql = "UPDATE project SET document=? WHERE userid=? and projectid=?";
        }
        ArrayList<Object> paramList = new ArrayList<Object>();
        paramList.add(file);
        paramList.add(userid);
        paramList.add(projectid);
        int res = -1;
        try{
            res = OtherDbActionUtils.updataDataInDB(sql, paramList);
        }catch (Exception e){
            e.printStackTrace();
        }

        return res;
    }
}
