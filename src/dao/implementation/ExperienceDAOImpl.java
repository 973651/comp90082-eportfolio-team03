package dao.implementation;

import bean.Experience;
import bean.Project;
import dao.ExperienceDAO;
import util.DbQueryUtils;
import util.OtherDbActionUtils;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExperienceDAOImpl implements ExperienceDAO {
    @Override
    public int addExperience(Experience e) {
        String sql = "insert into workingexperience (userId, jobname, company, workdescription, startTime, endTime) values (?,?,?,?,?,?)";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(e.getUserid());
        paramList.add(e.getJobname());
        paramList.add(e.getCompany());
        paramList.add(e.getWorkdescription());
        paramList.add(e.getStarttime());
        paramList.add(e.getEndtime());
        System.out.println(sql);
        int res = -1;
        try{
            res = OtherDbActionUtils.insertIntoDB(sql, paramList);
        } catch (SQLException ee) {
            ee.printStackTrace();
        }
        System.out.println("insert result: "+res);
        return res;
    }

    @Override
    public List<Experience> getExperience(int userId) {
        List<Experience> results = new ArrayList<>();
        String sql = "select * from workingexperience where userId = '"+userId+"'";
        ArrayList<Object> paramList = new ArrayList<Object>();
        try {
            results = DbQueryUtils.queryBeanListByElements(sql, paramList, Experience.class);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    public int deleteExp(int workid){
        String sql = "DELETE FROM workingexperience WHERE workid =?";
        ArrayList<Object> paramList = new ArrayList<Object>();
        paramList.add(workid);
        int res = -1;
        try {
            res = OtherDbActionUtils.deleteData(sql, paramList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public int editExp(Experience exp){
        System.out.println("i'm here");
        String sql = "UPDATE workingexperience SET jobname=?, company=?, starttime=?, endtime=?, workdescription=? WHERE workid=?";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(exp.getJobname());
        paramList.add(exp.getCompany());
        paramList.add(exp.getStarttime());
        paramList.add(exp.getEndtime());
        paramList.add(exp.getWorkdescription());
        paramList.add(exp.getWorkid());

        int res = -1;
        try {
            res = OtherDbActionUtils.updataDataInDB(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
}
