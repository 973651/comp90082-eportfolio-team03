package dao.implementation;

import bean.Tag;
import bean.User;
import dao.PeopleDAO;
import util.DbQueryUtils;
import util.OtherDbActionUtils;
import util.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Boyan XIN
 * @version 1.1 Update method added.
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/4/21 下午10:38
 */
public class PeopleDAOImpl implements PeopleDAO{

    @Override
    public ArrayList<User> searchPeopleByName(User user) {
        String sql = "SELECT * FROM user WHERE name LIKE '%"+user.getName()+"%'";
        System.out.println(sql);
        ArrayList<Object> paramList = new ArrayList<Object>();
        ArrayList<User> result = new ArrayList<>();
        try {
            result = DbQueryUtils.queryBeanListByElements(sql, paramList, User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int updateUserPwdById(User user) {
        String sql = "UPDATE User SET password=? "
                + "WHERE userid =?";

        ArrayList<Object> paramList = new ArrayList<>();

        paramList.add(user.getPassword());
        paramList.add(user.getUserid());

        int result = 0;

        try {
            result =  OtherDbActionUtils.updataDataInDB(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public String queryPasswordById(int userId) {
        String sql = "SELECT password FROM User WHERE userid = ?";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(userId);

        String result = "";
        try {
            result = (String)DbQueryUtils.queryResultByElement(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public User queryInfoById(User user) {
        String sql = "SELECT * FROM User WHERE userid = ?";
        System.out.println(sql);
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(user.getUserid());
        User result = new User();
        try {
            result = DbQueryUtils.queryBeanByElements(sql, paramList, User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(result);
        return result;
    }

    public User queryAvatarById(int userId) {
        String sql = "SELECT photo as photo from user where userid = ?";
        System.out.println(sql);
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(userId);
        User result = new User();
        try {
            result = DbQueryUtils.queryBeanByElements(sql, paramList, User.class);
            System.out.println("aaaa  " + result.getPhoto());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(result);
        return result;
    }

    @Override
    public int updateUserInfoById(User user) {
        String sql = "UPDATE User SET name =?, dob=?, phone =?, address =?, description=?, major=?, job=?, gender=? "
                + "WHERE userId =?";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(user.getName());
        paramList.add(user.getDob());
        paramList.add(user.getPhone());
        paramList.add(user.getAddress());
        paramList.add(user.getDescription());
        paramList.add(user.getMajor());
        paramList.add(user.getJob());
        paramList.add(user.getGender());
        paramList.add(user.getUserid());
        int result = -1;

        try {
            result = OtherDbActionUtils.updataDataInDB(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int updatePhotoById(int userid, String photo){
        String sql = "UPDATE User SET photo=? WHERE userId =?";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(photo);
        paramList.add(userid);
        int result = -1;
        try {
            result = OtherDbActionUtils.updataDataInDB(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<User> queryRecommendUser(String sql){
        ArrayList<Object> paramList = new ArrayList<>();
        ArrayList<User> result = new ArrayList<>();
        try {
            result = DbQueryUtils.queryBeanListByElements(sql, paramList, User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;

    }

    public ArrayList<User> searchPeopleByNameAndMajor(User user) {
        String sql = "SELECT * FROM user WHERE name LIKE '%" + user.getName() + "%' AND major=?";
        ArrayList<Object> paramList = new ArrayList<Object>();

        paramList.add(user.getMajor());
        ArrayList<User> result = new ArrayList<>();
        try {
            result = DbQueryUtils.queryBeanListByElements(sql, paramList, User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<User> searchPeopleByNameAndJob(User user) {
        String sql = "SELECT * FROM user WHERE name LIKE '%" + user.getName() + "%' AND job=?";
        ArrayList<Object> paramList = new ArrayList<Object>();
        paramList.add(user.getJob());
        ArrayList<User> result = new ArrayList<>();
        try {
            result = DbQueryUtils.queryBeanListByElements(sql, paramList, User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<User> searchPeopleByNameAndBothFilter(User user) {
        String sql = "SELECT * FROM user WHERE name LIKE '%" + user.getName() + "%' AND major=? AND job=?";
        ArrayList<Object> paramList = new ArrayList<Object>();
        paramList.add(user.getMajor());
        paramList.add(user.getJob());
        ArrayList<User> result = new ArrayList<>();
        try {
            result = DbQueryUtils.queryBeanListByElements(sql, paramList, User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

}
