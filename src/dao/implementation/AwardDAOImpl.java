package dao.implementation;

import bean.Award;
import dao.AwardDAO;
import util.DbQueryUtils;
import util.OtherDbActionUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AwardDAOImpl implements AwardDAO {
    @Override
    public List<Award> getAwards(int userId) {
        List<Award> results = new ArrayList<>();
        String sql = "SELECT * FROM award WHERE userId = '"+userId+"' ORDER BY awardTime DESC";
        ArrayList<Object> paramList = new ArrayList<Object>();
        try {
            results = DbQueryUtils.queryBeanListByElements(sql, paramList, Award.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    @Override
    public int addAward(Award a) {
        String sql = "INSERT INTO award (userId, infoId, awardName, awardDescription, awardTime) VALUES (?,?,?,?,?)";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(a.getUserid());
        paramList.add(a.getInfoid());
        paramList.add(a.getAwardname());
        paramList.add(a.getAwarddescription());
        paramList.add(a.getAwardtime());
        int res = -1;
        try {
            res = OtherDbActionUtils.insertIntoDB(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public int editAward(Award a) {
        String sql = "UPDATE award SET awardName=?, awardDescription=?, awardTime=? WHERE awardId=?";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(a.getAwardname());
        paramList.add(a.getAwarddescription());
        paramList.add(a.getAwardtime());
        paramList.add(a.getAwardid());
        int res = -1;
        try {
            res = OtherDbActionUtils.updataDataInDB(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public int deleteAward(int awardId) {
        String sql = "DELETE FROM award WHERE awardId = '"+awardId+"'";
        ArrayList<Object> paramList = new ArrayList<Object>();
        int res = -1;
        try {
            res = OtherDbActionUtils.deleteData(sql, paramList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }


}
