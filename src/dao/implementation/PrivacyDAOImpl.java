package dao.implementation;

import bean.Award;
import bean.Project;
import bean.Request;
import dao.PrivacyDAO;
import util.DbQueryUtils;
import util.OtherDbActionUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrivacyDAOImpl implements PrivacyDAO {
    @Override
    public int setPrivacy(int projectId, int visibility) {
        String sql = "UPDATE project SET visibility = ? WHERE projectId = ?";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(visibility);
        paramList.add(projectId);
        int res = -1;
        try {
            res = OtherDbActionUtils.updataDataInDB(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public int addVisibleUsers(int projectId, String userList) {
        String sql = "UPDATE project SET visibleTo = ? WHERE projectId = ?";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(userList);
        paramList.add(projectId);
        int res = -1;
        try {
            res = OtherDbActionUtils.updataDataInDB(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public int addRequest(Request req) {
        String sql = "INSERT INTO request (projectId, requestFrom, requestTime, requestContent, requestStatus) VALUES (?,?,?,?,?)";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(req.getProjectid());
        paramList.add(req.getRequestfrom());
        paramList.add(req.getRequesttime());
        paramList.add(req.getRequestcontent());
        paramList.add(0);
        int res = -1;
        try {
            res = OtherDbActionUtils.insertIntoDB(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public List<Request> getRequests(int projectId) {
        List<Request> results = new ArrayList<>();
        String sql = "SELECT requestId,projectId,requestFrom,requestTime,requestContent,u.name,u.photo " +
                "FROM request r LEFT JOIN user u ON r.requestFrom=u.userId " +
                "WHERE r.projectId = '"+projectId+"' AND r.requestStatus=0 ORDER BY r.requestTime";
        ArrayList<Object> paramList = new ArrayList<Object>();
        try {
            results = DbQueryUtils.queryBeanListByElements(sql, paramList, Request.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    @Override
    public List<Request> getMyRequests(int userId) {
        List<Request> results = new ArrayList<>();
        String sql = "SELECT r.requestId AS requestId, u.name AS name, u.userid AS userid, p.projectName AS requestprojectname, r.requestTime AS requestTime, r.requestStatus AS requestStatus" +
                " FROM request r, project p, user u" +
                " WHERE r.projectId=p.projectId AND p.userId=u.userId" +
                " AND r.requestFrom = '"+userId+"'" +
                " ORDER BY r.requestStatus DESC";
        ArrayList<Object> paramList = new ArrayList<Object>();
        try {
            results = DbQueryUtils.queryBeanListByElements(sql, paramList, Request.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    @Override
    public List<Project> getRequestedProjects(int userId) {
        String sql = "SELECT p.projectId AS projectId, p.projectName AS projectName, p.projectDescription AS projectDescription, COUNT(r.projectId) AS countrequest" +
                " FROM request r LEFT JOIN project p ON r.projectId = p.projectId" +
                " WHERE p.userId = '"+userId+"' AND p.visibility=1 AND r.requestStatus=0" +
                " GROUP BY r.projectId";
        ArrayList<Object> paramList = new ArrayList<>();
        List<Project> res = new ArrayList<>();
        try {
            res = DbQueryUtils.queryBeanListByElements(sql, paramList, Project.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public int cancelRequest(int requestId) {
        String sql = "DELETE FROM request WHERE requestId = "+requestId;
        ArrayList<Object> paramList = new ArrayList<>();
        int res = -1;
        try {
            res = OtherDbActionUtils.deleteData(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public void processRequest(int projectId, int requestFrom, int status) {
        String sql = "UPDATE request SET requestStatus = ? WHERE projectId = ? AND requestFrom = ?";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(status);
        paramList.add(projectId);
        paramList.add(requestFrom);
        try {
            OtherDbActionUtils.updataDataInDB(sql, paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
