package dao.implementation;

import dao.ResetPasswordDAO;
import util.OtherDbActionUtils;

import java.sql.SQLException;
import java.util.ArrayList;

public class ResetPasswordDAOImpl implements ResetPasswordDAO {
    @Override
    public int resetPassword(String email, String password) {
        String sql = "UPDATE user SET password=? WHERE email = ?";
        ArrayList<Object> paramList = new ArrayList<>();
        paramList.add(password);
        paramList.add(email);
        int result=-1;
        try {
            result = OtherDbActionUtils.otherDbAction(sql,paramList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}

