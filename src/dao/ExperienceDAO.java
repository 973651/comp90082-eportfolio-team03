package dao;

import bean.Experience;
import bean.Project;

import java.util.Date;
import java.util.List;

public interface ExperienceDAO {
    /**
     * This method is used to get all working experience of one user
     * @param userId
     * @return A list of project objects that store query outcomes
     */
    public List<Experience> getExperience(int userId);

    public int addExperience(Experience e);

}
