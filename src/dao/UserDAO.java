package dao;

import bean.User;
import util.JDBCUtils;

import java.sql.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserDAO {

    /**
     * login function
     * @param email
     * @param password
     * @return User object
     */
    public User login(String email, String password){
        User u = null;
        Connection con = null;
        Statement sta = null;
        ResultSet res = null;
        try {
            //1.get connection
            con = JDBCUtils.getConnection();
            //2.define sql
            String sql = "select * from user where email = '"+email+"' and password = '"+password+"'";
            //3.get object for executing sql
            sta = con.createStatement();
            //4.execute search
            res = sta.executeQuery(sql);
            //5.judge result
            if(res.next()){
                u = new User();
                u.setEmail(res.getString("email"));
                u.setPassword(res.getString("password"));
                u.setUserid(res.getInt("userid"));
                u.setName(res.getString("name"));
                u.setPhoto(res.getString("photo"));
                //System.out.println("login success");
            } else{
                System.out.println("login failed");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.close(res, sta, con);
        }
        return u;
    }

    /**
     * check if the format of email is legal
     * @param email
     * @return true: format is legal
     */
    public boolean emailFormat(String email){
        Pattern p = Pattern.compile("^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$");
        Matcher m = p.matcher(email);
        return m.matches();
    }

    /**
     * check if the email address has been registered
     * @param email
     * @return true: this email address has been used
     */
    public boolean emailExist(String email){
        Connection con = null;
        Statement sta = null;
        ResultSet res = null;
        try{
            con = JDBCUtils.getConnection();
            sta = con.createStatement();
            String sql = "select * from user where email = '"+email+"'";
            res = sta.executeQuery(sql);
            if(res.next()){
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.close(res,sta,con);
        }
        return false;
    }

    /**
     * check if the length of password is no less than 6 characters
     * @param password
     * @return true: the length is equal to or larger than 6
     */
    public boolean checkPassword(String password){
        return password.length()>=6;
    }

    /**
     * register function
     * @param user
     * @return
     */
    public boolean register(User user){
        String username=  user.getName();
        String email = user.getEmail();
        String password = user.getPassword();
        //默认头像
        String photo = "https://comp90082-image-storage.oss-cn-shenzhen.aliyuncs.com/defaultAvatar/defaultAvatar.jpg";

        Connection con = null;
        Statement sta = null;
        PreparedStatement presta = null;
        ResultSet res = null;
        try {
            con = JDBCUtils.getConnection();
            sta = con.createStatement();
            //check if user name, email and password is empty
            if(username.length()==0 || email.length()==0 || password.length()==0){
                return false;
            }

            //check if this email is exist and password is no less then 6 chars
            if(emailExist(email) || !checkPassword(password)){
                return false;
            }

            //add new user
            String insertSql = "insert into user (name,email,password,photo) values (?,?,?,?)";
            presta = con.prepareStatement(insertSql);
            presta.setString(1, username);
            presta.setString(2, email);
            presta.setString(3, password);
            presta.setString(4, photo);
            presta.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.close(res, sta, con);
        }
        return true;
    }

}
