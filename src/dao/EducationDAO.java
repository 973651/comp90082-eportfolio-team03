package dao;

import bean.Education;

import java.util.ArrayList;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/4/30 下午12:35
 */
public interface EducationDAO {

    public ArrayList<Education> queryAllEducationById(int id);

}
