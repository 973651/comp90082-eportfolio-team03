package helper;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/4/30 下午10:29
 */
public class FileClassificationHelper {

    public static String classifyFiles(String filename){
        System.out.println(filename);
        String x = "abcde_aef.pdf";

        String[] suffix = x.split(".");
        System.out.println(suffix);
        if (suffix.equals("bmp") || suffix.equals("jpg") ||
                suffix.equals("jpeg") || suffix.equals("tif") ||
                suffix.equals("gif") || suffix.equals("svg")){
            return "image";
        }else if (suffix.equals("pdf")){
            return "pdf";
        }else if (suffix.equals("docx") || suffix.equals("doc")){
            return "word";
        }else{
            return "undefined";
        }

    }

}
