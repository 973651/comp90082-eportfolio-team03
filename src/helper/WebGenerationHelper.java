package helper;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 * @date 2020/4/21 下午9:49
 */
public class WebGenerationHelper {


    /**
     * Read template file then display
     * @param template page
     * @param response
     * @throws IOException
     */
    public static void writeHtmlByName(String htmlQualifiedName, HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("UTF-8");
        System.out.println("write By Name");

        OutputStream outStream = response.getOutputStream();
        try {
            FileInputStream fip = new FileInputStream(htmlQualifiedName);
            // 建立缓冲区
            byte[] buffer = new byte[1024];
            int len;
            while ((len = fip.read(buffer)) != -1) {
                outStream.write(buffer, 0, len);
            }
            fip.close();
            outStream.close();
            // 关闭输入流,释放系统资源
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
