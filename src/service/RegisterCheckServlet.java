package service;

import dao.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RegisterCheckUtil", urlPatterns = "/RegisterCheckUtil")
public class RegisterCheckServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");

        String email = request.getParameter("email");
        System.out.println("check email: "+email);
        UserDAO userDAO = new UserDAO();
        if(!userDAO.emailFormat(email)){
            response.getWriter().print("format wrong");
        } else if(userDAO.emailExist(email)){
            response.getWriter().print("exist");
        } else{
            response.getWriter().print("pass");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
