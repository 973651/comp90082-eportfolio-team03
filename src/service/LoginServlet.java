package service;

import bean.User;
import dao.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

@WebServlet(name = "LoginServlet", urlPatterns = "/LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        String type = request.getParameter("type");
        PrintWriter out = response.getWriter();

        if(type.equals("login")){
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            UserDAO userDAO = new UserDAO();
            User user = userDAO.login(email, password);
            if(user != null){
                Cookie useridCookie = new Cookie("userid", String.valueOf(user.getUserid()));
                response.addCookie(useridCookie);
                out.print("success");
            } else{
                out.print("failed");
            }
        } else{
            //logout
            Cookie[] cookies = request.getCookies();
            if(cookies==null || cookies.length==0){
                out.print("no user login");
                return;
            } else{
                for(Cookie c: cookies){
                    c.setMaxAge(0);
                    response.addCookie(c);
                }
                out.print("logout");
            }
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
