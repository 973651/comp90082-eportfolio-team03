CREATE DATABASE ePortfolio_database;
use ePortfolio_database;

create table User(
	userId int primary key auto_increment,
    name varchar(45),
    email varchar(45),
    passward varchar(45),
    DOB date,
    gender tinyint(10),
    phone int,
    address varchar(255),
    photo blob,
    description text
);
select * from User;
alter table user change column passward password varchar(45);

create table UserInfo(
	inforID int primary key auto_increment,
    info varchar(45)
);
alter table UserInfo change column inforID infoId int;
select * from UserInfo;

create table education(
	educationId int primary key auto_increment,
    userId int,
    infoId int,
    school varchar(255),
    major varchar(255),
    enrollmentTime date,
    graduationTime date,
    mainCourse text
);
alter table education rename Education;

create table WorkingExperience(
	workId int primary key auto_increment,
    userId int,
    infoId int,
    jobName varchar(50),
    company varchar(255),
    startTime date,
    endTime date,
    workDescription text,
    jobImg blob,
    jobVideo longblob
);

create table awards(
	awardId int primary key auto_increment,
    userId int,
    infoId int,
    awardName varchar(255),
    awardDesciption text,
    awardTime date,
    awardImg blob,
    awardVideo longblob
);
alter table awards rename award;
alter table award change column awardDesciption awardDescription text;
alter table award drop column awardImg;
alter table award drop column awardVideo;

create table visibility(
	userId int,
    infoId int,
    visibleTo int,
    visibilityId varchar(45) primary key
);

create table project(
	projectId int primary key auto_increment,
    userId int,
    infoId int,
    projectName varchar(255),
    projectDescription text,
    startTime date,
    endTime date,
    projectImg blob,
    projectVideo varchar(255)
);
alter table project add column content text;
alter table project add column document text;
alter table project add column visibility tinyint(1);
alter table project drop column infoId;
alter table project add column visibleTo text;

create table request(
	requestId int primary key auto_increment,
    projectId int,
    requestFrom int,
    requestTime date,
    requestContent text
);
alter table request add column requestStatus tinyint(1);

create table Tag(
	userId int,
    projectid int,
    projecttag varchar(255)
);
