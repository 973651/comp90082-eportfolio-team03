$(function() {
    // 填充
    getAwards();

    $('#time').datepicker({
        autoclose: true,
        clearBtn: true,
        todayBtn: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
    });

});

function getAwards(){
    var strcookie = document.cookie;
    var userIdStr = strcookie.split(";")[0];
    var userId = userIdStr.split("=")[1];
    var item = 0;
    $.ajax({
        type: 'get',
        url: 'AwardsServlet',
        data: {
            "type": "query",
            "userId": userId
        },
        dataType: 'json',
        success: function(data){
            console.log(data);
            if(data==null || data==""){
                //alert(data);
            } else{
                $('#awardLists').empty();
                for(x in data){
                    item = item+1;
                    loadAward(data[x], item);
                }
                console.log($('.awardDate input'));
                $('.awardDate input').datepicker({
                    autoclose: true,
                    clearBtn: true,
                    todayBtn: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd'
                });

            }
        },
        error : function(msg) {
            alert("get awards failed");
            console.log(msg)
        }
    });
}

function loadAward(data, item){
    var id = data.awardid;
    $('#awardLists').append('<span>Award Item '+item+'</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <button class="delete" onclick="awardDeleteConfirm('+id+')">Delete</button> \n' +
        '                            <div class="tab-pane active">\n' +
        '                                <form class="form-horizontal">\n' +
        '                                    <input type="text" name="edit_awardId" value="'+id+'" style="display: none">\n' +
        '                                    <div class="form-group">\n' +
        '                                        <label class="col-sm-2 control-label">Award Name</label>\n' +
        '                                        <div class="col-sm-8">\n' +
        '                                            <input type="text" class="form-control1" name="edit_awardName" value="' + data.awardname + '">\n' +
        '                                        </div>\n' +
        '                                    </div>\n' +
        '\n' +
        '                                    <div class="form-group">\n' +
        '                                        <label class="col-sm-2 control-label">Issuing Dates</label>\n' +
        '                                        <div class="col-sm-8">\n' +
        '                                            <div class="awardDate">' +
        '                                               <input type="text" class="form-control1" placeholder="yyyy-mm-dd" name="edit_awardTime" value="' + data.str_awardtime + '">\n' +
        '                                            </div>\n'+
        '                                        </div>\n' +
        '                                    </div>\n' +
        '\n' +
        '                                    <div class="form-group mb-n">\n' +
        '                                        <label class="col-sm-2 control-label label-input-lg">Description</label>\n' +
        '                                        <div class="col-sm-8">\n' +
        '                                            <input type="text" class="form-control1 input-lg" name="edit_awardDes" value="' + data.awarddescription+ '">\n' +
        '                                        </div>\n' +
        '                                    </div>\n' +
        '                                </form>\n' +
        '                            </div>');
}

function editAwards(){
    var strcookie = document.cookie;
    var userIdStr = strcookie.split(";")[0];
    var userId = userIdStr.split("=")[1];
    var names = document.getElementsByName("edit_awardName");
    var times = document.getElementsByName("edit_awardTime");
    var descriptions = document.getElementsByName("edit_awardDes");
    var awardIds = document.getElementsByName("edit_awardId");
    var n = names.length;
    var count = 0;
    for(i=0; i<n; i++){
        if(times[i].value==null || times[i].value.length==0){
            times[i].value = "9999-12-31";
        }
        $.ajax({
            type: 'get',
            url: 'AwardsServlet',
            data: {
                "type": "edit",
                "userId": userId,
                "awardId": awardIds[i].value,
                "awardName": names[i].value,
                "description": descriptions[i].value,
                "awardTime": times[i].value
            },
            dataType: 'text',
            success: function(data){
                count ++;
                if (count === n){
                    alert(" Successfully Upload Information !");
                    $('#awardLists').html("");
                    getAwards();
                }
            },
            error : function(msg) {
                alert("edit award" +i+" failed");
                console.log(msg)
            }
        });
    }
}

function addAward(){
    var strcookie = document.cookie;
    var userIdStr = strcookie.split(";")[0];
    var userId = userIdStr.split("=")[1];
    var name = $("#awardName").val();
    var description = $("#description").val();
    var time = $("#time").val();
    if(time==null || time.length==0){
        time = "9999-12-31";
    }
    $.ajax({
        type: 'get',
        url: 'AwardsServlet',
        data: {
            "type": "add",
            "userId": userId,
            "infoId": 2,
            "awardName": name,
            "description": description,
            "awardTime": time
        },
        dataType: 'text',
        success: function(data){
            if(data == "success"){
                alert("Add Award Success !");
                window.location.href="admin.html";
            } else{
                alert("Insert Failed");
            }
        }
    });
}

function awardDeleteConfirm(id){
    $.confirm({
        title: 'Item Delete Confirm',
        content: 'Are You Sure You Want to Delete this Award?\n',
        icon: 'fa fa-warning',
        theme: 'supervan',
        buttons: {
            ok: {
                text: "ok",
                btnClass: 'btn-primary',
                keys: ['enter'],
                action: function() {
                    deleteAward(id);
                }
            },
            cancel: function(){
                console.log('the user clicked cancel');
            }
        }
    });
}

function deleteAward(id){
    $.ajax({
        type: 'get',
        url: 'AwardsServlet',
        data: {
            "type": "delete",
            "awardId": id,
        },
        dataType: 'text',
        success: function(data){
            if(data == "success"){
                $('#awardLists').html("");
                getAwards();
            } else{
                alert("Delete Failed");
            }
        },
        error : function(msg) {
            alert("Delete Award Failed");
            console.log(msg)
        }
    });
}