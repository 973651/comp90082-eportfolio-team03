function getAwards(userId){
    var strcookie = document.cookie;
    var userIdStr = strcookie.split(";")[0];
    var userId = userIdStr.split("=")[1];
    $.ajax({
        type: 'get',
        url: 'AwardsServlet',
        data: {
            "type": "query",
            "userId": userId
        },
        dataType: 'json',
        success: function(data){
            console.log(data);
            if(data==null || data==""){
                $("#award-display").html("");
                $("#award-display").append('<div class="me-empty">\n' +
                    '                <div class="tips">\n' +
                    '                    <p>You haven\'t fill in any award information.</p>\n' +
                    '                    <p>Feel free to edit new information.</p>\n' +
                    '                </div>\n' +
                    '                <div class="add-file">\n' +
                    '                    <a href="admin.html">\n' +
                    '                        <i class="fa fa-plus fa-3x"></i>\n' +
                    '                    </a>\n' +
                    '                </div>\n' +
                    '            </div>');
            } else{
                $("#award-display").html("");
                $("#award-display").append('<div class="main-award" id="award-box"></div>');
                for(x in data){
                    loadAward(data[x]);
                }
            }
        },
        error : function(msg) {
            alert("get awards failed");
            console.log(msg)
        }
    });
}

function getOthersAwards(){
    var url = window.location.href;
    var userId = url.split("id=")[1].split("#")[0];
    $.ajax({
        type: 'get',
        url: 'AwardsServlet',
        data: {
            "type": "query",
            "userId": userId
        },
        dataType: 'json',
        success: function(data){
            console.log(data);
            if(data==null || data==""){
                $("#award-display").html("");
                $("#award-display").append('<div class="empty">\n' +
                    '                <span class="tip">Sorry, this user didn\'t fill in award information.</span>\n' +
                    '            </div>');
            } else{
                $("#award-display").html("");
                $("#award-display").append('<div class="main-award" id="award-box"></div>');
                for(x in data){
                    loadAward(data[x]);
                }
            }
        },
        error : function(msg) {
            alert("get awards failed");
            console.log(msg)
        }
    });
}

function loadAward(data) {
    $('#award-box').append('<div class="award">\n' +
        '                    <div class="award-icon"></div>\n' +
        '                    <div class="award-content">\n' +
        '                        <span class="date">' + data.str_awardtime + '</span>\n' +
        '                        <h5 class="title">' + data.awardname + '</h5>\n' +
        '                        <p class="description">' + data.awarddescription+ '</p>\n' +
        '                    </div>\n' +
        '                </div>');
}
