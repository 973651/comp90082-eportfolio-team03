function setVisibility(){
    var projectId = 1;
    var options = $("#privacy option:selected").val();
    var value = 0;
    if(options == "public") value=2;
    else if(options == "apply") value=1;
    $.ajax({
        type: 'get',
        url: 'PrivacyServlet',
        data: {
            "type": "setVisibility",
            "projectId": projectId,
            "visibility": value
        },
        dataType: 'text',
        success: function(data){
            if(data == "success"){
                //alert("set success");
            } else{
                alert("set failed");
            }
        },
        error : function(msg) {
            alert("set privacy failed");
            console.log(msg)
        }
    });
}

setVisibleTo = function(projectId, users){
    var projectId = projectId;
    var toUser = users;
    var userLists = getVisibleToUser(projectId);
    $.ajax({
        type: 'get',
        url: 'PrivacyServlet',
        async: false,
        data: {
            "type": "setVisibleTo",
            "projectId": projectId,
            "toUser": toUser,
            "toUserList": userLists
        },
        dataType: 'text',
        success: function(data){
            if(data == "success"){
                //alert("set visible to success");
            } else{
                alert("set visible to failed");
            }
        },
        error : function(msg) {
            alert("set visible to error");
            console.log(msg)
        }
    });
}

function getVisibleToUser(projectId){
    var visibleToUsers = "";
    var projectId = projectId;
    $.ajax({
        type: 'get',
        url: 'DisplayServlet',
        async: false,
        data: {
            "type": "queryOne",
            "projectId": projectId
        },
        dataType: 'json',
        success: function(data){
            visibleToUsers = data[0].visibleto;
        },
        error : function(msg) {
            alert("get privacy failed");
            console.log(msg);
        }
    });
    return visibleToUsers;
}


