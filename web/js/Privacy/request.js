function getDate(){
    var date = new Date();
    var y = date.getFullYear();
    var m = date.getMonth()+1;
    var d = date.getDate();
    return y+"-"+m+"-"+d;
}

function getReqeustedProjects(){
    var strcookie = document.cookie;
    var userIdStr = strcookie.split(";")[0];
    var userId = userIdStr.split("=")[1];
    $.ajax({
        type: 'get',
        url: 'PrivacyServlet',
        data: {
            "type": "getProjects",
            "userId": userId
        },
        dataType: 'json',
        success: function(data){
            console.log(data);
            if(data==null || data.length==0){
                $("#requestProjectList").append('<h4>No request.</h4>');
            } else{
                loadSubPage(data, 1);
            }
        },
        error : function(msg) {
            alert("get projects failed");
            console.log(msg)
        }
    });
}

function loadCandidates(projectId){
    $('#privacyContent').html("");
    $.ajax({
        url : '/webpage/privacy/privacy-candidate.html',
        //async : false,
        type : 'GET',
        success : function(data) {
            $('#privacyContent').append(data);
        }
    });
    getRequests(projectId);

}

function sendRequest(projectId,userId,ownerId){
    var time = getDate();
    var content = $("#reasonInput"+projectId).val();

    $.ajax({
        type: 'get',
        url: 'PrivacyServlet',
        data: {
            "type": "sentRequest",
            "userId": userId,
            "projectId": projectId,
            "requestTime": time,
            "requestContent": content
        },
        dataType: 'text',
        success: function(data){
            if(data == "success"){
                window.location.href="other-person.html?id="+ownerId;
            } else{
                alert("send failed");
            }
        },
        error : function(msg) {
            alert("send request failed");
            console.log(msg)
        }
    });
}

//Get all requests I have received
function getRequests(projectId) {
    $.ajax({
        type: 'get',
        url: 'PrivacyServlet',
        data: {
            "type": "getRequests",
            "projectId": projectId
        },
        dataType: 'json',
        success: function(data){
            console.log(data);
            var url = window.location.href;
            var projectName = url.split("project=")[1];
            projectName = projectName.replace(/%20/g, " ");
            for(var x in data){

                var radio1 = "radio1-"+data[x].projectid+"-"+data[x].requestfrom;
                var radio2 = "radio2-"+data[x].projectid+"-"+data[x].requestfrom;

                $("#candidateRequests").append('<li>\n' +
                    '                                    <div class="req-in">\n' +
                    '                                        <img src="'+data[x].photo+'" alt="" class="proj-img">\n' +
                    '                                        <div class="user-des">\n' +
                    '                                            <h3 style="text-decoration: underline"><a href="other-person.html?id='+data[x].requestfrom+'">'+data[x].name+'</a ></h3>\n' +
                    '                                            <h5>Application date: '+data[x].str_requesttime+'</h5>\n' +
                    '                                        </div>\n' +
                    '                                        <div class="req-detail">\n' +
                    '                                                <button class="request" onclick="openDetail('+data[x].requestid+')">Request Details</button>\n' +
                    '                                        </div>\n' +
                    '                                        <div class="req-choice">\n' +
                    '                                            <form name="approve" method="post">\n' +
                    '                                                <label style="color:#8BC34A">\n' +
                    '                                                    <input class="radioCheck" type="radio" name="grant" id="'+radio1+'" value="yes" tag="0"><label for="'+radio1+'">Approve</label>\n' +
                    '                                                </label>\n' +
                    '                                                <label style="color:#bb0000">\n' +
                    '                                                    <input class="radioCheck" type="radio" name="grant" id="'+radio2+'" value="no" tag="0"><label for="'+radio2+'">Refuse</label>\n' +
                    '                                                </label>\n' +
                    '                                            </form>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                </li>');

                $("#requestDetail").append('<div class="modal fade" id="see-req'+data[x].requestid+'" tabindex="-1" role="dialog" aria-labelledby="modal-register-label" aria-hidden="true">\n' +
                    '    <div class="modal-dialog">\n' +
                    '        <div class="modal-content" >\n' +
                    '            <div class="modal-header">\n' +
                    '                <button type="button" class="close" data-dismiss="modal">\n' +
                    '                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>\n' +
                    '                </button>\n' +
                    '                <h3 class="modal-title" id="modal-register-label" style="text-align: center">Application Details</h3>\n' +
                    '            </div>\n' +
                    '            <div class="modal-body">\n' +
                    '                <form class="form-horizontal">\n' +
                    '                    <div style="padding-left:30px;height:35px">\n' +
                    '                        <span style="font-weight: bold;padding-right:10px;color:#1976d2;">Request Project:</span>\n' +
                    '                        <span>'+projectName+'</span>\n' +
                    '                    </div>\n' +
                    '                    <div style="padding-left:30px;height:35px">\n' +
                    '                        <span style="font-weight: bold;padding-right:10px;color:#1976d2;">Applicant:</span>\n' +
                    '                        <span>'+data[x].name+'</span>\n' +
                    '                    </div>\n' +
                    '                    <div style="padding-left:30px;height:35px;padding-bottom:50px;">\n' +
                    '                        <span style="font-weight: bold;padding-right:10px;color:#1976d2;">Reason:</span>\n' +
                    '                        <span>'+data[x].requestcontent+'</span>\n' +
                    '                    </div>\n' +
                    '                </form>\n' +
                    '            </div>\n' +
                    '        </div>\n' +
                    '    </div>\n' +
                    '</div>');

            }
        },
        error : function(msg) {
            alert("get requests failed");
            console.log(msg)
        }
    });
    return requestUsers;
}

function openDetail(requestId){
    // var url = window.location.href;
    // var projectId = url.split("=")[1];
    $("#see-req"+requestId).modal();
}

/**
 * accept or refuse requests with project and user id
 * @param projectId
 * @param users List of users stores in string with ","
 * @param status 0: not processed, 1: refuse, 2: accept
 */
function processRequest(projectId, users, status){
    $.ajax({
        type: 'get',
        url: 'PrivacyServlet',
        async: false,
        data: {
            "type": "processRequest",
            "projectId": projectId,
            "users": users,
            "status": status
        },
        dataType: 'text',
        success: function(data){
            if(data == "success"){
                //alert("process success");
            } else{
                console.log("process projectid "+projectId+" users "+users+" :"+data);
            }
        },
        error : function(msg) {
            alert("process request failed");
            console.log(msg)
        }
    });
}

function agreeAll(projectId){
    var requestUsers = "";
    $.ajax({
        type: 'get',
        url: 'PrivacyServlet',
        async: false,
        data: {
            "type": "getRequests",
            "projectId": projectId
        },
        dataType: 'json',
        success: function (data) {
            console.log(data);
            for (var x in data) {
                requestUsers += data[x].requestfrom + ",";
            }
        }
    });
    processRequest(projectId, requestUsers, 2);
    setVisibleTo(projectId, requestUsers);
    toProjectList();
}

function toProjectList(){
    $('#page-wrapper').html("");
    $.ajax({
        url : 'webpage/privacy/privacy-list.html',
        async : false,
        type : 'GET',
        success : function(data) {
            $('#page-wrapper').append(data);
        }
    });
}

function submitSelections(){
    var checkInputs = document.querySelectorAll('input:checked');
    var acceptUsers = "";
    var refuseUsers = "";
    var projectId = -1;
    for(var i = 0;i<checkInputs.length;i++){
        var v = checkInputs[i].value;
        var id = checkInputs[i].id;
        projectId = id.split("-")[1];
        var userId = id.split("-")[2];
        if(v == "yes")  acceptUsers += userId+",";
        else    refuseUsers += userId+",";
    }
    $.confirm({
        title: 'Sure to submit?',
        content: '',
        icon: 'fa fa-warning',
        theme: 'white',
        buttons: {
            ok: {
                text: "ok",
                btnClass: 'btn-primary',
                keys: ['enter'],
                action: function() {
                    if(acceptUsers.length != 0){
                        processRequest(projectId, acceptUsers, 2);
                        setVisibleTo(projectId, acceptUsers);
                    }
                    if(refuseUsers.length != 0){
                        processRequest(projectId, refuseUsers, 1);
                    }
                    if(projectId != -1){
                        loadCandidates(projectId);
                    }
                }
            },
            cancel: function(){
                console.log('the user clicked cancel');
            }
        }
    });
}

//Requests sent by myself
function getMyRequests(){
    var strcookie = document.cookie;
    var userIdStr = strcookie.split(";")[0];
    var userId = userIdStr.split("=")[1];

    $.ajax({
        type: 'get',
        url: 'PrivacyServlet',
        data: {
            "type": "getMyRequests",
            "userId": userId
        },
        dataType: 'json',
        success: function(data){
            console.log(data);
            if(data==null || data.length==0){
                $("#myRequestsList").append('<h4>No request.</h4>');
            } else{
                loadSubPage(data, 2);
            }
        },
        error : function(msg) {
            alert("get my requests failed");
            console.log(msg)
        }
    });
}

function deleteRequest(requestId){
    $.ajax({
        type: 'get',
        url: 'PrivacyServlet',
        data: {
            "type": "deleteRequest",
            "requestId": requestId
        },
        dataType: 'text',
        success: function(data){
            if(data == "success"){
                $('#page-wrapper').html("");
                $.ajax({
                    url : 'webpage/privacy/privacy-my.html',
                    async : false,
                    type : 'GET',
                    success : function(data) {
                        $('#page-wrapper').append(data);
                    }
                });
            } else{
                alert("delete failed");
            }
        },
        error : function(msg) {
            alert("delete request failed");
            console.log(msg)
        }
    });
}