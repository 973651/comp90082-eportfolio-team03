/**
 * @author Boyan XIN
 * @version 1.1 some minor edit of l()
 * @contact boyanx@student.unimelb.edu.au
 */


$('#backgroudInfo').on('click', function(){
    console.log("start to load background info");
    $('#page-wrapper').html("");
    $.ajax({
        url : '/webpage/admin-related/admin-background.html', // 这里是静态页的地址
        async : false,
        type : 'GET', // 静态页用get方法，否则服务器会抛出405错误
        success : function(data) {
            $('#page-wrapper').append(data);
        }
    });
});

$('#accountInfo').on('click', function(){
    console.log("start to load account info");
    $('#page-wrapper').html("");
    $.ajax({
        url : '/webpage/admin-related/admin-account.html', // 这里是静态页的地址
        async : false,
        type : 'GET', // 静态页用get方法，否则服务器会抛出405错误
        success : function(data) {
            $('#page-wrapper').append(data);
        }
    });
});

$('#changePassword').on('click', function(){
    console.log("start to load change password");
    $('#page-wrapper').html("");
    $.ajax({
        url : '/webpage/admin-related/admin-password.html', // 这里是静态页的地址
        async : false,
        type : 'GET', // 静态页用get方法，否则服务器会抛出405错误
        success : function(data) {
            $('#page-wrapper').append(data);
        }
    });
});

// $('#projectList').on('click', function(){
//     console.log("start to load project list");
//     $('#page-wrapper').html("");
//     $.ajax({
//         url : '/webpage/project-related/project-list.html', // 这里是静态页的地址
//         async : false,
//         type : 'GET', // 静态页用get方法，否则服务器会抛出405错误
//         success : function(data) {
//             console.log(data.innerHTML);
//             $('#page-wrapper').append(data);
//         }
//     });
// });

$('#privacyList').on('click', function(){
    console.log("start to load privacy list");
    $('#page-wrapper').html("");
    $.ajax({
        url : 'webpage/privacy/privacy-list.html', // 这里是静态页的地址
        async : false,
        type : 'GET', // 静态页用get方法，否则服务器会抛出405错误
        success : function(data) {
            $('#page-wrapper').append(data);
        }
    });
});

$('#privacyMy').on('click', function(){
    console.log("start to load my application");
    $('#page-wrapper').html("");
    $.ajax({
        url : 'webpage/privacy/privacy-my.html', // 这里是静态页的地址
        async : false,
        type : 'GET', // 静态页用get方法，否则服务器会抛出405错误
        success : function(data) {
            $('#page-wrapper').append(data);
        }
    });
});

$(function(){
    $.ajax({
        url : '/webpage/admin-related/admin-account.html', // 这里是静态页的地址
        async : false,
        type : 'GET', // 静态页用get方法，否则服务器会抛出405错误
        success : function(data) {
            $('#page-wrapper').append(data);
        }
    });
});

