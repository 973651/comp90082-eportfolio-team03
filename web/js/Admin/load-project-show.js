jQuery(document).ready(function() {
    console.log("start to load project show");
    document.querySelectorAll("a").on('click', function(){
        console.log("start to load project show");
        $('#project-list').html("");
        $.ajax({
            url : '/webpage/project-related/project-show.html', // 这里是静态页的地址
            async : false,
            type : 'GET', // 静态页用get方法，否则服务器会抛出405错误
            success : function(data) {

                $('#project-list').append(data);
            }
        });
    });
});
