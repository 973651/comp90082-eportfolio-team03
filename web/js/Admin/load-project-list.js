$(function(){
    console.log("listen click event");
    $('#backLink2').on('click', function(){
        console.log("start to load project list back");
        $('#project-show').html("");
        $.ajax({
            url : '/webpage/project-related/project-list.html', // 这里是静态页的地址
            async : false,
            type : 'GET', // 静态页用get方法，否则服务器会抛出405错误
            success : function(data) {

                $('#project-show').append(data);
            }
        });
    });

});