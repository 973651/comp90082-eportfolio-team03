/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 */
$(document).ready(

    function(){
        chooseSearchType();
        searchKeyEvent();
    }
);

/**
 * 选择搜索类型
 *
 * @returns
 */
function chooseSearchType() {

    $('#search_people').click(
        function() {
            var sel1 = document.getElementById("majorFilter");
            var sel2 = document.getElementById("jobFilter");
            var majorVal = sel1.value;
            var jobVal = sel2.value;
            console.log("button clicked");
            searchByKeyWords(majorVal,jobVal);
        });
}

/**
 * Search event, jump to a new page
 */
function searchByKeyWords(majorType, jobType){

    $(window).attr('location', '/searchSvt?keywords=' + $('#search_keywords').val() +'&fil1='+ majorType +'&fil2='+ jobType);

}

/**
 * 搜索输入框绑定回车键
 *
 * @returns
 */
function searchKeyEvent() {
    $('#search_keywords').keypress(function(event) {
        if (event.keyCode == 13) {
            searchByKeyWords();
        }
    });
}