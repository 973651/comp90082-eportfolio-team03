/**
 * Define parameters
 * @param data all json data
 * @param type 1：load project list in requests
 *             2: load request list sent by current user
 */
loadSubPage = function(data, type){
    var pageSize = 3;
    var pageNum = Math.ceil(data.length/pageSize);
    var dataStr = JSON.stringify(data);

    if(pageNum == 0){
        //$("#page-bar").append('<h4>No data here</h4>');
    } else{
        $("#page-bar").append('<div id="currentPage" style="display: none"></div>');    //to save current page's number
        $("#page-bar").append("<li id='previousBtn' class='page-item disabled'>\n" +
            "                                            <a class='page-link' href='#' tabindex='-1' onclick='changePage("+"-1,"+dataStr+","+i+","+pageSize+","+pageNum+","+type+")'>Previous</a>\n" +
            "                                        </li>");

        for(var i=1; i<=pageNum; i++){
            $("#page-bar").append("<li class='page-item' id='subpage"+i+"'><a class='page-link' onclick='loadParticularPage("+dataStr+","+i+","+pageSize+","+pageNum+","+type+")' href='#'>"+i+"</a></li>");
        }

        $("#page-bar").append("<li id='nextBtn' class='page-item'>\n" +
            "                                            <a class='page-link' href='#' tabindex='1' onclick='changePage("+"1,"+dataStr+","+i+","+pageSize+","+pageNum+","+type+")'>Next</a>\n" +
            "                                        </li>");

        loadParticularPage(data, 1, pageSize, pageNum, type);
    }

}

function changePage(move, dataStr, p, pageSize, pageNum, type){
    var curPage = $("#currentPage").html();
    var newPage = parseInt(curPage)+move;
    if(newPage>0 && newPage<=pageNum){
        loadParticularPage(dataStr, newPage, pageSize, pageNum, type);
    }
}

function loadParticularPage(dataStr, p, pageSize, pageNum, type) {
    var data = eval(dataStr);

    //save current page's number
    $("#currentPage").html(p);

    //style preparation
    if(p > 1)   $("#previousBtn").attr("class", "page-item");
    else    $("#previousBtn").attr("class", "page-item disabled");

    if(p == pageNum)    $("#nextBtn").attr("class", "page-item disabled");
    else    $("#nextBtn").attr("class", "page-item");

    for(var i=1; i<=pageNum; i++){
        $("#subpage"+i).attr("class", "page-item");
    }
    $("#subpage"+p).attr("class", "page-item active");

    var start = (parseInt(p)-1)*pageSize;

    if(type == 1){
        //clear original list
        $("#requestProjectList").html("");
        for(var x=start; x<start+pageSize; x++){
            var projectId = data[x].projectid;
            $("#requestProjectList").append(' <li>\n' +
                '                      <div class="req-in">\n' +
                '                      <div class="proj-des">\n' +
                '                      <h3><a href="person-project.html?id='+data[x].projectid+'">' + data[x].projectname + '</a></h3>\n' +
                '                      <h5>' + data[x].projectdescription + '</h5>\n' +
                '                      </div>\n' +
                '                      <div class="req-des">\n' +
                '                      <span>Total number of Applicants: ' + data[x].countrequest + '</span>\n' +
                '                      <div class="req-button">\n' +
                '                      <a href="#project=' + data[x].projectname + '"><button class="candidate" onclick="loadCandidates('+data[x].projectid+')">See applicants</button></a>\n' +
                '                      <button class="agree" onclick="agreeAll('+projectId+')">Agree all</button>\n' +
                '                      </div>\n' +
                '                      </div>\n' +
                '                      </div>\n' +
                '                      </li>');
        }
    } else if(type == 2){
        $("#myRequestsList").html("");
        for(var x=start; x<start+pageSize; x++){
            var statusNum = data[x].requeststatus;
                var status = "Unhandled";
                if(statusNum == '1')    status = "Refused";
                else if(statusNum == '2')   status = "Approved";
                $("#myRequestsList").append(' <li>\n' +
                    '                                <div class="req-in">\n' +
                    '                                    <div class="req-detail">\n' +
                    '                                        <span style="font-size: x-large; ">'+data[x].projectname+'</span>\n' +
                    '                                        <h4>Project Owner: <a href="other-person.html?id='+data[x].userid+'">'+data[x].name+'</a>  Apply date: '+data[x].str_requesttime+'</h4>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="req-status">\n' +
                    '                                        <span>Status:</span>\n' +
                    '                                        <span class="status">'+status+'</span>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="req-btn"><input type="button" onclick="deleteRequest('+data[x].requestid+')" value="DELETE"/></div>' +
                    '                                </div>\n' +
                    '                            </li>');
        }
    } else if (type ==3){
        $('#projectByTag').html("");
        for(var x=start; x<start+pageSize; x++){
            var project = data[x];
            $('#projectByTag').append('<li><span class="proj-title"><a href="#keywords='+project.projectid+'">'+ project.projectname +'</a></span>\n' +
                '<a href="/webpage/project-related/editor.html?keywords='+ project.projectid +'">' +
                '   <input class="edit" type="button" value="EDIT"/>' +
                '</a>\n' +
                '<input id="'+ project.projectid +'" class="delete-list" type="button" value="DELETE"/>\n' +
                '<h5>Project Period: '+project.str_starttime + ' - ' + project.str_endtime +'</h5>\n' +
                '<p>'+ project.projectdescription +'</p>\n' +
                '</li>');
        }
    } else if (type == 4){
        $('#projectAll').html("");
        for (var x=start; x<start+pageSize;x++){
            var project = data[x];
            $('#projectAll').append('<li><span class="proj-title"><a href="#keywords='+project.projectid+'">'+ project.projectname +'</a></span>\n' +
                '<a href="/webpage/project-related/editor.html?keywords='+ project.projectid +'">' +
                '   <input class="edit" type="button" value="EDIT"/>' +
                '</a>\n' +
                '<input id="'+ project.projectid +'" class="delete-list" type="button" value="DELETE"/>\n' +
                '<h5>Project Period: '+project.str_starttime + ' - ' + project.str_endtime +'</h5>\n' +
                '<p>'+ project.projectdescription +'</p>\n' +
                '</li>');
        }
    }

}