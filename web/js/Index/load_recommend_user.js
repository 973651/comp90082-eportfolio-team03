/**
 * @author Boyan XIN
 * @version 1.1 some minor edit of loadPeople()
 * @contact boyanx@student.unimelb.edu.au
 */


var $carousel = $('.flickity-slider-wrap').flickity();
$(function(){
    var strcookie = document.cookie;
    var userIdStr = strcookie.split(";")[0];
    var userId = userIdStr.split("=")[1];
    if (userId == undefined){
        console.log("no login user");
        var emptyId = 0;
        loadPeople(emptyId);
    } else{
        loadPeople(userId);
    }

});

/**
 * Query A list of people
 */
function loadPeople(userId){
    $.ajax({
        type: 'GET',
        async: false,
        url: '/query.people',
        dataType: 'json',
        data:{
            type: 'query_recommend',
            userId: userId
        },
        success: function (response) {
            console.log("success function response");
            for (x in response){
                loadRecommend(response[x])
            }
            $carousel.flickity('playPlayer');

        },
        error: function (XMLHttpRequest, textStatus) {
            $.confirm({
                title: 'Something Wrong with Your Search',
                content: textStatus + ' : ' + XMLHttpRequest,
                autoClose: 'ok|1000',
                type: 'red',
                buttons: {
                    ok: {
                        text: 'Confirm',
                        btnClass: 'btn-primary',
                    },
                }
            });
        }
    });
}

function loadRecommend(people_data) {

    jQuery(document).ready(function(){
        var $cellElems = $('<div class="gallery-cell">\n' +
            '                            <article>\n' +
            '                                  <a href="/other-person.html?id='+ people_data.userid +'" class="entry-img">\n' +
            '                                        <img src="'+people_data.photo+'" alt="">\n' +
            '                                     <span class="entry-category">'+people_data.major+'</span>\n' +
            '                                   </a>\n' +
            '                                     <div class="entry text-center">\n' +
            '                                        <h4 class="entry-title uppercase">'+people_data.name+'</h4>\n' +
            '                                       <span class="entry-date">'+people_data.job+'</span>\n' +
            '                                   </div>\n' +
            '                                </article>\n' +
            '                             </div>');
        $carousel.flickity('append', $cellElems);

    });

}

function loadToPage(){
    console.log(cell);
    // document.getElementById("main-slider").innerHTML = cell;
    $('#main-slider').html(cell);
}