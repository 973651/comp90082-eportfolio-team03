/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 */

/**
 * click to open reset modal
 */
function openReset(){
    clearTips();
    $("#modal-reseter").modal();
}

/**
 * send the reset password request email to user
 */
function askReset() {
    checkEmailExistence();
}



function checkEmailExistence(){
    var email = $("#reseter-email").val();
    $.ajax({
        type: 'get',
        url: 'RegisterCheckUtil',
        data: {
            "email": email
        },
        dataType: 'text',
        success: function(data){
            if(data == "format wrong"){
                //alert("Wrong email format, please input again!");
                $("#tip_rest_email").html("Wrong email format, please input again !");
            } else if(data == "exist"){
                //alert("Email has been used");
                $("#tip_rest_email").html("Correct User Email Account !");
                $.ajax({
                    type: 'post',
                    url: '/AskResetServlet',
                    async: false,
                    data: {
                        "email": email,
                    },
                    dataType: 'text',
                    success: function(data){
                        if(data == "success"){
                            alert("Already Send a Mail");
                            // clearModal();
                        } else{
                            alert("can not send a mail");
                        }
                    }
                });

            } else{
                $('#tip_rest_email').html("User Does Not Exist !");
                //alert("legal email");
            }
        },
        error : function(msg) {
            //alert("failed");
            console.log(msg)
        }
    });
}

function clearModal(){
    $('#reseter-email').html("");
}
