function openLogin(){
    clearTips();
    $("#modal-login").modal();
}

function login(){
    var email=$("#email").val();
    var password=$("#password").val();
    $.ajax({
        type: 'post',
        url: 'LoginServlet',
        data: {
            "type": "login",
            "email": email,
            "password": password
        },
        dataType: 'text',
        success: function(data){
            if(data == "failed"){
                $("#tip_1").html("Login failed, please check your email and password!");
            } else{
                window.location.href="index.html";
                //alert("登录成功，换成要跳转的页面");
            }
        },
        error : function(msg) {
            alert("failed");
            console.log(msg)
        }
    });
}

function clearTip() {
    $("#tip_1").empty();
}

function clearLoginInputs(){
    document.getElementById("email").value = "";
    document.getElementById("password").value = "";
}

function clearRegisterInputs(){
    document.getElementById("register-name").value = "";
    document.getElementById("register-email").value = "";
    document.getElementById("register-password").value = "";
}

function checkName(){
    var password=$("#register-name").val();
    if(password.length == 0){
        $("#tip_name").html("User name cannot be empty");
    }
}

function checkEmail(){
    var email=$("#register-email").val();
    $.ajax({
        type: 'get',
        url: 'RegisterCheckUtil',
        data: {
            "email": email
        },
        dataType: 'text',
        success: function(data){
            if(data == "format wrong"){
                //alert("Wrong email format, please input again!");
                $("#tip_email").html("Wrong email format, please input again!");
            } else if(data == "exist"){
                //alert("Email has been used");
                $("#tip_email").html("Email has been used");
            } else{
                //alert("legal email");
            }
        },
        error : function(msg) {
            //alert("failed");
            console.log(msg)
        }
    });
}

function checkPassword(){
    var password=$("#register-password").val();
    if(password.length < 6){
        //alert("Password should not be less than 6 characters");
        $("#tip_password").html("Password should not be less than 6 characters");
    }
}

function clearTips() {
    $("#tip_1").empty();
    $("#tip_name").empty();
    $("#tip_email").empty();
    $("#tip_password").empty();
    $("#tip_failed").empty();
}

function register(){
    var name = $("#register-name").val();
    var email = $("#register-email").val();
    var password = $("#register-password").val();
    $.ajax({
        type: 'post',
        url: 'RegisterServlet',
        data: {
            "type": "register",
            "name": name,
            "email": email,
            "password": password
        },
        dataType: 'text',
        success: function(data){
            if(data == "success"){
                window.location.href="index.html";
                //alert("注册成功，换成要跳转的页面");
            } else{
                $("#tip_failed").html("Sign up failed, please check your email and password!");
            }
        },
        error : function(msg) {
            //alert("failed");
            console.log(msg)
        }
    });
}

function loadLoginState(){
    var strcookie = document.cookie;
    if(strcookie==null || strcookie.length==0){
        //alert("aab");
        $("#userIcon").append('<a class="nav-link dropdown-toggle" data-toggle="" onclick="confirmToLogin()" role="button" aria-haspopup="true"\n' +
            '                   aria-expanded="false" href="#"><i class="fa fa-user"></i></a >');
    } else{
        $("#userIcon").append('<a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"\n' +
            '                   aria-expanded="false" href=""><i class="fa fa-user"></i></a >\n' +
            '                <ul class="dropdown-menu">\n' +
            '                    <li><a class="nav-link" href="person.html">View Profile</a ></li>\n' +
            '                    <li><a class="nav-link" href="admin.html">My Admin</a ></li>\n' +
            '                    <li><a class="nav-link" href="#" onclick="logout()" style="padding:0.5rem"><i class="fa fa-power-off"></i> Log Out</a ></li>\n' +
            '                    <li><a class="nav-link" href="#" onclick="accountDeleteConfirm()" style="padding:0.5rem"><i class="fa fa-minus-circle"></i> Delete Account</a ></li>\n' +
            '                </ul>');
    }
}

function loadIndexLoginState(){
    var strcookie = document.cookie;
    if(strcookie==null || strcookie.length==0){
        $("#userIcon").append('<li class="nav-item dropdown">\n' +
            '                        <a class="nav-link dropdown-toggle" data-modal-id="modal-login" onclick="openLogin()" href="#"><i class="fa fa-user"></i></a >\n' +
            '                    </li>\n' +
            '                    <li class="nav-item active"><a class="nav-link" href="index.html"><i class="fa fa-home"></i></a ></li>');
    } else{
        $("#userIcon").append('<li class="nav-item dropdown">\n' +
            '                        <a class="nav-link dropdown-toggle" data-toggle="" role="button" aria-haspopup="true"\n' +
            '                           aria-expanded="false" href="#"><i class="fa fa-user"></i></a >\n' +
            '                        <ul class="dropdown-menu">\n' +
            '                            <li><a class="nav-link" href="person.html">View Profile</a ></li>\n' +
            '                            <li><a class="nav-link" href="admin.html">My Admin</a ></li>\n' +
            '                            <li><a class="nav-link" href="#" onclick="adminLogout()" style="padding:0.5rem"><i class="fa fa-power-off"></i> Log Out</a ></li>\n' +
            '                            <li><a class="nav-link" href="#" onclick="accountDeleteConfirm()" style="padding:0.5rem"><i class="fa fa-minus-circle"></i> Delete Account</a ></li>\n' +
            '                        </ul>\n' +
            '                    </li>\n' +
            '                    <li class="nav-item active"><a class="nav-link" href="index.html"><i class="fa fa-home"></i></a ></li>');
    }
}

function confirmToLogin(){
    $.confirm({
        title: 'You need login to see further pages',
        content: 'Are You Sure Going Back And Login?\n',
        icon: 'fa fa-warning',
        theme: 'white',
        buttons: {
            ok: {
                text: "ok",
                btnClass: 'btn-primary',
                keys: ['enter'],
                action: function() {
                    window.location.href="index.html";
                }
            },
            cancel: function(){
                console.log('the user clicked cancel');
            }
        }
    });
}

function openHomepage(){
    window.location.href="person.html";
}

function logout(){
    $.confirm({
        title: 'Sure to logout?',
        content: '',
        icon: 'fa fa-warning',
        theme: 'white',
        buttons: {
            ok: {
                text: "ok",
                btnClass: 'btn-primary',
                keys: ['enter'],
                action: function() {
                    adminLogout();
                }
            },
            cancel: function(){
                console.log('the user clicked cancel');
            }
        }
    });
}

//Logout without confirm
function adminLogout(){
    $.ajax({
        type: 'post',
        url: 'LoginServlet',
        data: {
            "type": "logout"
        },
        dataType: 'text',
        success: function(data){
            if(data == "logout"){
                window.location.href="index.html";
            } else{
                alert("no user now");
            }
        },
        error : function(msg) {
            alert("logout failed");
            console.log(msg)
        }
    });
}

function accountDeleteConfirm(){
    $.confirm({
        title: 'Confirmation of Delete Account',
        content: 'Are You Sure to Delete Your Account ?\n' +
        'All of Your Information Will be Deleted and Cannot Recovered',
        icon: 'fa fa-warning',
        theme: 'supervan',
        buttons: {
            ok: {
                text: "ok",
                btnClass: 'btn-primary',
                keys: ['enter'],
                action: function() {
                    deleteAccount();
                }
            },
            cancel: function(){
                console.log('the user clicked cancel');
            }
        }
    });
}

function deleteAccount(){

    var strcookie = document.cookie;
    var userIdStr = strcookie.split(";")[0];
    var userId = userIdStr.split("=")[1];
    $.ajax({
        type : 'POST',
        async : false,
        url : '/delete.account',
        data : {
            type : 'delete_account',
            userId : userId,
        },
        success : function(response) {
            if (response == -1) {
                alert("Delete Account Faild");
            } else {
                adminLogout();
                alert("Successfully Delete Your Account !");
            }
        },
        error : function(XMLHttpRequest, textStatus) {

        }
    });


}