/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 *
 */

var flag = 1;

$(function() {
    // 填充
    getTags();

});

/**
 * get ones all project tag without duplicate
 */
function getTags(){
    //var userId=$("#userId").val();
    var strcookie = document.cookie;
    var userIdStr = strcookie.split(";")[0];
    var userId = userIdStr.split("=")[1];
    var item = 0;
    $.ajax({
        type: 'post',
        url: '/query.tag',
        data: {
            "type": "queryAllTag",
            "userid": userId
        },
        dataType: 'json',
        success: function(data){
            $('#taglist').append('<li><a id="secondbar" href="#keywords=All" class=" hvr-bounce-to-right"><i class="fa fa-list nav_icon"></i>All Projects</a></li>')
            for(x in data){
                loadTagToPage(data[x]);
            }
            loadButton();
            buttonDisplaySetting();
            clickBarEvent();

        },
        error : function(msg) {
            alert("get tags failed");
            console.log(msg)
        }
    });
}

/**
 * load tag in to the leftside bar
 * @param tags
 */
function loadTagToPage(tags){
    // console.log(tags.projecttag);
    $('#taglist').append('<li><a id="secondbar" href="#keywords='+tags.projecttag+'" class=" hvr-bounce-to-right"><i class="fa fa-list nav_icon"></i>'+tags.projecttag+'</a></li>');
}

/**
 * load button
 */
function loadButton(){
    $('#taglist').append('<a id="displyButton" href="#" onclick="toggle_fn(this)"><i class="fa fa-arrow-down nav_icon" style="opacity: 0"></i>Display All</a>');
}

/**
 * button display setting
 */
function buttonDisplaySetting(){

    var lis= $('#taglist li');
    var alink =document.getElementById("displyButton");
    if(lis.length<4){
        alink.style.display = "none";
    }
    for(var i=0;i<lis.length;i++){
        if(i>3){
            lis[i].className="hide";
        }
    }

}

/**
 * button click to see more tag, if number of tags > 5
 * @param btn
 */
function toggle_fn(btn){
    var lis=$('#taglist li');
    for(var i=4;i<lis.length;i++){
        lis[i].className=flag===-1?"hide":"show";
    }
    if(flag===-1){
        btn.innerHTML="<i class=\"fa fa-arrow-down nav_icon\" style=\"opacity: 0\"></i>Display All";
    }else{
        btn.innerHTML="<i class=\"fa fa-arrow-down nav_icon\" style=\"opacity: 0\"></i>Collapse";
//btn.style.display="none";
    }
    flag=-flag;
}

/**
 * click bar to load project list according to tag value
 */
function clickBarEvent() {
    console.log("second bar click event");
    console.log($('#taglist li'));
    $('#taglist li').on('click', function(){
        console.log("start to load project show in second bar");
        $('#page-wrapper').html("");
        $.ajax({
            url : '/webpage/project-related/project-list-tag.html', // 这里是静态页的地址
            async : false,
            type : 'GET', // 静态页用get方法，否则服务器会抛出405错误
            success : function(data) {
                $('#page-wrapper').append(data);
            }
        });
    });
}