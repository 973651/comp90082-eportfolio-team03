/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 *
 */

var tagList = [];
var strcookie = document.cookie;
var userIdStr = strcookie.split(";")[0];
var userId = userIdStr.split("=")[1];

$(function() {
    // 填充

    console.log("start load project info")
    getProjectInfo();
    getOneProjectTag();
    getDBTags();

});

$.extend({
    'getUrlParam' : function(){
        var query = window.location.href;
        console.log(query);
        var projectid = query.split('=');
        console.log(projectid);
        return projectid[1];
    }
});

function getProjectInfo(){

    console.log("load project list");
    var projId = $.getUrlParam();
    //alert("project: userid="+userId);
    $.ajax({
        type: 'post',
        url: '/DisplayServlet',
        data: {
            "type": "queryOne",
            "userId": userId,
            "projectId" : projId
        },
        dataType: 'json',
        success: function(data){
            console.log(data);
            loadProjectToEditor(data[0]);
        },
        error : function(msg) {
            alert("get projects failed");
            console.log(msg)
        }
    });
}

function getOneProjectTag(){
    //var userId=$("#userId").val();
    var strcookie = document.cookie;
    var userIdStr = strcookie.split(";")[0];
    var userId = userIdStr.split("=")[1];
    var projId = $.getUrlParam();
    var item = 0;
    $.ajax({
        type: 'post',
        url: '/query.tag',
        data: {
            "type": "queryOneProjectTag",
            "userid": userId,
            "projectId": projId
        },
        dataType: 'json',
        success: function(data){
            for(x in data){
                console.log(data[x].projecttag)
                if (data[x].projecttag != null) {
                    tagList.push(data[x].projecttag);
                }
            }
            loadTagsToEditor(tagList);
        },
        error : function(msg) {
            alert("get tags failed");
            console.log(msg)
        }
    });
}

function getDBTags(){
    //var userId=$("#userId").val();
    var item = 0;
    $.ajax({
        type: 'post',
        url: '/query.tag',
        data: {
            "type": "queryAllTag",
            "userid": userId
        },
        dataType: 'json',
        success: function(data){
            for(x in data){
                loadTagToSelect(data[x]);
            }
        },
        error : function(msg) {
            alert("get tags failed");
            console.log(msg)
        }
    });
}

function loadProjectToEditor(data){
    console.log("fill info into the blank")
    document.getElementById("editTitle").value = data.projectname;
    KindEditor.html('#editContent', data.content);
    $('#editDescription').append(data.projectdescription);
    // console.log(data.str_starttime);
    // console.log(data.str_endtime);
    $('#edit_pro_start').val(data.str_starttime);
    $('#edit_pro_end').val(data.str_endtime);

    if (data.visibility == 2){
        $('#editPrivacy').append('<option value="2" selected="selected">Public Project</option>\n' +
            '                            <option value="0">Private Project</option>\n' +
            '                            <option value="1">Need Grant Permission</option>');
    } else if (data.visibility == 1){
        $('#editPrivacy').append('<option value="2">Public Project</option>\n' +
            '                            <option value="0">Private Project</option>\n' +
            '                            <option value="1" selected="selected">Need Grant Permission</option>');
    }else{
        $('#editPrivacy').append('<option value="2">Public Project</option>\n' +
            '                            <option value="0" selected="selected">Private Project</option>\n' +
            '                            <option value="1">Need Grant Permission</option>');
    }

}

function loadTagsToEditor(taglist){
    console.log(taglist);
    var tags = new InputTagGeneration({
        elem : '#editTags',
        content : taglist,
        inputT : 'text',
        theme : '#1e9fff',
        placeholder : 'Please Enter Your Project Tags',
        beforEnter : function (){

        },
        afterEnter : function (tags){
            console.log(tags)
        }
    });
    tags.clearAll();
    tags.reload(taglist);
    console.log(tags.getValue(), 'acquire all tags');
    tag.on('delTag', function (res) {
        console.log('Get new value after delete the tags', res)
    })
}

function loadTagToSelect(tag){
    if (tag.projecttag != null){
        $('#editorProjecttags').append('<option value="'+ tag.projecttag +'">'+tag.projecttag+'</option>');
    }
}