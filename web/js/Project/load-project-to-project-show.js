/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 *
 */

// gloabl variable
var imageStr;
var videoStr;
var documentStr;
var imagelist;
var videolist;
var documents;
var projectId = null;
var strcookie = document.cookie;
var userIdStr = strcookie.split(";")[0];
var userId = userIdStr.split("=")[1];

$(function() {
    // 填充
    getInfo();

});

$.extend({
    'getUrlParam' : function(){
        var query = window.location.href;
        var vars = query.split('=');
        return vars[1];
    }
});

/**
 * get a specific project content(all content include files)
 */
function getInfo(){

    projectId = $.getUrlParam();
    //alert("project: userid="+userId);
    $.ajax({
        type: 'post',
        url: '/DisplayServlet',
        data: {
            "type": "queryOne",
            "userId": userId,
            "projectId":projectId
        },
        dataType: 'json',
        success: function(data){
            console.log(data);
            loadProjectDetail(data[0]);
            displyPhoto();
            clickEvent();
            mouseMoveEvent();
        },
        error : function(msg) {
            alert("get projects failed");
            console.log(msg)
        }
    });
}

/**
 * Load project content into HTML with details
 * @param project
 */
function loadProjectDetail(project){
    clearHTML();

    document.getElementById("projectName").innerText = project.projectname;
    document.getElementById("projectTime").innerText = project.str_starttime + '  --  ' + project.str_endtime;
    document.getElementById("projectDes").innerText = project.projectdescription;
    document.getElementById("projectContent").innerHTML = project.content;


    imageStr = project.projectimg;
    videoStr = project.projectvideo;
    documentStr = project.document;

    imagelist = project.projectimg.split(';;;');
    videolist = project.projectvideo.split(';;;');
    documents = project.document.split(';;;');
    console.log(documents);
    var row = 1;

    if (imagelist[0] == ""){
        console.log("size  = 0");
        $('#image').html("");
        $('#image').append(' <div class="empty-file">\n' +
            '<div class="tips">\n' +
            '<p>You don\'t have any files right now.</p>\n' +
            '<p>Feel free to add new files.</p>\n' +
            '</div>\n' +
            '\n' +
            '<div class="add-file">\n' +
            '<a href="/webpage/project-related/editor.html?keywords='+projectId+'">\n' +
            '<i class="fa fa-plus fa-3x"></i>\n' +
            '</a>\n' +
            '</div>\n' +
            ' </div>');
    } else{
        for (var i = 0; i < imagelist.length-1; i++){
            console.log(imagelist[i]);
            if (i < 3){
                $('#row1').append('<div class="col-md-4 portfolio-item">\n' +
                    '<a class="portfolio-link">\n' +
                    '   <div class="caption-port">\n' +
                    '      <div class="caption-port-content">\n' +
                    '         <i id="'+ i +'" class="fa fa-trash fa-2x"></i>\n' +
                    '      </div>\n' +
                    '   </div>\n' +
                    '</a>\n' +
                    '<img src="'+ imagelist[i] +'" class="img-responsive thumbnail m-r-15" alt="">\n' +
                    '</div>');
            }
            else if (i < 6) {
                $('#row2').append('<div class="col-md-4 portfolio-item">\n' +
                    '<a class="portfolio-link">\n' +
                    '   <div class="caption-port">\n' +
                    '      <div class="caption-port-content">\n' +
                    '         <i id="'+ i +'" class="fa fa-trash fa-2x"></i>\n' +
                    '      </div>\n' +
                    '   </div>\n' +
                    '</a>\n' +
                    '<img src="'+ imagelist[i] +'" class="img-responsive thumbnail m-r-15" alt="">\n' +
                    '</div>');
            }
            else if (i < 9) {
                $('#row3').append('<div class="col-md-4 portfolio-item">\n' +
                    '<a class="portfolio-link">\n' +
                    '   <div class="caption-port">\n' +
                    '      <div class="caption-port-content">\n' +
                    '         <i id="'+ i +'" class="fa fa-trash fa-2x"></i>\n' +
                    '      </div>\n' +
                    '   </div>\n' +
                    '</a>\n' +
                    '<img src="'+ imagelist[i] +'" class="img-responsive thumbnail m-r-15" alt="">\n' +
                    '</div>');
            }
            else if (i < 12) {
                $('#row4').append('<div class="col-md-4 portfolio-item">\n' +
                    '<a class="portfolio-link">\n' +
                    '   <div class="caption-port">\n' +
                    '      <div class="caption-port-content">\n' +
                    '         <i id="'+ i +'" class="fa fa-trash fa-2x"></i>\n' +
                    '      </div>\n' +
                    '   </div>\n' +
                    '</a>\n' +
                    '<img src="'+ imagelist[i] +'" class="img-responsive thumbnail m-r-15" alt="">\n' +
                    '</div>');
            }
            else if (i < 15) {
                $('#row5').append('<div class="col-md-4 portfolio-item">\n' +
                    '<a class="portfolio-link">\n' +
                    '   <div class="caption-port">\n' +
                    '      <div class="caption-port-content">\n' +
                    '         <i id="'+ i +'" class="fa fa-trash fa-2x"></i>\n' +
                    '      </div>\n' +
                    '   </div>\n' +
                    '</a>\n' +
                    '<img src="'+ imagelist[i] +'" class="img-responsive thumbnail m-r-15" alt="">\n' +
                    '</div>');
            }
            else if (i < 18) {
                $('#row6').append('<div class="col-md-4 portfolio-item">\n' +
                    '<a class="portfolio-link">\n' +
                    '   <div class="caption-port">\n' +
                    '      <div class="caption-port-content">\n' +
                    '         <i id="'+ i +'" class="fa fa-trash fa-2x"></i>\n' +
                    '      </div>\n' +
                    '   </div>\n' +
                    '</a>\n' +
                    '<img src="'+ imagelist[i] +'" class="img-responsive thumbnail m-r-15" alt="">\n' +
                    '</div>');
            }
            else if (i < 21) {
                $('#row7').append('<div class="col-md-4 portfolio-item">\n' +
                    '<a class="portfolio-link">\n' +
                    '   <div class="caption-port">\n' +
                    '      <div class="caption-port-content">\n' +
                    '         <i id="'+ i +'" class="fa fa-trash fa-2x"></i>\n' +
                    '      </div>\n' +
                    '   </div>\n' +
                    '</a>\n' +
                    '<img src="'+ imagelist[i] +'" class="img-responsive thumbnail m-r-15" alt="">\n' +
                    '</div>');
            }
            else if (i < 24) {
                $('#row8').append('<div class="col-md-4 portfolio-item">\n' +
                    '<a class="portfolio-link">\n' +
                    '   <div class="caption-port">\n' +
                    '      <div class="caption-port-content">\n' +
                    '         <i id="'+ i +'" class="fa fa-trash fa-2x"></i>\n' +
                    '      </div>\n' +
                    '   </div>\n' +
                    '</a>\n' +
                    '<img src="'+ imagelist[i] +'" class="img-responsive thumbnail m-r-15" alt="">\n' +
                    '</div>');
            }
            else if (i < 27) {
                $('#row9').append('<div class="col-md-4 portfolio-item">\n' +
                    '<a class="portfolio-link">\n' +
                    '   <div class="caption-port">\n' +
                    '      <div class="caption-port-content">\n' +
                    '         <i id="'+ i +'" class="fa fa-trash fa-2x"></i>\n' +
                    '      </div>\n' +
                    '   </div>\n' +
                    '</a>\n' +
                    '<img src="'+ imagelist[i] +'" class="img-responsive thumbnail m-r-15" alt="">\n' +
                    '</div>');
            }
            else if (i < 30) {
                $('#row10').append('<div class="col-md-4 portfolio-item">\n' +
                    '<a class="portfolio-link">\n' +
                    '   <div class="caption-port">\n' +
                    '      <div class="caption-port-content">\n' +
                    '         <i id="'+ i +'" class="fa fa-trash fa-2x"></i>\n' +
                    '      </div>\n' +
                    '   </div>\n' +
                    '</a>\n' +
                    '<img src="'+ imagelist[i] +'" class="img-responsive thumbnail m-r-15" alt="">\n' +
                    '</div>');
            }
        }
    }


    $('#videoplayer').attr('src',videolist[0]);

    if (videolist[0] == ""){
        $('#vList').append(' <div class="empty-file">\n' +
            '<div class="tips">\n' +
            '<p>You don\'t have any files right now.</p>\n' +
            '<p>Feel free to add new files.</p>\n' +
            '</div>\n' +
            '\n' +
            '<div class="add-file">\n' +
            '<a href="/webpage/project-related/editor.html?keywords='+projectId+'">\n' +
            '<i class="fa fa-plus fa-3x"></i>\n' +
            '</a>\n' +
            '</div>\n' +
            ' </div>');
    }else{
        for (var v = 0; v < videolist.length-1; v++){
            if (videolist[v] != null){
                var videoindex = videolist[v].lastIndexOf("/");
                var vedioname = videolist[v].substr(videoindex+1);
                console.log(vedioname);
                $('#vList').append('<div class="file-na">\n' +
                    '<span id="v'+ v +'" href="#videoplayer">'+vedioname+'</span>\n' +
                    '<a href="'+ videolist[v] +'">DOWNLOAD</a>'+
                    '<input id="video'+v+'" type="button" value="DELETE"/>\n' +
                    '</div>')
            }
        }
    }

    if (documents[0] == ""){
        console.log("size  = 0");
        $('#fList').append(' <div class="empty-file">\n' +
            '<div class="tips">\n' +
            '<p>You don\'t have any files right now.</p>\n' +
            '<p>Feel free to add new files.</p>\n' +
            '</div>\n' +
            '\n' +
            '<div class="add-file">\n' +
            '<a href="/webpage/project-related/editor.html?keywords='+projectId+'">\n' +
            '<i class="fa fa-plus fa-3x"></i>\n' +
            '</a>\n' +
            '</div>\n' +
            ' </div>');
    } else{
        for (var d = 0; d < documents.length - 1; d++){
            var index = documents[d].lastIndexOf("/");
            var name = documents[d].substr(index+1);
            console.log(documents[d]);
            console.log(name);
            if (documents[d] != null){
                $('#fList').append('<div class="file-na">\n' +
                    '<span>'+ name +'</span>\n' +
                    '<a href="'+ documents[d] +'">DOWNLOAD</a>\n' +
                    '<input id="document'+ d +'" type="button" value="DELETE"/>\n' +
                    '</div>');
            }

        }
    }



}

/**
 * photo click event
 */
function displyPhoto(){
    let container = document.documentElement||document.body;
    let img,div,src,btnleft,btnright;
    var imgid=0;
    let x,y,w,h,tx,ty,tw,th,ww,wh;
    let closeMove=function(){
        if(div==undefined){
            return false;
        }
        div.style.opacity=0;
        img.style.height=h+"px";
        img.style.width=w+"px";
        img.style.left=x+"px";
        img.style.top=(y - container.scrollTop)+"px";
        // 延迟移除dom
        setTimeout(function(){
            div.remove();
            img.remove();
            btnright.remove();
            btnleft.remove();
        },100);

    };

    let closeFade=function(){
        if(div==undefined){
            return false;
        }
        div.style.opacity=0;
        img.style.opacity=0;
        // 延迟移除dom
        setTimeout(function(){
            div.remove();
            img.remove();
            btnright.remove();
            btnleft.remove();
        },100);
    };


// 监听滚动关闭层
    document.addEventListener("scroll",function(){
        closeFade();
    });
    document.querySelectorAll("img").forEach(v=>{

        if (v.parentNode.localName!='a') {
            v.id=imgid;
            imgid++;
            v.addEventListener("click",function(e){ // 注册事件
                // 记录小图的位置个大小
                x=e.target.offsetLeft;
                y=e.target.offsetTop;
                w=e.target.offsetWidth;
                h=e.target.offsetHeight;
                src=e.target.src;
                id=e.target.id;
                // 创建遮罩层
                div=document.createElement("div");
                div.style.cssText=`
	            position:fixed;
	            left:0;
	            top:0;
	            bottom:0;
	            right:0;
	            background-color: rgba(25,25,25,0.8);
	            z-index:99999999;
	            transition:all .3s cubic-bezier(0.165, 0.84, 0.44, 1);
	        `;
                document.body.appendChild(div);
                setTimeout(function(){
                    div.style.opacity=1;
                },0);
                // (此处可以加loading)

                // 创建副本
                img=new Image();
                btnright=document.createElement("button");
                btnleft=document.createElement("button");
                img.src=src;
                btnleft.style.cssText=`
			    position:fixed;
			    border-radius: 50%;;
			    left:${x - 20}px;
			    top:${y - container.scrollTop + h/2}px;
			    width:50px;
			    height:50px;
			    border: 0px;
			    background-color: rgba(200,200,200,0.8);
			    font-size: 20px;
			    z-index: 999999999;
			    transition:all .3s cubic-bezier(0.165, 0.84, 0.44, 1);
			`;
                btnright.style.cssText=`
			    position:fixed;
			    border-radius: 50%;
			    left:${x + w + 20}px;
			    top:${y - container.scrollTop + h/2}px;
			    width:50px;
			    border: 0px;
			    height:50px;
			    font-size: 20px;
			    background-color: rgba(200,200,200,0.8);
			    z-index: 999999999;
			    transition:all .3s cubic-bezier(0.165, 0.84, 0.44, 1);
			`;
                btnleft.innerText="<";
                btnright.innerText=">";

                img.style.cssText=`
			    position:fixed;
			    border-radius: 12px;
			    left:${x}px;
			    top:${y - container.scrollTop}px;
			    width:${w}px;
			    height:${h}px;
			    z-index: 999999999;
			    transition:all .3s cubic-bezier(0.165, 0.84, 0.44, 1);
			    opacity:0;
			`;

                btnleft.onclick=function(){
                    if(id<=0){
                        alert("Already the First Image！");
                        return;
                    }
                    id--;
                    var left=document.getElementById(id);
                    img.src=left.src;
                    x=left.offsetLeft;
                    y=left.offsetTop;
                    w=left.offsetWidth;
                    h=left.offsetHeight;
                }
                btnright.onclick=function(){

                    if(id>=imgid-1){
                        alert("Already the Last Image！");
                        return;
                    }
                    id++;
                    var right=document.getElementById(id);
                    img.src=right.src;
                    x=right.offsetLeft;
                    y=right.offsetTop;
                    w=right.offsetWidth;
                    h=right.offsetHeight;
                }

                img.onload=function(){
                    document.body.appendChild(img);
                    document.body.appendChild(btnright);
                    document.body.appendChild(btnleft);

                    // 浏览器宽高
                    wh=window.innerHeight;
                    ww=window.innerWidth;

                    // 目标宽高和坐标
                    if(w/h<ww/wh){
                        th=wh-80;
                        tw=w/h*th >> 0;
                        tx=(ww - tw) / 2;
                        ty=40;
                    }
                    else{
                        tw=ww*0.8;
                        th=h/w*tw >> 0;
                        tx=ww*0.1;
                        ty=(wh-th)/2;
                    }

                    // 延迟写入否则不会有动画
                    setTimeout(function(){
                        img.style.opacity=1;
                        img.style.height=th+"px";
                        img.style.width=tw+"px";
                        img.style.left=tx+"px";
                        img.style.top=ty+"px";
                        btnleft.style.left=(tx-90)+"px";
                        btnleft.style.top=(ty+th/2)+"px";
                        btnright.style.left=(tx+tw+40)+"px";
                        btnright.style.top=(ty+th/2)+"px";
                        // 点击隐藏
                        div.onclick=img.onclick=closeMove;
                    },10);
                };
            });//end event
        }
    });//end forEach
}

/**
 * back to project list click event
 */
function clickEvent(){
    console.log("listen click event");
    $('#backLink2').on('click', function(){
        console.log("start to load project list back !!!!");
        $('#page-wrapper').html("");
        $.ajax({
            url : '/webpage/project-related/project-list.html', // 这里是静态页的地址
            async : false,
            type : 'GET', // 静态页用get方法，否则服务器会抛出405错误
            success : function(data) {

                $('#page-wrapper').append(data);
            }
        });
    });

    jQuery(document).ready(function (){
        document.querySelectorAll("a div div i").forEach(v=>{
            v.addEventListener("click", function(){

                $.confirm({
                    title: 'Image Delete Confirm',
                    content: 'Are You Sure You Want to Delete this Image?\n' +
                    'Image Cannot be Recovered',
                    icon: 'fa fa-warning',
                    theme: 'supervan',
                    buttons: {
                        ok: {
                            text: "ok",
                            btnClass: 'btn-primary',
                            keys: ['enter'],
                            action: function(){
                                console.log(v);
                                var imageIdex = v.id;
                                console.log(imageIdex);
                                deleteOneImage(imageIdex, imageStr);
                            }
                        },
                        cancel: function(){
                            console.log('the user clicked cancel');
                        }
                    }
                });

            });
        });

    });

    jQuery(document).ready(function (){
        console.log(document.querySelectorAll("input"));

        document.querySelectorAll("input").forEach(v=>{
            console.log("kaixin");
            v.addEventListener("click", function(){

                var index = v.id;
                $.confirm({
                    title: 'File Delete Confirm',
                    content: 'Are You Sure You Want to Delete this File?\n' +
                    'File Cannot be Recovered',
                    icon: 'fa fa-warning',
                    theme: 'supervan',
                    buttons: {
                        ok: {
                            text: "ok",
                            btnClass: 'btn-primary',
                            keys: ['enter'],
                            action: function() {
                                var fileType = index.substr(0, index.length - 1);
                                var fileIndex = index.substr(index.length - 1);
                                console.log(fileType);
                                console.log(fileIndex);
                                deleteAFile(fileType, fileIndex);
                            }
                        },
                        cancel: function(){
                            console.log('the user clicked cancel');
                        }
                    }
                });

            });
        });
    });

    jQuery(document).ready(function() {
        $('#vList span').on("click",function(){
            var vid = $(this);
            var id = vid[0].id;
            var index = id.substr(1);
            $('#videoplayer').attr('src',videolist[index]);
        });
    });
}

/**
 * mouse move to photo will ddisplay delete logo
 */
function mouseMoveEvent(){
    $(document).ready(init);
    function init() {
        $(this).find(".caption-port-content").hide();
        $(".portfolio-item").mouseleave(function () {
            $(this).find(".caption-port-content").hide();
        });
        $(".portfolio-item").mouseenter(function () {
            $(this).find(".caption-port-content").show();
        });
    }
}

/**
 * image delete with confirmation
 * @param index
 * @param list
 */
function deleteOneImage(index, list){

    $.ajax({
        type: 'post',
        url: '/DeleteServlet',
        data: {
            "type": "deleteImage",
            "userId": userId,
            "projectId": projectId,
            "index": index,
            "images": list
        },
        success: function(data){
            alert("Successfully Delete");
            getInfo();
        },
        error : function(msg) {
            alert("Delete Failed");
            console.log(msg)
        }
    });


}

/**
 * file delete with confirmation
 * @param fType
 * @param fIndex
 */
function deleteAFile(fType, fIndex){
    $.ajax({
        type: 'post',
        url: '/DeleteServlet',
        data: {
            "type": "deleteFile",
            "userId": userId,
            "projectId": projectId,
            "index": fIndex,
            "fType": fType,
            "videos": videoStr,
            "docs": documentStr
        },
        success: function(data){
            alert("Successfully Delete");
            getInfo();
        },
        error : function(msg) {
            alert("Delete Failed");
            console.log(msg)
        }
    });

}

/**
 * reload html with clear <div>
 */
function clearHTML(){
    // $('#projectTime').html("");
    // $('#projectContent').html("");
    // $('#projectDes').html("");
    $('#row1').html("");
    $('#row2').html("");
    $('#row3').html("");
    $('#row4').html("");
    $('#row5').html("");
    $('#row6').html("");
    $('#row7').html("");
    $('#row8').html("");
    $('#row9').html("");
    $('#row10').html("");
    $('#vList').html("");
    $('#fList').html("");
}