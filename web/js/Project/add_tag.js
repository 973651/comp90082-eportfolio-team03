/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 */

/**
 * Tag Generation Class, used to generate project Tag
 */
class InputTagGeneration{

    constructor(config = {}) {
        this.content = config.content;
        // this.theme = config.theme;
        this.elem = config.elem;
        this.inputType = config.inputType || 'text';
        this.placeholder = config.placeholder;
        this.beforeEnter = config.beforeEnter;
        this.afterEnter = config.afterEnter;
        this.proxy = {};
        console.log(this.theme);

        this.init();
        return this;
    };

    // 事件监听
    on(event, callback) {
        this.proxy[event] = callback;
    }

    // 公共方法获取tag标签
    getTagsHtml(content) {
        return "<div class='inputTags--tag'>" +
                    "<span>" + content + "</span><span class='icon'>×</span>" +
            "</div>";
    };

    // 初始化
    init() {
        var self = this,
            container = "<div name='container'>",
            tags_html = "";
        $(self.elem).addClass('inputTags--container').html();
        // 循环添加标签
        for (var i = 0; i < self.content.length; i++){
            tags_html += self.getTagsHtml(self.content[i]);

        }
        $(self.container).before(container);

        // 监听事件
        self.bindEvent();
        self.bindDelTag();
    }

    // 监听回车
    bindEvent() {
        var self = this;
        $(self.elem + " input").keypress(function(e) {
            var key = e.keyCode ? e.keyCode : e.which;

            if (key == '13') {
                var input = this;
                // 生成标签前
                if (self.beforeEnter && self.beforeEnter() == false) {
                    return false;
                }
                // 生成标签
                self.render([$(input).val().trim()]);
                // 生成标签回调
                self.afterEnter && self.afterEnter(self.content);
            }
        })
    }

    // 渲染标签
    render(tag_text_array) {
        var self = this;
        tag_text_array.forEach((v, k) => {
            if (v != '' && self.content.indexOf(v) == -1) {
                // 添加标签
                $(self.elem + " input").before(self.getTagsHtml(v));
                self.content.push(v);
            }
        });

        // 清空input内容
        $(self.elem + " input").val('');
    }

    // 绑定删除标签事件
    bindDelTag() {
        var self = this;
        $(self.elem).on('click', '.inputTags--tag', function(obj) {
            self.delTag(this);
        })
    }

    // 删除标签事件
    delTag(obj) {
        var self = this;
        var del_text = $(obj).find('span').text(),
            del_index = self.content.indexOf(del_text);

        // 删除索引
        self.content.splice(del_index, 1);
        // 删除标签
        $(obj).remove();

        // 订阅者模式
        self.proxy['delTag'] && self.proxy['delTag'](self.content);
    }

    // 清空输入框
    clearAll() {
        this.content = [];
        $(this.elem + " .inputTags--tag").remove();
    }

    // 重新加载内容
    reload(conetnt) {
        var tmp_content = conetnt ? new Set(conetnt) : [];
        // 先清空
        this.clearAll();
        // 渲染
        this.render([...tmp_content]);
    }

    // 获取全部数据
    getValue() {
        return this.content;
    }

}

