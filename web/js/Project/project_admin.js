// --这是管理页面上的项目

// TODO
function addProject(){
    var userId = $("#uid").val();
    var name = $("#projectName").val();
    var description = $("#description").val();
    var start_time = $("#startTime").val();
    var end_time = $("#endTime").val();

    $.ajax({
        type: 'post',
        url: 'AddServlet',
        data: {
            "userId": userId,
            "projectName": name,
            "description": description,
            "startTime": start_time,
            "endTime": end_time
        },
        dataType: 'text',
        success: function(data){
            if(data == "success"){
                alert("insert success");
            } else{
                alert("insert failed");
            }
        },
        error : function(msg) {
            alert("project add failed");
            console.log(msg)
        }
    });
}

function getOneProject(id){
    $.ajax({
        type: 'post',
        url: 'DisplayServlet',
        data: {
            "type": "queryOne",
            "projectId": id
        },
        dataType: 'json',
        success: function(data){
            console.log(data);

            loadDescription(data[0]);
        },
        error : function(msg) {
            alert("get project failed");
            console.log(msg)
        }
    });
}

function loadDescription(data){
    $("#projectName").html(data.projectname);
    $("#projectDes").html(data.projectdescription);
    $("#projectTime").html(data.str_starttime+' - '+data.str_endtime);
    $("#projectContent").html(data.content);

    // $("#description").append('<h3 style="text-decoration: underline">Project Brief Intro:</h3>\n' +
    //     '                     <p>'+data.projectdescription+'</p>\n' +
    //     '                     <h3 style="text-decoration: underline">Project Period:</h3>\n' +
    //     '                     <p>'+data.str_starttime+' - '+data.str_endtime+'</p>\n' +
    //     '                     <h3 style="text-decoration: underline">Content:</h3>\n' +
    //     '                     <p>'+data.content+'</p>');
}