/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 *
 */
// userid from cookie
var strcookie = document.cookie;
var userIdStr = strcookie.split(";")[0];
var userId = userIdStr.split("=")[1];
//text type data
var projecttitle = null;
var projectcontent = null;
var projectdescription = null;
var starttime = null;
var endtime = null;
var projecttag = null;
// Array to store urls of different kinds of files
var vediofile = null;
var imgfile = null;
var otherfile = null;
var ifFilePublic = false;

$(function() {
    getTags();

    // file upload detect
    var upload = document.getElementById('uploadFile');
    var t_files = null;
    upload.addEventListener('change', function() {
        t_files = this.files;
        var str = [];
        if (t_files != null){
            for (var i = 0, len = t_files.length; i < len; i++) {
                str[i] = t_files[i].name;
                // console.log(str)
            };
        }
        if (t_files != null){
            if (t_files.length == 1){
                var index  = upload.value.lastIndexOf("\\");
                var value = upload.value.substr(index+1);
                $('#filesname').html("");
                $('#filesname').append(value);
            } else{

                $('#filesname').html("");
                $('#filesname').append(t_files.length + " Files Chosen");
            }
        }
    }, false);


    // upload file into alibaba OSS storage
    $('#upload').on("click", function(){
        uploadFiles(t_files);
    });

    // upload data into the DB
    $("#submitform").on("click", function() {
        if (t_files != null && ifFilePublic == false){
            $.confirm({
                title: 'Warning!',
                content: 'You Should Upload Your Files First ! ! \n',
                icon: 'fa fa-spinner fa-spin',
                theme: 'white',
                buttons: {
                    ok: {
                        text: "ok",
                        btnClass: 'btn-primary',
                        keys: ['enter'],
                        action: function() {

                        }
                    }
                }
            });
        }else if (t_files != null && ifFilePublic == true){
            submitForm();
        }else if (t_files == null){
            submitForm();
        }

    });

    // cancel confirmation
    $('#cancelAdd').on("click", function(){
        $.confirm({
            title: 'Cancel Edit!',
            content: 'You Change On This Project Will Not Be Saved ! \n' +
            'Click OK Back To Your Admin Account!',
            icon: 'fa fa-warning',
            theme: 'supervan',
            buttons: {
                ok: {
                    text: "ok",
                    btnClass: 'btn-primary',
                    keys: ['enter'],
                    action: function() {
                        $(window).attr('location', '/admin.html');

                    }
                },
                cancel: function(){

                }
            }
        });
    });
});

/**
 * Acquire all tag from DB
 */
function getTags(){
    //var userId=$("#userId").val();
    var item = 0;
    $.ajax({
        type: 'post',
        url: '/query.tag',
        data: {
            "type": "queryAllTag",
            "userid": userId
        },
        dataType: 'json',
        success: function(data){
            for(x in data){
                loadTagToAdd(data[x]);
            }
            // loadButton();
        },
        error : function(msg) {
            alert("get tags failed");
            console.log(msg)
        }
    });
}

/**
 * load tag to editor
 * @param tag
 */
function loadTagToAdd(tag){
    if (tag.projecttag != null){
        $('#projecttags').append('<option value="'+ tag.projecttag +'">');
    }

}

/**
 * classify the file type, store into array
 */
function classifyFiles(url){

    var index = url.lastIndexOf(".");
    var ext = url.substr(index+1);
    // alert(url + ext)

    console.log(ext + "  " + ext.length);

    if (isAssetTypeAnImage(ext)){
        if (imgfile == null){
            imgfile = url + ";;;";
        }else{
            imgfile = imgfile + url + ";;;";
        }

    } else if (isAssetTypeAVedio(ext)){
        if (vediofile == null){
            vediofile = url + ";;;";
        }else{
            vediofile = vediofile + url + ";;;";
        }
    } else{
        if (otherfile == null){
            otherfile = url + ";;;";
        }else{
            otherfile = otherfile + url + ";;;";
        }
    }

    console.log(imgfile);
    console.log(vediofile);
    console.log(otherfile);
}


/**
 * upload files (any type) to OSS
 */
function uploadFiles(t_files){
    if (t_files != null ){
        for (var i = 0, len = t_files.length; i < len; i++) {
            var formData = new FormData();
            formData.append("file["+ i +"]", t_files[i]);
            $.ajax({
                type: "post",
                url: '/large.file.upload',
                data: formData,
                processData: false,
                contentType: false,
                success: function (url) {
                    console.log("success !!!!");
                    $.confirm({
                        title: 'Success !',
                        content: 'File Upload Success \n',
                        icon: 'fa fa-spinner fa-spin',
                        theme: 'white',
                        buttons: {
                            ok: {
                                text: "ok",
                                keys: ['enter'],
                                action: function() {

                                }
                            }
                        }
                    });
                    ifFilePublic = true;
                    classifyFiles(url);
                },
                error:function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Upload Failed");
                }

            });
        }
    }
}

/**
 * submit project form to the DB
 */
function submitForm() {

    projecttitle = $('#title').val();
    projectcontent = $('textarea[name="content"]').val();
    projectdescription = $('#description').val();
    var sel = document.getElementById("privacySelect");
    var privacyVal = sel.value;
    starttime = $('#pro_start').val();
    endtime = $('#pro_end').val();

    var aSpan=document.getElementById("tags").getElementsByTagName("span");
    for (var i = 0; i < aSpan.length; i++){
        if (i!=0 && i % 2 ==0){
            if (projecttag == null){
                projecttag = aSpan[i].innerText + ";;;";
            }else{
                projecttag = projecttag + aSpan[i].innerText + ";;;";
            }
        }
    }
    // console.log(projectcontent);

    $.ajax({
        type : 'POST',
        async : false,
        url : '/AddServlet',
        data : {
            type : 'upload_project',
            title : projecttitle,
            content : projectcontent,
            userid : userId,
            description : projectdescription,
            starttime : starttime,
            endtime : endtime,
            img : imgfile,
            vedio : vediofile,
            otherfile : otherfile,
            tag : projecttag,
            visibility : privacyVal
        },
        success : function(response) {
            if (response == "" || response == null) {

            } else {
                $.confirm({
                    title: 'Congratulation!',
                    content: 'You Are Successfully Add Your Project ! \n' +
                    'Click OK Back To Your Admin Account!',
                    icon: 'fa fa-warning',
                    theme: 'supervan',
                    buttons: {
                        ok: {
                            text: "ok",
                            btnClass: 'btn-primary',
                            keys: ['enter'],
                            action: function() {
                                $(window).attr('location', '/admin.html');

                            }
                        }
                    }
                });

            }
        },
        error : function(XMLHttpRequest, textStatus) {

        }
    });

}

/**
 * file type judgement
 * @param ext
 * @returns {boolean}
 */
function isAssetTypeAnImage(ext) {
    var types = ['png', 'jpg', 'jpeg', 'bmp', 'gif', 'webp', 'psd', 'svg', 'tiff'];
    for (var i = 0; i < types.length; i++){
        if (ext === types[i]){
            return true;
        }
    }
}

/**
 * file type judgement
 * @param ext
 * @returns {boolean}
 */
function isAssetTypeAVedio(ext) {
    var types = ['rm','rmvb','mpeg1','mov','mtv','dat','wmv','avi','3gp','amv','dmv','flv',
        'mp4','mpeg2','mpeg4'];
    for (var i = 0; i < types.length; i++){
        if (ext ===types[i]){
            return true;
        }
    }
}