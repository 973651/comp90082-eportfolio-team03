/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 *
 */

var strcookie = document.cookie;
var userIdStr = strcookie.split(";")[0];
var userId = userIdStr.split("=")[1];

$(function() {
    // 填充
    getInfo();

});

$.extend({
    'getUrlParam' : function(){
        var query = window.location.href;
        // console.log(query);
        var url = query.split('=');
        return url[1];
    }
});

/**
 * query all of 'my' project
 */
function getInfo(){

    //alert("project: userid="+userId);
    $.ajax({
        type: 'post',
        url: '/DisplayServlet',
        data: {
            "type": "queryAll",
            "userId": userId
        },
        dataType: 'json',
        success: function(data){
            // console.log(data);
            $('#projectAll').html("");
            $('#page-bar').html("");
            loadSubProjectPage(data, 4);
            // for(x in data){
            //     loadProjectList(data[x]);
            // }
        },
        error : function(msg) {
            alert("projects failed");
            console.log(msg)
        }
    });
}

/**
 * load my projects in to the admin project list
 * @param project content
 */
function loadProjectList(project){
    $('#projectAll').append('<li><span class="proj-title"><a href="#keywords='+project.projectid+'">'+ project.projectname +'</a></span>\n' +
        '<a href="/webpage/project-related/editor.html?keywords='+ project.projectid +'">' +
        '   <input class="edit" type="button" value="EDIT"/>' +
        '</a>\n' +
        '<input id="'+ project.projectid +'" class="delete-list" type="button" value="DELETE"/>\n' +
        '<h5>Project Period: '+project.str_starttime + ' - ' + project.str_endtime +'</h5>\n' +
        '<p>'+ project.projectdescription +'</p>\n' +
        '</li>');
}

loadSubProjectPage = function(data, type){
    console.log("new way to page bar!!!!!")
    var pageSize = 3;
    var pageNum = Math.ceil(data.length/pageSize);
    // alert(pageNum);
    var dataStr = JSON.stringify(data);

    if(pageNum == 0){
        //$("#page-bar").append('<h4>No data here</h4>');
    } else{
        $("#page-bar").append('<div id="currentPage" style="display: none"></div>');    //用来存放当前页面编号
        $("#page-bar").append("<li id='previousBtn' class='page-item disabled'>\n" +
            "                                            <a class='page-link' href='#' tabindex='-1' onclick='changeAllListPage("+"-1,"+dataStr+","+i+","+pageSize+","+pageNum+","+type+")'>Previous</a>\n" +
            "                                        </li>");

        for(var i=1; i<=pageNum; i++){
            // $("#page-bar").append("<li class='page-item'><a class='page-link' onclick='loadParticularPage("+dataStr+","+i+","+pageSize+","+pageNum+",\""+idStr+",\""+htmlStr+"\")' href='#'>"+i+"</a></li>");
            $("#page-bar").append("<li class='page-item' id='subpage"+i+"'><a class='page-link' onclick='loadProjectListPage("+dataStr+","+i+","+pageSize+","+pageNum+","+type+")' href='#'>"+i+"</a></li>");
            // $("#page-bar").append('<li class="page-item">' +
            //             //     '<a class="page-link" href="#" onclick="test(\''+data+'\')">'+i+'</a></li>');
        }

        $("#page-bar").append("<li id='nextBtn' class='page-item'>\n" +
            "                                            <a class='page-link' href='#' tabindex='1' onclick='changeAllListPage("+"1,"+dataStr+","+i+","+pageSize+","+pageNum+","+type+")'>Next</a>\n" +
            "                                        </li>");

        loadProjectListPage(data, 1, pageSize, pageNum, type);
    }

}

function changeAllListPage(move, dataStr, p, pageSize, pageNum, type){
    // alert("change!!!!!!!!!!!!!!");
    var curPage = $("#currentPage").html();
    var newPage = parseInt(curPage)+move;
    if(newPage>0 && newPage<=pageNum){
        loadProjectListPage(dataStr, newPage, pageSize, pageNum, type);
    }
}

function loadProjectListPage(dataStr, p, pageSize, pageNum, type) {
    var data = eval(dataStr);
    // //清空原先列表
    // $("#requestProjectList").html("");

    //记录当前页的标号
    $("#currentPage").html(p);

    //样式准备
    if(p > 1)   $("#previousBtn").attr("class", "page-item");
    else    $("#previousBtn").attr("class", "page-item disabled");

    if(p == pageNum)    $("#nextBtn").attr("class", "page-item disabled");
    else    $("#nextBtn").attr("class", "page-item");

    for(var i=1; i<=pageNum; i++){
        $("#subpage"+i).attr("class", "page-item");
    }
    $("#subpage"+p).attr("class", "page-item active");

    //加载数据显示列表
    var start = (parseInt(p)-1)*pageSize;

    if (type == 4){
        $('#projectAll').html("");
        for (var x=start; x<start+pageSize;x++){
            var project = data[x];
            if (project!=undefined){
                $('#projectAll').append('<li><span class="proj-title"><a href="#keywords='+project.projectid+'">'+ project.projectname +'</a></span>\n' +
                    '<a href="/webpage/project-related/editor.html?keywords='+ project.projectid +'">' +
                    '   <input class="edit" type="button" value="EDIT"/>' +
                    '</a>\n' +
                    '<input id="'+ project.projectid +'" class="delete-list" type="button" value="DELETE"/>\n' +
                    '<h5>Project Period: '+project.str_starttime + ' - ' + project.str_endtime +'</h5>\n' +
                    '<p>'+ project.projectdescription +'</p>\n' +
                    '</li>');
            }
        }
    }

    // console.log("load click event")

    clickevent();
}

/**
 * delete, edit and view project click event definetion
 */
function clickevent() {
    jQuery(document).ready(function() {
        // console.log( document.querySelectorAll("li span a"));
        document.querySelectorAll("li span a").forEach(v=>{
            v.addEventListener("click",function(e){
                // console.log("start to load project show");
                $('#page-wrapper').html("");
                $.ajax({
                    url : '/webpage/project-related/project-show.html', // 这里是静态页的地址
                    async : false,
                    type : 'GET', // 静态页用get方法，否则服务器会抛出405错误
                    success : function(data) {
                        $('#page-wrapper').append(data);
                    }
                });
            });
        });
    });

    jQuery(document).ready(function (){

        document.querySelectorAll("li input").forEach(v=>{
            v.addEventListener("click", function(){

                $.confirm({
                    title: 'Delete Confirm',
                    content: 'Are You Sure You Want to Delete this Project?\n' +
                    'Project Cannot be Recovered',
                    confirmButton: 'Yes',
                    cancelButton: 'No',
                    confirmButtonClass: 'btn-info',
                    cancelButtonClass: 'btn-danger',
                    icon: 'fa fa-warning',
                    theme: 'supervan',
                    buttons: {
                        ok: {
                            text: "ok",
                            btnClass: 'btn-primary',
                            keys: ['enter'],
                            action: function(){
                                // console.log("delete confirm");
                                var pid = v.id;
                                deleteOneProject(pid);
                            }
                        },
                        cancel: function(){
                            console.log('the user clicked cancel');
                        }
                    }
                });

            });
        });

    });
}

/**
 * delete a project with confirmation
 * @param id
 */
function deleteOneProject(id){
    // console.log("delete proceess  ");
    // console.log(id);
    $.ajax({
        type: 'post',
        url: '/DeleteServlet',
        data: {
            "type": "deleteProject",
            "userId": userId,
            "projectId": id
        },
        success: function(data){
            alert("Successfully Delete");
            getInfo();
        },
        error : function(msg) {
            alert("Delete Failed");
            console.log(msg)
        }
    });
}