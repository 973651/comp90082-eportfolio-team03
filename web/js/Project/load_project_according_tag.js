/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 *
 */

$(function() {
    // 填充
    getInfo();

});

$.extend({
    'getUrlParam' : function(){
        var query = window.location.href;
        // console.log(query);
        var tag = query.split('keywords=');
        return tag[1];
    }
});

/**
 * Get Project By Selected Tag
 */
function getInfo(){

    var strcookie = document.cookie;
    var userIdStr = strcookie.split(";")[0];
    var userId = userIdStr.split("=")[1];
    var tag = $.getUrlParam();
    //alert("project: userid="+userId);
    $.ajax({
        type: 'post',
        url: '/DisplayServlet',
        data: {
            "type": "queryProjectByTag",
            "userId": userId,
            "tag": tag
        },
        dataType: 'json',
        success: function(data){
            loadPageTitle(tag);
            // for(x in data){
            //     loadProjectList(data[x]);
            // }
            loadSubProjectByTagPage(data, 3);
        },
        error : function(msg) {
            alert("projects failed");
            console.log(msg)
        }
    });
}

/**
 * Load page title according to tag
 * @param tag
 */
function loadPageTitle(tag){
    var span = document.getElementById("pageTitle");
    span.innerText = "Project for \"" + tag + " Project\"";
}

/**
 * Load project content into html
 * @param project
 */
function loadProjectList(project){
    $('#projectByTag').append('<li><span class="proj-title"><a href="#keywords='+project.projectid+'">'+ project.projectname +'</a></span>\n' +
        '<a href="/webpage/project-related/editor.html?keywords='+ project.projectid +'">' +
        '   <input class="edit" type="button" value="EDIT"/>' +
        '</a>\n' +
        '<input id="'+ project.projectid +'" class="delete-list" type="button" value="DELETE"/>\n' +
        '<h5>Project Period: '+project.str_starttime + ' - ' + project.str_endtime +'</h5>\n' +
        '<p>'+ project.projectdescription +'</p>\n' +
        '</li>');
}


loadSubProjectByTagPage = function(data, type){
    // console.log("new Way load project By TAG PAge Bar")
    var pageSize = 3;
    var pageNum = Math.ceil(data.length/pageSize);
    // alert(pageNum);
    var dataStr = JSON.stringify(data);

    if(pageNum == 0){
        //$("#page-bar").append('<h4>No data here</h4>');
    } else{
        $("#page-bar").append('<div id="currentPage" style="display: none"></div>');    //用来存放当前页面编号
        $("#page-bar").append("<li id='previousBtn' class='page-item disabled'>\n" +
            "                                            <a class='page-link' href='#' tabindex='-1' onclick='changeTagPage("+"-1,"+dataStr+","+i+","+pageSize+","+pageNum+","+type+")'>Previous</a>\n" +
            "                                        </li>");

        for(var i=1; i<=pageNum; i++){
            // $("#page-bar").append("<li class='page-item'><a class='page-link' onclick='loadParticularPage("+dataStr+","+i+","+pageSize+","+pageNum+",\""+idStr+",\""+htmlStr+"\")' href='#'>"+i+"</a></li>");
            $("#page-bar").append("<li class='page-item' id='subpage"+i+"'><a class='page-link' onclick='loadProjectByTagPage("+dataStr+","+i+","+pageSize+","+pageNum+","+type+")' href='#'>"+i+"</a></li>");
            // $("#page-bar").append('<li class="page-item">' +
            //             //     '<a class="page-link" href="#" onclick="test(\''+data+'\')">'+i+'</a></li>');
        }

        $("#page-bar").append("<li id='nextBtn' class='page-item'>\n" +
            "                                            <a class='page-link' href='#' tabindex='1' onclick='changeTagPage("+"1,"+dataStr+","+i+","+pageSize+","+pageNum+","+type+")'>Next</a>\n" +
            "                                        </li>");

        loadProjectByTagPage(data, 1, pageSize, pageNum, type);
    }

}

function changeTagPage(move, dataStr, p, pageSize, pageNum, type){
    // alert("change!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    // console.log("change!!!!!!!!!!")
    var curPage = $("#currentPage").html();
    var newPage = parseInt(curPage)+move;
    if(newPage>0 && newPage<=pageNum){
        // console.log("page change!!!!!")
        loadProjectByTagPage(dataStr, newPage, pageSize, pageNum, type);
    }
}

function loadProjectByTagPage(dataStr, p, pageSize, pageNum, type) {
    var data = eval(dataStr);
    // //清空原先列表
    // $("#requestProjectList").html("");

    //记录当前页的标号
    $("#currentPage").html(p);

    //样式准备
    if(p > 1)   $("#previousBtn").attr("class", "page-item");
    else    $("#previousBtn").attr("class", "page-item disabled");

    if(p == pageNum)    $("#nextBtn").attr("class", "page-item disabled");
    else    $("#nextBtn").attr("class", "page-item");

    for(var i=1; i<=pageNum; i++){
        $("#subpage"+i).attr("class", "page-item");
    }
    $("#subpage"+p).attr("class", "page-item active");

    //加载数据显示列表
    var start = (parseInt(p)-1)*pageSize;
    // console.log(start);
    // console.log(pageSize)

    if (type ==3){
        $('#projectByTag').html("");
        for(var x=start; x<start+pageSize; x++){
            var project = data[x];
            // console.log(project);
            if (project != undefined){
                $('#projectByTag').append('<li><span class="proj-title"><a href="#keywords='+project.projectid+'">'+ project.projectname +'</a></span>\n' +
                    '<a href="/webpage/project-related/editor.html?keywords='+ project.projectid +'">' +
                    '   <input class="edit" type="button" value="EDIT"/>' +
                    '</a>\n' +
                    '<input id="'+ project.projectid +'" class="delete-list" type="button" value="DELETE"/>\n' +
                    '<h5>Project Period: '+project.str_starttime + ' - ' + project.str_endtime +'</h5>\n' +
                    '<p>'+ project.projectdescription +'</p>\n' +
                    '</li>');
            }
        }
    }
    // console.log("detect onclick!!!!")
    clickevent();

}

/**
 * click to see more details about a specific project
 */
function clickevent() {
    jQuery(document).ready(function() {
        // console.log( document.querySelectorAll("li span a"));
        document.querySelectorAll("li span a").forEach(v=>{
            v.addEventListener("click",function(e){
                // console.log("start to load project show");
                $('#page-wrapper').html("");
                $.ajax({
                    url : '/webpage/project-related/project-show.html', // 这里是静态页的地址
                    async : false,
                    type : 'GET', // 静态页用get方法，否则服务器会抛出405错误
                    success : function(data) {

                        $('#page-wrapper').append(data);
                    }
                });
            });
        });
    });

    jQuery(document).ready(function (){

        document.querySelectorAll("li input").forEach(v=>{
            v.addEventListener("click", function(){

                $.confirm({
                    title: 'Delete Confirm',
                    content: 'Are You Sure You Want to Delete this Project?\n' +
                    'Project Cannot be Recovered',
                    confirmButton: 'Yes',
                    cancelButton: 'No',
                    confirmButtonClass: 'btn-info',
                    cancelButtonClass: 'btn-danger',
                    icon: 'fa fa-warning',
                    theme: 'supervan',
                    buttons: {
                        ok: {
                            text: "ok",
                            btnClass: 'btn-primary',
                            keys: ['enter'],
                            action: function(){
                                var pid = v.id;
                                deleteOneProject(pid);
                            }
                        },
                        cancel: function(){
                            console.log('the user clicked cancel');
                        }
                    }
                });

            });
        });

    });

}

/**
 * delete project with confirmation
 * @param id
 */
function deleteOneProject(id){
    // console.log("delete process")
    // console.log(id)
    var strcookie = document.cookie;
    var userIdStr = strcookie.split(";")[0];
    var userId = userIdStr.split("=")[1];
    $.ajax({
        type: 'post',
        url: '/DeleteServlet',
        data: {
            "type": "deleteProject",
            "userId": userId,
            "projectId": id
        },
        success: function(data){
            alert("Successfully Delete");
            $('#page-wrapper').html("");
            $.ajax({
                url : '/webpage/project-related/project-list.html', // 这里是静态页的地址
                async : false,
                type : 'GET', // 静态页用get方法，否则服务器会抛出405错误
                success : function(data) {
                    $('#page-wrapper').append(data);
                }
            });
        },
        error : function(msg) {
            alert("Delete Failed");
            console.log(msg)
        }
    });
}