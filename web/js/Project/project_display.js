// Display projects on personal page

//global variables: current user id
var strcookie = document.cookie;
var userId = 0;
if(strcookie.length > 0){
    var userIdStr = strcookie.split(";")[0];
    userId = userIdStr.split("=")[1];
}

//global variable: selected user id, may be 0
var url = window.location.href;
var urlArray = url.split("id=");
var otheruserId = 0;
if(urlArray.length > 1) otheruserId = urlArray[1].split("#")[0];

//load current user's project list
function getMyProjectList(){
    $("#filterTagBtn").html("");
    $("#myProjectList").html("");
    $.ajax({
        type: 'post',
        url: 'DisplayServlet',
        async: false,
        data: {
            "type": "queryAll",
            "userId": userId
        },
        dataType: 'json',
        success: function(data){
            console.log(data);
            if(data.length == 0){
                $("#myProjectList").append('<div class="me-empty">\n' +
                    '            <div class="tips">\n' +
                    '                <p>You haven\'t created any project item.</p>\n' +
                    '                <p>Feel free to add new project.</p>\n' +
                    '            </div>\n' +
                    '            <div class="add-file">\n' +
                    '                <a href="admin.html">\n' +
                    '                    <i class="fa fa-plus fa-3x"></i>\n' +
                    '                </a>\n' +
                    '            </div>\n' +
                    '        </div>');
            } else{
                loadMyTagFilterBtn();
                for(x in data){
                    loadMyProject(data[x]);
                    loadMyProjectTags(data[x].projectid, userId);
                }
            }
        },
        error : function(msg) {
            alert("get projects failed");
            console.log(msg)
        }
    });
}

//load current user's project filtered by tags
function loadMyProjectByTag(projectTag){
    $("#myProjectList").html("");
    $.ajax({
        type: 'post',
        url: 'DisplayServlet',
        async: false,
        data: {
            "type": "queryProjectByTag",
            "userId": userId,
            "tag": projectTag
        },
        dataType: 'json',
        success: function(data){
            console.log(data);
            for(x in data){
                loadMyProject(data[x]);
                loadMyProjectTags(data[x].projectid, userId);
            }
        },
        error : function(msg) {
            alert("get my projects by tag failed");
            console.log(msg)
        }
    });
}

//load own projects to list
function loadMyProject(data){
    $("#myProjectList").append('<tr>\n' +
        '                    <td class="proj-title">'+data.projectname+'</td>\n' +
        '                    <td class="proj-tag" id="project-tag'+data.projectid+'">\n' +
        '                    </td>\n' +
        '                    <td class="proj-de">\n' +
        '                        <a href="person-project.html?id='+data.projectid+'">\n' +
        '                            <button class="request">Details</button>\n' +
        '                        </a>\n' +
        '                    </td>\n' +
        '                </tr>');
}

//load (approved or public) projects to list
function loadApprovedProject(data){
    $("#myProjectList").append('<tr>\n' +
        '                    <td class="proj-title">'+data.projectname+'</td>\n' +
        '                    <td class="proj-tag" id="project-tag'+data.projectid+'">\n' +
        '                    </td>\n' +
        '                    <td class="proj-de">\n' +
        '                        <a href="other-person-project.html?id='+data.projectid+'">\n' +
        '                            <button class="request">Details</button>\n' +
        '                        </a>\n' +
        '                    </td>\n' +
        '                </tr>');
}

//load (need require) projects to list
function loadRequestedProject(data){
    $("#myProjectList").append('<tr>\n' +
        '                    <td class="proj-title">'+data.projectname+'</td>\n' +
        '                    <td class="proj-tag" id="project-tag'+data.projectid+'">\n' +
        '                    </td>\n' +
        '                    <td class="proj-de">\n' +
        '                        <a href="#">\n' +
        '                            <button type="button" data-toggle="modal" data-target="#send-req'+data.projectid+'">Details</button>\n' +
        '                        </a>\n' +
        '                    </td>\n' +
        '                </tr>');
}

//load project list when not login (for login alert after click details button)
function loadUnloginProject(data){
    $("#myProjectList").append('<tr>\n' +
        '                    <td class="proj-title">'+data.projectname+'</td>\n' +
        '                    <td class="proj-tag" id="project-tag'+data.projectid+'">\n' +
        '                    </td>\n' +
        '                    <td class="proj-de">\n' +
        '                        <a href="#">\n' +
        '                            <button class="request" onclick="confirmLogin()">Details</button>\n' +
        '                        </a>\n' +
        '                    </td>\n' +
        '                </tr>');
}

function confirmLogin(){
    $.confirm({
            title: 'You need login to see details',
            content: 'Are You Sure Going Back And Login?\n',
            icon: 'fa fa-warning',
            theme: 'white',
            buttons: {
                ok: {
                    text: "ok",
                    btnClass: 'btn-primary',
                    keys: ['enter'],
                    action: function() {
                        window.location.href="index.html";
                    }
                },
                cancel: function(){
                    console.log('the user clicked cancel');
                }
            }
        });
}

//get all tags of particular project
function loadMyProjectTags(projectId, user){
    $.ajax({
        type: 'post',
        url: '/query.tag',
        async: false,
        data: {
            "type": "queryOneProjectTag",
            "userid": user,
            "projectId": projectId
        },
        dataType: 'json',
        success: function(data){
            //console.log(data);
            for(x in data){
                //$("#tr-filter"+projectId).attr("class", "col-sm-4 portfolio-item filter "+data[x].projecttag);
                $("#project-tag"+projectId).append('<span class="label-tag">'+data[x].projecttag+'</span>');
            }
        },
        error : function(msg) {
            alert("get tags failed");
            console.log(msg)
        }
    });
}

//get all tag filter buttons of current user
function loadMyTagFilterBtn(){
    $("#filterTagBtn").append('<button class="btn btn-general btn-green filter-b" onclick="getMyProjectList()">All</button>');
    $.ajax({
        type: 'post',
        url: '/query.tag',
        async: false,
        data: {
            "type": "queryAllTag",
            "userid": userId
        },
        dataType: 'json',
        success: function(data){
            console.log(data);
            for(x in data){
                $("#filterTagBtn").append('<button class="btn btn-general btn-green filter-b" onclick="loadMyProjectByTag(\''+data[x].projecttag+'\')">'+data[x].projecttag+'</button>');
            }
        },
        error : function(msg) {
            alert("get my tag filter button failed");
            console.log(msg)
        }
    });
}

//get all other's projects with privilege 2(public) and 1(need grant)
//for those 1 projects, load details buttons according to whether they are permitted
function getOthersProjectList(){
    $("#filterTagBtn").html("");
    $("#myProjectList").html("");
    $.ajax({
        type: 'post',
        url: 'DisplayServlet',
        async: false,
        data: {
            "type": "queryAll",
            "userId": otheruserId
        },
        dataType: 'json',
        success: function(data){
            console.log(data);
            if(data.length == 0){
                $("#myProjectList").append('<div class="empty">\n' +
                    '            <span class="tip">Sorry, this user didn\'t create any project item.</span>\n' +
                    '        </div>');
            } else{
                loadOthersTagFilterBtn();
                if(strcookie==null || strcookie.length==0){
                    for(x in data){
                        var visibility = data[x].visibility;
                        if(visibility==2 || visibility==1){
                            loadUnloginProject(data[x]);
                            loadMyProjectTags(data[x].projectid, otheruserId);
                        }
                    }
                } else{
                    for(x in data){
                        var visibility = data[x].visibility;
                        if(visibility == 2){
                            loadApprovedProject(data[x]);
                            loadMyProjectTags(data[x].projectid, otheruserId);
                        } else if(visibility == 1){
                            if(containsCurrentUser(data[x].visibleto)){
                                loadApprovedProject(data[x]);
                            } else{
                                loadRequestedProject(data[x]);
                                loadRequestModal(data[x]);
                            }
                            loadMyProjectTags(data[x].projectid, otheruserId);
                        }
                    }
                }
            }
        },
        error : function(msg) {
            alert("get other projects failed");
            console.log(msg)
        }
    });
}

function containsCurrentUser(visibleTo){
    var users = visibleTo.split(",");
    for(var i=0; i<users.length; i++){
        if(parseInt(users[i]) == parseInt(userId)){
            return true;
        }
    }
    return false;
}

function loadOthersTagFilterBtn() {
    $("#filterTagBtn").append('<button class="btn btn-general btn-green filter-b" onclick="getOthersProjectList()">All</button>');
    $.ajax({
        type: 'post',
        url: '/query.tag',
        async: false,
        data: {
            "type": "getOnesAllVisibleTag",
            "userid": otheruserId
        },
        dataType: 'json',
        success: function(data){
            console.log(data);
            for(x in data){
                $("#filterTagBtn").append('<button class="btn btn-general btn-green filter-b" onclick="loadOtherProjectByTag(\''+data[x].projecttag+'\')">'+data[x].projecttag+'</button>');
            }
        },
        error : function(msg) {
            alert("get other tag filter button failed");
            console.log(msg)
        }
    });
}

//load project list using tag filters by other user
function loadOtherProjectByTag(projectTag){
    $("#myProjectList").html("");
    $.ajax({
        type: 'post',
        url: 'DisplayServlet',
        async: false,
        data: {
            "type": "queryProjectByTag",
            "userId": otheruserId,
            "tag": projectTag
        },
        dataType: 'json',
        success: function(data){
            console.log(data);
            for(x in data){
                var visibility = data[x].visibility;
                if(visibility == 2){
                    loadApprovedProject(data[x]);
                    loadMyProjectTags(data[x].projectid, otheruserId);
                } else if(visibility == 1){
                    if(containsCurrentUser(data[x].visibleto)){
                        loadApprovedProject(data[x]);
                    } else{
                        loadRequestedProject(data[x]);
                        loadRequestModal(data[x]);
                    }
                    loadMyProjectTags(data[x].projectid, otheruserId);
                }
            }
        },
        error : function(msg) {
            alert("get other projects by tag failed");
            console.log(msg)
        }
    });
}

function loadRequestModal(data){
    $("#requestModal").append('<div class="modal fade" id="send-req'+data.projectid+'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">\n' +
        '<div class="modal-dialog" role="document">\n' +
        '        <div class="modal-content">\n' +
        '            <div class="modal-header">\n' +
        '                <h4 class="modal-title" id="exampleModalLabel1"> Project Request</h4>\n' +
        '                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n' +
        '            </div>\n' +
        '            <div class="modal-body">\n' +
        '                <div class="line">\n' +
        '                    <h5>Project Name: </h5>\n' +
        '                    <span>'+data.projectname+'</span>\n' +
        '                </div>\n' +
        '                <div class="line">\n' +
        '                    <h5>Project Description: </h5>\n' +
        '                    <span>'+data.projectdescription+'</span>\n' +
        '                </div>\n' +
        '                <div class="remind">\n' +
        '                    <p>This is a private project.</p>\n' +
        '                    <p>If you want to see the whole content, please fill in the request below.</p>\n' +
        '                </div>\n' +
        '                <form>\n' +
        '                    <div class="form-group">\n' +
        '                        <label>Apply Reason:</label>\n' +
        '                        <textarea class="form-control" id="reasonInput'+data.projectid+'"></textarea>\n' +
        '                    </div>\n' +
        '                </form>\n' +
        '            </div>\n' +
        '            <div class="modal-footer">\n' +
        '                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>\n' +
        '                <button type="button" class="btn btn-primary" onclick="sendRequest('+data.projectid+','+userId+','+data.userid+')">Submit</button>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div></div>');
}