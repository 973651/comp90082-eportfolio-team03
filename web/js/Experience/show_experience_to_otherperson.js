/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 */

$.extend({
    'getUidParam' : function(){
        var query = window.location.href;
        var vars = query.split('=');
        return vars[1];
    }
});
$(function() {
    // 填充用户信息
    getOExperience();

});

function getOExperience(){
    var userId = $.getUidParam().split("#")[0];
    $.ajax({
        type: 'post',
        url: '/ExperienceServlet',
        data: {
            "userId": userId
        },
        dataType: 'json',
        success: function(data){
            if (data[0] == undefined){
                $('#oexperience_list').append('<div class="empty">\n' +
                    '                <span class="tip">Sorry, this user didn\'t fill in any work experience.</span>\n' +
                    '            </div>');
            } else{
                console.log(data);
                for(x in data){
                    loadExperience(data[x]);
                }
            }
        },
        error : function(msg) {
            alert("get experience failed");
            console.log(msg)
        }
    });
}

function loadExperience(experience_data) {
    console.log(experience_data);
    $('#oexperience_list').append('<div class="resume-item col-md-6 col-sm-12 " >\n' +
        '<div class="card mx-0 p-4 mb-5" style="border-color: #17a2b8; box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.21);">\n' +
        '<div class=" resume-content mr-auto">\n' +
        '<h4 class="mb-3"><i class="fa fa-globe mr-3 text-info"></i>' +experience_data.jobname + '   ' + experience_data.company +'</h4>\n' +
        '<p>' + experience_data.workdescription + '</p></div><div class="resume-date text-md-right">\n' +
        '<span class="text-primary">'+ experience_data.str_starttime + '-' + experience_data.str_endtime +'</span>\n' +
        '</div></div></div>');
}