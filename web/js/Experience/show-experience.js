var strcookie = document.cookie;
var userIdStr = strcookie.split(";")[0];
var userId = userIdStr.split("=")[1];

$(function() {
    // 填充用户信息
    getExperience();

});

function getExperience(){
    $.ajax({
        type: 'post',
        url: '/ExperienceServlet',
        data: {
            "userId": userId
        },
        dataType: 'json',
        success: function(data){
            console.log(data[0]);
            if (data[0]== undefined){
                $('#experience_list').append('<div class="me-empty">\n' +
                    '                <div class="tips">\n' +
                    '                    <p>You haven\'t filled in any work experience.</p>\n' +
                    '                    <p>Feel free to edit new information.</p>\n' +
                    '                </div>\n' +
                    '                <div class="add-file">\n' +
                    '                    <a href="admin.html">\n' +
                    '                        <i class="fa fa-plus fa-3x"></i>\n' +
                    '                    </a>\n' +
                    '                </div>\n' +
                    '            </div>');
            } else{
                for(x in data){
                    loadExperience(data[x]);
                }
            }
        },
        error : function(msg) {
            alert("get experience failed");
            console.log(msg)
        }
    });
}

function loadExperience(experience_data) {
    console.log(experience_data);
    $('#experience_list').append('<div class="resume-item col-md-6 col-sm-12 " >\n' +
        '<div class="card mx-0 p-4 mb-5" style="border-color: #17a2b8; box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.21);">\n' +
        '<div class=" resume-content mr-auto">\n' +
        '<h4 class="mb-3"><i class="fa fa-globe mr-3 text-info"></i>' +experience_data.jobname + '   ' + experience_data.company +'</h4>\n' +
        '<p>' + experience_data.workdescription + '</p></div><div class="resume-date text-md-right">\n' +
        '<span class="text-primary">'+ experience_data.str_starttime + '-' + experience_data.str_endtime +'</span>\n' +
        '</div></div></div>');
}