
$(function() {
    // 填充用户信息
    fillEducationToOPerson()

});

$.extend({
    'getEduIdParam' : function(){
        var query = window.location.href;
        var vars = query.split('=');
        return vars[1];
    }
});

function fillEducationToOPerson(){
    var userId = $.getEduIdParam().split("#")[0];
    $.ajax({
        type: 'post',
        url: '/edu.servlet',
        data: {
            "type":"query_all_edu",
            "userId": userId
        },
        dataType: 'json',
        success: function(data){
            if (data[0] == undefined){
                $('#oeduExp').append('<div class="empty">\n' +
                    '            <span class="tip">Sorry, this user didn\'t fill in education information.</span>\n' +
                    '        </div>');
            } else{
                for(x in data){
                    loadEduO(data[x]);
                }
            }

        },
        error : function(msg) {
            alert("failed");
            console.log(msg)
        }
    });
}

function loadEduO(edu){
    $('#oeduExp').append('<div class="title_con"></div>\n' +
        '                <ul class="edu">\n' +
        '                    <li>\n' +
        '                        <label>University:</label>\n' +
        '                        <span class="value">'+ edu.school +'</span>\n' +
        '                    </li>\n' +
        '                    <li>\n' +
        '                        <label>Degree:</label>\n' +
        '                        <span class="value">'+ edu.degree +'</span>\n' +
        '                    </li>\n' +
        '                    <li>\n' +
        '                        <label>Major:</label>\n' +
        '                        <span class="value">'+ edu.major +'</span>\n' +
        '                    </li>\n' +
        '                    <li>\n' +
        '                        <label>Study Period:</label>\n' +
        '                        <span class="value">'+edu.str_starttime +'  -  '+ edu.str_endtime+'</span>\n' +
        '                    </li>\n' +
        '                    <li>\n' +
        '                        <label>Main Course:</label>\n' +
        '                        <span class="value">'+ edu.maincourse +'</span>\n' +
        '                    </li>\n' +
        '                </ul>');
}