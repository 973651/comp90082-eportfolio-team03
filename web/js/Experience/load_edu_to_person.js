/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 */

var strcookie = document.cookie;
var userIdStr = strcookie.split(";")[0];
var userId = userIdStr.split("=")[1];

$(function() {
    // 填充用户信息
    fillEducationToPerson()

});

function fillEducationToPerson(){
    $.ajax({
        type: 'post',
        url: '/edu.servlet',
        data: {
            "type":"query_all_edu",
            "userId": userId
        },
        dataType: 'json',
        success: function(data){
            console.log(data[0]);
            if (data[0] == undefined){
                $('#eduExp').append('<div class="me-empty">\n' +
                    '            <div class="tips">\n' +
                    '                <p>You haven\'t fill in any education information.</p>\n' +
                    '                <p>Feel free to edit new information.</p>\n' +
                    '            </div>\n' +
                    '            <div class="add-file">\n' +
                    '                <a href="admin.html">\n' +
                    '                    <i class="fa fa-plus fa-3x"></i>\n' +
                    '                </a>\n' +
                    '            </div>\n' +
                    '        </div>')

            } else{
                for(x in data){
                    loadEdu(data[x]);
                }
            }

        },
        error : function(msg) {
            alert("failed");
            console.log(msg)
        }
    });
}

function loadEdu(edu){
    console.log(edu);
    $('#eduExp').append('<div class="title_con"></div>\n' +
        '\n' +
        '                <ul class="edu">\n' +
        '                    <li>\n' +
        '                        <label>University:</label>\n' +
        '                        <span class="value">'+ edu.school +'</span>\n' +
        '                    </li>\n' +
        '\n' +
        '                    <li>\n' +
        '                        <label>Degree:</label>\n' +
        '                        <span class="value">'+ edu.degree +'</span>\n' +
        '                    </li>\n' +
        '\n' +
        '                    <li>\n' +
        '                        <label>Major:</label>\n' +
        '                        <span class="value">'+ edu.major +'</span>\n' +
        '                    </li>\n' +
        '\n' +
        '                    <li>\n' +
        '                        <label>Study Period:</label>\n' +
        '                        <span class="value">'+ edu.str_starttime + '  -  '+ edu.str_endtime +'</span>\n' +
        '                    </li>\n' +
        '\n' +
        '                    <li>\n' +
        '                        <label>Main Course:</label>\n' +
        '                        <span class="value">'+ edu.maincourse +'</span>\n' +
        '                    </li>\n' +
        '                </ul>');
}