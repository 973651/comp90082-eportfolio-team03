/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 */

var strcookie = document.cookie;
var userIdStr = strcookie.split(";")[0];
var userId = userIdStr.split("=")[1];

$(function() {
    // 填充用户信息
    fillExperienceInfo();
    fillEducationInfo();

    jQuery(document).ready(function(){
        $('#submitEdu').on("click", function(){
            addNewEdu();
        });
    });

    jQuery(document).ready(function(){
        $('#submitWork').on("click", function(){
            addNewWork();
        })
    });

});

function fillExperienceInfo() {

    var count = 1;
    $.ajax({
        type: 'post',
        url: '/ExperienceServlet',
        data: {
            "userId": userId
        },
        dataType: 'json',
        success: function(data){
            for(x in data){
                loadExperience(data[x],count);
                count ++;
            }
            $('.input-daterange input').datepicker({
                autoclose: true,
                clearBtn: true,
                todayBtn: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });
        },
        error : function(msg) {
            alert("failed to get experience");
            console.log(msg)
        }
    });
}

function fillEducationInfo(){

    var count = 1;
    $.ajax({
        type: 'post',
        url: '/edu.servlet',
        data: {
            "type":"query_all_edu",
            "userId": userId
        },
        dataType: 'json',
        success: function(data){
            for(x in data){
                loadEducation(data[x],count);
                count ++;
            }
            $('.input-daterange input').datepicker({
                autoclose: true,
                clearBtn: true,
                todayBtn: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });
        },
        error : function(msg) {
            alert("failed");
            console.log(msg)
        }
    });
}

function loadExperience(experience_data, count) {
    console.log(experience_data)

    $('#experience-show').append('<span>Work Experience Item '+ count +'</span>\n' +
        '                    <button class="delete" onclick="expDeleteConfirm('+experience_data.workid+')">Delete</button>\n' +
        '                    <div class="tab-pane active">\n' +
        '                        <form class="form-horizontal">\n' +
        '                            <input type="text" name="edit_expid" value="'+experience_data.workid+'" style="display: none">\n' +
        '                            <div class="form-group">\n' +
        '                                <label class="col-sm-2 control-label">Company</label>\n' +
        '                                <div class="col-sm-8">\n' +
        '                                    <input name="expcompany" type="text" class="form-control1" value="'+ experience_data.company +'">\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '\n' +
        '                            <div class="form-group">\n' +
        '                                <label class="col-sm-2 control-label">Job Title</label>\n' +
        '                                <div class="col-sm-8">\n' +
        '                                    <input name="expjob" type="text" class="form-control1" value="'+ experience_data.jobname +'">\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '\n' +
        '                            <div class="form-group">\n' +
        '                                <label class="col-sm-2 control-label">Work Period</label>\n' +
        '                                <div class="col-sm-8">\n' +
        '                                   <div class="input-group input-daterange">\n' +
        '                                       <input name="work_start_show" type="text" placeholder="DD-MM-Year" data-date-format="yyyy-mm-dd" class="form-control1" style="background-color: #fff" value="'+ experience_data.str_starttime +'"/>\n' +
        '                                       <div class="input-group-addon">To</div>\n' +
        '                                       <input name="work_end_show" type="text" placeholder="DD-MM-Year" data-date-format="yyyy-mm-dd" class="form-control1" style="background-color: #fff" value="'+ experience_data.str_endtime +'"/>\n' +
        '                                   </div>' +
        '                                </div>\n' +
        '                            </div>\n' +
        '\n' +
        '                            <div class="form-group mb-n">\n' +
        '                                <label class="col-sm-2 control-label label-input-lg">Description</label>\n' +
        '                                <div class="col-sm-8">\n' +
        '                                    <textarea name="expdes" type="text" class="form-control1 input-lg">'+ experience_data.workdescription +'</textarea>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                        </form>\n' +
        '                    </div>');
}

function loadEducation(education, count){

    $('#education-show').append('<span>Education Item '+ count +'</span>\n' +
        '                    <button class="delete" onclick="eduDeleteConfirm('+education.educationid+')">Delete</button>\n' +
        '                    <div class="tab-pane active">\n' +
        '                        <form class="form-horizontal">\n' +
        '                            <input type="text" name="edit_eduid" value="'+education.educationid+'" style="display: none">\n' +
        '                            <div class="form-group">\n' +
        '                                <label class="col-sm-2 control-label">University</label>\n' +
        '                                <div class="col-sm-8">\n' +
        '                                    <input name="eduschool" type="text" class="form-control1" value="'+ education.school +'">\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '\n' +
        '                            <div class="form-group">\n' +
        '                                <label class="col-sm-2 control-label">Degree</label>\n' +
        '                                <div class="col-sm-8"><select name="eduselector" class="form-control1">\n' +
        '                                    <option value="'+ education.degree +'">'+ education.degree +'</option>\n' +
        '                                    <option value="PHD">PHD</option>\n' +
        '                                    <option value="Master">Master</option>\n' +
        '                                    <option value="Bachelor">Bachelor</option>\n' +
        '                                    <option value="MBA">MBA</option>\n' +
        '                                    <option value="Others">Others</option>\n' +
        '                                </select></div>\n' +
        '                            </div>\n' +
        '\n' +
        '                            <div class="form-group">\n' +
        '                                <label class="col-sm-2 control-label">Major</label>\n' +
        '                                <div class="col-sm-8">\n' +
        '                                    <input name="edumajor" type="text" class="form-control1" value="'+ education.major +'">\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '\n' +
        '                            <div class="form-group">\n' +
        '                                <label class="col-sm-2 control-label">Graduation Date</label>\n' +
        '                                <div class="col-sm-8">\n' +
        '                                   <div class="input-group input-daterange">\n' +
        '                                       <input name="edu_start_show" type="text" placeholder="DD-MM-Year" data-date-format="yyyy-mm-dd" class="form-control1" style="background-color: #fff" value="'+ education.str_starttime +'"/>\n' +
        '                                       <div class="input-group-addon">To</div>\n' +
        '                                       <input name="edu_end_show" type="text" placeholder="DD-MM-Year" data-date-format="yyyy-mm-dd" class="form-control1" style="background-color: #fff" value="'+ education.str_endtime +'"/>\n' +
        '                                      </div>' +
        '                                </div>\n' +
        '                            </div>\n' +
        '\n' +
        '                            <div class="form-group">\n' +
        '                                <label class="col-sm-2 control-label">Main Course</label>\n' +
        '                                <div class="col-sm-8">\n' +
        '                                    <textarea name="educourse" type="text" class="form-control1">'+ education.maincourse +'</textarea>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '\n' +
        '                        </form>\n' +
        '                    </div>')
}

function addNewEdu(){
    var school = $('#school').val();
    var sel = document.getElementById("degree");
    var degree = sel.value;
    var major = $('#major').val();
    var maincourse = document.getElementById("maincourse").value;
    var starttime = $('#edu_start').val();
    var endtime = $('#edu_end').val();

    $.ajax({
        type: 'post',
        url: '/edu.servlet',
        data: {
            "type":"insert_new_edu",
            "school": school,
            "degree": degree,
            "major":major,
            "starttime":starttime,
            "endtime":endtime,
            "maincourse":maincourse,
            "userId": userId
        },
        dataType: 'text',
        success: function(response){
            if (response == "success"){
                alert("Add New Education Experience Success !");
                window.location.href="admin.html";
            } else{
                alert("Add New Education Experience Failed");
            }
        }
    });

}

function addNewWork(){

    var company = $('#company').val();
    var jobname = $('#jobname').val();
    var desc = document.getElementById("workDesc").value;
    var start = $('#work_start').val();
    var end = $('#work_end').val();

    $.ajax({
        type: 'post',
        url: '/exp.servlet.add',
        data: {
            "type":"insert_new_work",
            "company": company,
            "jobname": jobname,
            "start":start,
            "end":end,
            "desc":desc,
            "userId":userId
        },
        dataType: 'text',
        success: function(response){
            if (response == "success"){
                alert("Add New Work Experience Success !");
                window.location.href="admin.html";
            } else{
                alert("Add New Work Experience Success !");
            }

        }
    });
}

function expDeleteConfirm(id){
    $.confirm({
        title: 'Item Delete Confirm',
        content: 'Are You Sure You Want to Delete this Work Experience?\n',
        icon: 'fa fa-warning',
        theme: 'supervan',
        buttons: {
            ok: {
                text: "ok",
                btnClass: 'btn-primary',
                keys: ['enter'],
                action: function() {
                    deleteExp(id);
                }
            },
            cancel: function(){
                console.log('the user clicked cancel');
            }
        }
    });
}

function eduDeleteConfirm(id){
    $.confirm({
        title: 'Item Delete Confirm',
        content: 'Are You Sure You Want to Delete this Education Experience?\n',
        icon: 'fa fa-warning',
        theme: 'supervan',
        buttons: {
            ok: {
                text: "ok",
                btnClass: 'btn-primary',
                keys: ['enter'],
                action: function() {
                    deleteEdu(id);
                }
            },
            cancel: function(){
                console.log('the user clicked cancel');
            }
        }
    });
}

function deleteExp(workid){

    $.ajax({
        type: 'post',
        url: '/exp.servlet.update',
        data: {
            "type": "delete",
            "workid": workid,
        },
        dataType: 'text',
        success: function(data){
            if(data == "success"){
                $('#experience-show').html("");
                fillExperienceInfo()
            } else{
                alert("delete failed");
            }
        },
        error : function(msg) {
            alert("delete exp failed");
            console.log(msg)
        }
    });
}

function deleteEdu(eduid){
    $.ajax({
        type: 'post',
        url: '/edu.servlet',
        data: {
            "type": "delete",
            "userId": userId,
            "eduid": eduid,
        },
        dataType: 'text',
        success: function(data){
            if(data == "success"){
                $('#education-show').html("");
                fillEducationInfo();
            } else{
                alert("delete failed");
            }
        },
        error : function(msg) {
            alert("delete edu failed");
            console.log(msg)
        }
    });
}

function editExps(){

    var company = document.getElementsByName("expcompany");
    var edit_expid = document.getElementsByName("edit_expid");
    var job = document.getElementsByName("expjob");
    var des = document.getElementsByName("expdes");
    var starttime = document.getElementsByName("work_start_show");
    var endtime = document.getElementsByName("work_end_show");
    var n = company.length;
    var count = 0;
    for(i=0; i<n; i++){
        $.ajax({
            type: 'post',
            url: '/exp.servlet.update',
            data: {
                "type": "edit",
                "userId": userId,
                "expid":edit_expid[i].value,
                "company": company[i].value,
                "job": job[i].value,
                "starttime": starttime[i].value,
                "endtime": endtime[i].value,
                "des": des[i].value
            },
            dataType: 'text',
            success: function(data){
                count ++;
                if (count === n){
                    alert(" Successfully Upload Information !");
                    $('#experience-show').html("");
                    fillExperienceInfo();
                }
            },
            error : function(msg) {
                alert("edit experience" +i+" failed");
                console.log(msg)
            }
        });
    }

}

function editEdus(){

    var school = document.getElementsByName("eduschool");
    var edit_eduid = document.getElementsByName("edit_eduid");
    var selector = document.getElementsByName("eduselector");
    var major = document.getElementsByName("edumajor");
    var course = document.getElementsByName("educourse");
    var starttime = document.getElementsByName("edu_start_show");
    var endtime = document.getElementsByName("edu_end_show");
    var n = school.length;
    var count = 0;
    for(i=0; i<n; i++){
        $.ajax({
            type: 'post',
            url: '/edu.servlet',
            data: {
                "type": "edit",
                "userId": userId,
                "educationId": edit_eduid[i].value,
                "school": school[i].value,
                "degree": selector[i].value,
                "major": major[i].value,
                "starttime": starttime[i].value,
                "endtime": endtime[i].value,
                "course": course[i].value
            },
            dataType: 'text',
            success: function(data){
                count ++;
                if (count === n){
                    alert(" Successfully Upload Information !");
                    $('#education-show').html("");
                    fillEducationInfo();
                }
            },
            error : function(msg) {
                alert("edit education" +i+" failed");
                console.log(msg)
            }
        });
    }

}