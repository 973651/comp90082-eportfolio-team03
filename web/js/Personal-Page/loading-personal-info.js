/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 */
var strcookie = document.cookie;
var userIdStr = strcookie.split(";")[0];
var userId = userIdStr.split("=")[1];
$(function() {
    // 填充用户信息
    fillUserInfo();

});

function fillUserInfo() {
    // 加载用户信息
    $.ajax({
        type : 'GET',
        async : false,
        url : '/edit.people',
        dataType : 'json',
        data : {
            type : 'query_info',
            userId : userId,
            userName : "alexander"
            // userId : $.cookie('userid'),
            // userName : $.cookie('name')
        },
        success : function(response) {
            // document.getElementById("username").innerHTML = response[0].name;
            // document.getElementById("major").innerHTML =response[0].major;
            // document.getElementById("dob").innerHTML = response[0].str_dob;
            // document.getElementById("txt_email").innerHTML =response[0].email;
            // document.getElementById("phone").innerHTML =response[0].phone;
            // document.getElementById("address").innerHTML =response[0].address;
            // document.getElementById("selfIntro").innerHTML =response[0].description;
            $('#username').append(response[0].name);
            $('#major').append(response[0].major);
            $('#dob').append(response[0].str_dob);
            $('#address').append(response[0].address);
            $('#txt_email').append(response[0].email);
            $('#phone').append(response[0].phone);
            $('#selfIntro').append(response[0].description);
            $('#leftAvatar').attr('src', response[0].photo);
            $('#rightAvatar').attr('src', response[0].photo);

        }
    });
}