/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 */
$.extend({
    'getUserIdParam' : function(){
        var query = window.location.href;
        var vars = query.split('=');
        return vars[1];
    }
});
$(function() {
    // 填充用户信息
    fillOUserInfo();

});

function fillOUserInfo() {
    var userId = $.getUserIdParam().split("#")[0];
    console.log(userId);
    // 加载用户信息
    $.ajax({
        type : 'GET',
        async : false,
        url : '/edit.people',
        dataType : 'json',
        data : {
            type : 'query_info',
            userId : userId,
            // userId : $.cookie('userid'),
            // userName : $.cookie('name')
        },
        success : function(response) {
            console.log(response);
            $('#ousername').append(response[0].name);
            $('#odob').append(response[0].str_dob);
            $('#oaddress').append(response[0].address);
            $('#otxt_email').append(response[0].email);
            $('#ophone').append(response[0].phone);
            $('#omajor').append(response[0].major);
            $('#oselfIntro').append(response[0].description);
            $('#oleftAvatar').attr('src', response[0].photo);
            $('#orightAvatar').attr('src', response[0].photo);

        }
    });
}