/**
 * @author Boyan XIN
 * @version 1.1 some minor edit of loadPeople()
 * @contact boyanx@student.unimelb.edu.au
 */

$(function(){
    console.log("success load js file")
    queryPeople();
});

$.extend({
    'getUrlParam' : function(variable){
        var query = window.location.search.substring(1);
        var vars = query.split('&');

        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (pair[0] == variable) {
                return pair[1];
            }
        }

        return false;
    }
});

/**
 * Query A list of people
 */
function queryPeople(){
    console.log($.getUrlParam('keywords') ? $.getUrlParam('keywords') : '');
    $.ajax({
        type: 'GET',
        async: false,
        url: '/query.people',
        dataType: 'json',
        data:{
            type: 'query_all',
            keywords : $.getUrlParam('keywords') ? $.getUrlParam('keywords') : '',
            fil1 : $.getUrlParam('fil1') ? $.getUrlParam('fil1') : '',
            fil2 : $.getUrlParam('fil2') ? $.getUrlParam('fil2') : ''
        },
        success: function (response) {

            console.log("success function response");
            for (x in response){
                loadPeopleList(response[x])
            }
        },
        error: function (XMLHttpRequest, textStatus) {
            $.confirm({
                title: 'Something Wrong with Your Search',
                content: textStatus + ' : ' + XMLHttpRequest,
                autoClose: 'ok|1000',
                type: 'red',
                buttons: {
                    ok: {
                        text: 'Confirm',
                        btnClass: 'btn-primary',
                    },
                }
            });
        }
    });
}

function loadPeopleList(people_data) {
    console.log(people_data);
    $('#searchResults').append('<li>\n' +
        '                          <div class="user-in">\n' +
        '                              <img src="'+ people_data.photo +'" alt="" class="user-img">\n' +
        '                              <div class="user-des">\n' +
        '                                  <h3><a href="/other-person.html?id='+ people_data.userid +'">'+ people_data.name +'</a></h3>\n' +
        '                                  <h5>'+ people_data.major +'</h5>\n' +
        '                                   <p>'+ people_data.description +'</p>\n' +
        '                              </div>\n' +
        '                          </div>\n' +
        '                       </li>');
}