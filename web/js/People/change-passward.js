/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 */

/** Init */
var strcookie = document.cookie;
var userIdStr = strcookie.split(";")[0];
var userId = userIdStr.split("=")[1];
$(document).ready(
    function() {
    // validation

    modifyPwdValidator();

    // submit
    $('#submitNew').click(submitEditForm);
    }
);

/**
 * 提交修改密码表单
 * @returns
 */
function submitEditForm() {
    // 触发全部验证
    $('#changePwd').data('bootstrapValidator').validate();
    var flag = $('#changePwd').data('bootstrapValidator').isValid();

    if (!flag) {
        return;
    }

    // 如果旧密码不对
    if (confirmOldPwd() == -1) {
        //改变旧密码框校验状态
        $('#oldPassword').parent().parent().removeClass('has-success').addClass('has-error');
        $('#oldPassword').next().removeClass('glyphicon-ok').addClass('glyphicon-remove');
        $('#helpBlock').show();

        //旧密码框内容改变，隐藏校验
        $('#oldPassword').on('input', function(){
            $('#helpBlock').hide();
        });

        return;
    }
    console.log("edit ! ! !")

    $.ajax({
        url : '/edit.people',
        async : false,
        type : 'GET',
        data : {
            type : 'change_pwd',
            userId: userId,
            password: $('#newPassword').val()
            // userName : $.cookie('username'),
            // password : $.md5($('#newPassword').val())
        },
        success : function(response) {
            if(response == 1){
                //重置表单和验证规则
                $('#changePwd')[0].reset();
                $('#changePwd').data('bootstrapValidator').resetForm(true);
                //清空修改密码区
                // $('#mainWindow').html('');

                //退出登录
                // quitLoginClick();
                $.ajax({
                    type: 'post',
                    url: 'LoginServlet',
                    data: {
                        "type": "logout"
                    },
                    dataType: 'text',
                    success: function(data){
                        if(data == "logout"){
                            window.location.href="index.html";
                        } else{
                            alert("no user now");
                        }
                    },
                    error : function(msg) {
                        alert("logout failed");
                        console.log(msg)
                    }
                });
            }else{
                $.confirm({
                    title: false,
                    content: 'Fail to Modify the Password',
                    autoClose: 'ok|2000',
                    type: 'red',
                    buttons: {
                        ok: {
                            text: 'Yes',
                            btnClass: 'btn-primary',
                        },
                    }
                });
            }
        },
        error : function(XMLHttpRequest, textStatus) {
            $.confirm({
                title : 'Something Wrong When Change the Password',
                content : textStatus + ' : ' + XMLHttpRequest.status,
                autoClose : 'ok|2000',
                type : 'red',
                buttons : {
                    ok : {
                        text : 'Yes',
                        btnClass : 'btn-primary',
                    },
                }
            });
        }
    });
}

/**
 * 确认旧密码
 *
 * @returns
 */
function confirmOldPwd() {
    var result;

    $.ajax({
        url : '/edit.people',
        async : false,
        type : 'GET',
        data : {
            type : 'auth_password',
            userId : userId,
            password : $('#oldPassword').val()
            // userName : $.cookie('username'),
            // password : $.md5($('#oldPassword').val())
        },
        success : function(response) {
            if (response == 1) {
                alert("Change Password Success! Automatically Logout ~");
                result = 1;
            } else {
                result = -1;
                alert("Change Password Failed")
            }
        },
        error : function(XMLHttpRequest, textStatus) {
            $.confirm({
                title : 'Something Wrong When Validate the Original Pasword',
                content : textStatus + ' : ' + XMLHttpRequest.status,
                autoClose : 'ok|2000',
                type : 'red',
                buttons : {
                    ok : {
                        text : '确认',
                        btnClass : 'btn-primary',
                    },
                }
            });
            result = -1;
        }
    });
    return result;


}

/**
 * 修改密码有效性验证
 *
 * @returns
 */
function modifyPwdValidator() {
    $('#changePwd').bootstrapValidator({
        message : '输入无效!',
        feedbackIcons : {
            valid : 'glyphicon glyphicon-ok',
            invalid : 'glyphicon glyphicon-remove',
            validating : 'glyphicon glyphicon-refresh'
        },
        fields : {
            oldPassword : {
                message : 'invalid password!',
                validators : {
                    notEmpty : {
                        message : 'password can\'t be empty!'
                    },
                    stringLength : {
                        min : 6,
                        max : 15,
                        message : 'The length must between 6 to 15!'
                    },
                }
            },
            newPassword : {
                message : 'invalid password!',
                validators : {
                    notEmpty : {
                        message : 'password can\'t be empty!!'
                    },
                    stringLength : {
                        min : 6,
                        max : 15,
                        message : 'The length must between 6 to 15!'
                    },
                    different : {
                        field : 'oldPassword',
                        message : 'The new password can\'t same as the old one!'
                    }
                }
            },
            confirmPwd : {
                validators : {
                    notEmpty : {
                        message : 'password can\'t be empty'
                    },
                    identical : {
                        field : 'newPassword',
                        message : 'Please enter the same password!'
                    }
                }
            },
        }
    });
}