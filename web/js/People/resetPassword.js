$.extend({
    'getUrlParam' : function(variable){
        var query = window.location.search.substring(1);
        var vars = query.split('&');

        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (pair[0] == variable) {
                return pair[1];
            }
        }

        return false;
    }
});

$(document).ready(
    function() {
        // validation

        modifyPassword();

        $('#resetPassword').on("click", function(){
            resetPassword();
        })

    }
);

function resetPassword() {
    var password = $('#confirmpassword').val();
    var email=$.getUrlParam('keyword') ? $.getUrlParam('keyword') : '';
    $.ajax({
        type: 'post',
        async : false,
        url: '/resetPasswordServlet',
        data: {
            "password":password,
            "email": email
        },
        success : function(response) {
            if (response == -1) {
                alert("Reset Faild");
            } else {
                $.confirm({
                    title: 'Congratulation!',
                    content: 'You Are Successfully Change Your Password ! \n' +
                    'Click OK Go Back to Main Page!',
                    theme: 'white',
                    buttons: {
                        ok: {
                            text: "ok",
                            action: function() {
                                $(window).attr('location', '/index.html');
                            }
                        }
                    }
                });
            }
        }
    });
}

function modifyPassword(){
    $(document).ready(
        function() {
            // validation
            $('#resetPwd').bootstrapValidator({
                message: 'This value is not valid',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    newpassword: {
                        message: 'The password is not valid',
                        validators: {
                            notEmpty: {
                                message: 'The password is required and cannot be empty'
                            },
                            stringLength: {
                                min: 6,
                                max: 15,
                                message: 'The password must be more than 6 and less than 15 characters long'
                            },
                        },
                        confirmpassword: {
                            notEmpty: {
                                message: 'The password is required and cannot be empty'
                            },
                            identical: {  //比较是否相同
                                field: 'password',  //需要进行比较的input name值
                                message: 'password must be same'
                            },

                        }
                    }
                }
            });
        }
    );

}
