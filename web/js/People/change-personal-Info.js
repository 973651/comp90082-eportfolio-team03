/**
 * @author Boyan XIN
 * @version 1.0
 * @contact boyanx@student.unimelb.edu.au
 */
var strcookie = document.cookie;
var userIdStr = strcookie.split(";")[0];
var userId = userIdStr.split("=")[1];
var avatarLink = null;
$(function() {
    // 填充用户信息
    fillUserInfo();
    changePhoto();
    // 提交事件
     jQuery(document).ready(function(){
         console.log("start to listen");
         $('#editPersonalInfo').on("click", function(){
             submitForm();
         });
     })
});


/**
 * 填充用户信息
 *
 * @returns
 */
function fillUserInfo() {
    // 加载用户信息
    $.ajax({
        type : 'GET',
        async : false,
        url : '/edit.people',
        dataType : 'json',
        data : {
            type : 'query_info',
            userId : userId,
        },
        success : function(response) {
            $('#imgshow').attr('src', response[0].photo);// 显示图片
            $('#userid').val(response[0].userid);
            $('#username').val(response[0].name);
            $('#myemail').val(response[0].email);
            $('#dob').val(response[0].str_dob);
            $('#phone').val(response[0].phone);
            $('#address').val(response[0].address);
            $('#desc').val(response[0].description);
            $('#job').val(response[0].job);
            $('#selector2').append('<option value="'+ response[0].major +'">'+ response[0].major +'</option>\n' +
                '<option value="Science">Science</option>\n' +
                '<option value="Law">Law</option>\n' +
                '<option value="Management">Management</option>\n' +
                '<option value="Arts">Arts</option>\n' +
                '<option value="Engineering">Engineering</option>\n' +
                '<option value="Medicine">Medicine</option>');

            if (response[0].gender == 0){
                $('#genderRadio').append('<div class="checkbox-inline"><label><input type="radio" name="gender" value="0" checked="checked"> Man</label></div>\n' +
                    '<div class="checkbox-inline"><label><input type="radio" name="gender" value="1"> Woman</label></div>\n' +
                    '<div class="checkbox-inline"><label><input type="radio" name="gender" value="2"> Other</label></div>\n' +
                    '<div class="checkbox-inline"><label><input type="radio" name="gender" value="3"> Prefer not to show</label></div>');
            } else if (response[0].gender == 1){
                $('#genderRadio').append('<div class="checkbox-inline"><label><input type="radio" name="gender" value="0"> Man</label></div>\n' +
                    '<div class="checkbox-inline"><label><input type="radio" name="gender" value="1" checked="checked"> Woman</label></div>\n' +
                    '<div class="checkbox-inline"><label><input type="radio" name="gender" value="2"> Other</label></div>\n' +
                    '<div class="checkbox-inline"><label><input type="radio" name="gender" value="3"> Prefer not to show</label></div>');
            } else if (response[0].gender == 2){
                $('#genderRadio').append('<div class="checkbox-inline"><label><input type="radio" name="gender" value="0"> Man</label></div>\n' +
                    '<div class="checkbox-inline"><label><input type="radio" name="gender" value="1"> Woman</label></div>\n' +
                    '<div class="checkbox-inline"><label><input type="radio" name="gender" value="2" checked="checked"> Other</label></div>\n' +
                    '<div class="checkbox-inline"><label><input type="radio" name="gender" value="3"> Prefer not to show</label></div>');
            } else if (response[0].gender == 3){
                $('#genderRadio').append('<div class="checkbox-inline"><label><input type="radio" name="gender" value="0"> Man</label></div>\n' +
                    '<div class="checkbox-inline"><label><input type="radio" name="gender" value="1"> Woman</label></div>\n' +
                    '<div class="checkbox-inline"><label><input type="radio" name="gender" value="2"> Other</label></div>\n' +
                    '<div class="checkbox-inline"><label><input type="radio" name="gender" value="3"  checked="checked"> Prefer not to show</label></div>');
            } else {
                $('#genderRadio').append('<div class="checkbox-inline"><label><input type="radio" name="gender" value="0"> Man</label></div>\n' +
                    '<div class="checkbox-inline"><label><input type="radio" name="gender" value="1"> Woman</label></div>\n' +
                    '<div class="checkbox-inline"><label><input type="radio" name="gender" value="2"> Other</label></div>\n' +
                    '<div class="checkbox-inline"><label><input type="radio" name="gender" value="3"> Prefer not to show</label></div>');
            }
        }
    });
}


/**
 * Submit the information
 *
 * @returns
 */
function submitForm() {
    console.log("submit form");

    var address = $('#address').val();
    // var email = $('#m').val();
    var phone = $('#phone').val();
    var new_username = $('#username').val();
    var description = $('#desc').val();
    var dob = $('#dob').val();
    var selector = document.getElementById("selector2");
    var major = selector.value;
    var job = $('#job').val();
    var gender = $("input[name='gender']:checked").val();
    console.log(gender);

    $.ajax({
        type : 'GET',
        async : false,
        url : '/edit.people',
        data : {
            type : 'modify_info',
            userName : new_username,
            userId : userId,
            address : address,
            phone: phone,
            description: description,
            dob: dob,
            major: major,
            job: job,
            gender: gender

        },
        success : function(response) {
            if (response == -1) {
                alert("Upload Personal Information Faild");
            } else {
                alert("Upload Personal Information Success");
            }
        },
        error : function(XMLHttpRequest, textStatus) {

        }
    });
}

/**
 * Check Photo
 */
function changePhoto(){
    $("#submitModify").on("click", function() {
        uploadImg()
    });
}


/**
 * 上传图片
 *
 * @param src
 * @returns
 */
function uploadImg() {
    $.ajax({
        type: "post",
        url: '/upload',
        data: new FormData($('#upload-form')[0]),
        processData: false,
        contentType: false,
        success: function (url) {
            alert("success upload!")
            $('#imgshow').attr('src',url);
            avatarLink = url;
            uploadAvatarIntoDb(avatarLink);
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Upload Failed");
        }

    });
}

function uploadAvatarIntoDb(newAvatar){

    $.ajax({
        type : 'GET',
        async : false,
        url : '/edit.people',
        data : {
            type : 'modify_photo',
            userId : userId,
            photo: newAvatar
        },
        success : function(response) {
            if (response == -1) {
                alert("Upload New Avatar Faild");
            } else {
                alert("Upload New Avatar Success");
                fillUserInfo();
            }
        },
        error : function(XMLHttpRequest, textStatus) {

        }
    });


}



