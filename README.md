## 1. Project Introduction

#### Project Background

E-portfolio is a web application that allows users to record and share their personal information and works in digital form. In general, those information can be input text, electronic files, graphics, multimedia and other forms of content. At the same time, it is also an important platform for the display of users’ capabilities and self-expression.

- For students, E-portfolio, like traditional portfolios, can help them reflect on their learning and allow students to have a better understanding of learning strategies and learning needs. The E-portfolio no longer need the learners to keep the paper evidence collected, instead it stores them in a more secure server.
- For mentors, appraisers, managers, and even employers, they can remotely access to these information to check the progress of learners and evaluate students’ learning profiles, while finding interested employees. Through E-portfolio, the communication with students are strengthened, and it reduces the cost of assessment and quality assurance.



------

#### Project Goal

The rational and effective use of E-portfolio is particularly important for every student. Our group aims at developing a user-friendly E-portfolio website for both the staffs and students in the University of Melbourne. This E-portfolio website would provide a platform for users to maintain their personal photos, history works, resumes, etc. which would not only facilitate the students but also those faculty and employers.

## 2. Client Information

|    Full Name    |                            Email                             |            Department             |
| :-------------: | :----------------------------------------------------------: | :-------------------------------: |
| **Doc Wallace** | [ian.wallace@unimelb.edu.au](mailto:ian.wallace@unimelb.edu.au) | Computing and Information Systems |



## 3. Supervisor Information

| **Full Name** | **Email**                                                 | **Profile**                          | **Contact Number** |
| ------------- | --------------------------------------------------------- | ------------------------------------ | ------------------ |
| Chuang Wang   | [chuangw1@unimelb.edu.au](mailto:chuangw1@unimelb.edu.au) | https://www.linkedin.com/in/chuangw/ | 0410181120         |

## 4. Team Information

- Our team consists of six students from Master of Information Technology. With a strong and active team, we actively engaged in finding out solutions for our clients and we keep our minds in supporting the clients’ needs all the time.

- In our scrum team, there are three roles — Scrum master Quality Manager and Scrum Team Member.

  1. Considering the size and composition of our team, some responsibilities such as defining requirements and risk management are assigned to the whole team rather than determined by Scrum Master.

  2. As a student team without professional developers, our **Scrum Master and Quality Manager** also participate in designing and developing the website.

  3. The Quality Manager is not the only person responsible for testing, but needs to check the quality of the overall development.

  4. We do not distinguish between front-end and back-end developers, each member plays a cross-functional role (the roles include designer, programmer, tester and risk manager), which will make the most of everyone’s capacity and improve the efficiency to the maximum extent. 

     

| Full Name   | Email                                                        | Roles  and Responsibility                                    | Contact  Number |
| ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ | --------------- |
| Boyan XIN   | [boyanx@student.unimelb.edu.au](mailto:boyanx@student.unimelb.edu.au) | Scrum Master in this team,  the responsibility includes:Prepare project plan.Prioritize the  features.Coordinate the team and ensure the agile process.Schedule the weekly  meetings.Analysis.Estimation and Iteration plan.Risk management.Also need to  participate in designing and coding work. | 451800416       |
| Lixin Shen  | [lixins@student.unimelb.edu.au](mailto:lixins@student.unimelb.edu.au) | Quality Manager in this  team, the responsibility includes:Also need to participate in designing and  coding work.Help developers test their work.Check code, approves tested part  move to 'Done'.Communicate with team members.Participate in every group  meeting and provide suggestions.Participate in sprint planning and review. | 420450110       |
| Jie Niu     | [jien1@student.unimelb.edu.au](mailto:jien1@student.unimelb.edu.au) | Scrum Team Members, the responsibility includes:Discuss and allocate  tasks.Analysis.Framework design.Functional logic design.Data structure  design.Development work (include front-end and back-end of the product).Code  management (code version, product iteration)Test their own work.Participate  in every group meeting and provide suggestions.Participate in sprint planning  and review. | 450540629       |
| DAWEI JU    | [daweij@student.unimelb.edu.au](mailto:daweij@student.unimelb.edu.au) |                                                              | 452643315       |
| Ye TIAN     | [yetian@student.unimelb.edu.au](mailto:yetian@student.unimelb.edu.au) |                                                              | +86 15624962520 |
| Xinquan Cai | [xinquanc@student.unimelb.edu.au](mailto:xinquanc@student.unimelb.edu.au) |                                                              | 414612467       |


## Acknowledgments
- In our web, we use the open source rich text editor plugin — NKEditor. NKEditor is developed based on KindEditor, which is a lightweight, open source, cross browser, web based WYSIWYG HTML editor. Thanks to the authors of these two editor, so that we can have a powerful text editor.
- Thanks for our subject coordinator Leon Sterling, always patient with us students affected by the epidemic (Covid-19) and help us solve problems.
- Thanks for our client Doc Wallace, always patient and offer us useful advice.
- Thanks for our supervisor Chuang Wang who give us a lot of useful suggestions and always help us communicate with the coordinator.